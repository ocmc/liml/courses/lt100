+++
title = "A Method for Comparing the Meaning of Words"
date =  2020-08-06T14:16:49-07:00
weight = 20
+++

Within a language, it is often helpful to study and compare several words of similar meaning. Comparing the words with each other helps to show the exact meaning of each word, and how that word differs in meaning from other words. It shows which parts of the meaning are the same, and which are different. Here is a procedure to follow:  

1) Make a list of words that have meanings that are similar. List them down the side of a piece of paper, leaving two or three lines for each word.  

2) Write a definition for each word, breaking down the parts of meaning. Compare the words with each other to make sure that you have defined precisely in what way the meaning of each word is different.  

2) Check that all the facts have been listed that are relevant for showing the difference in meaning between the words. (If no difference can be found between any two words, it means that they have the same meanings and may both be translated by one term in the Receptor Language, at least in some contexts.)  

---

#### EXAMPLE 1  

In English, there are a number of words for different places where water is found. These include the following words:  

river  
stream  
lake  
pond  
well  
sea  

A **river** is a large body of fresh water that flows.  
A **stream** is a small body of fresh water that flows.  
A **lake** is a large body of fresh water that does not flow.  
A **pond** is a small body of fresh water that does not flow.  
A **well** is a deep, man-made hole, from which water can be drawn.  
A **sea** is a very, very large body of salt water that had tides.  

Comparing the words helps to show what is the precise difference of meaning between them. It is important for a translator to be aware of these differences in the Source Language, so that he can translate that meaning correctly.      

A similar study of words in the same area of meaning can be made in the Receptor Language.  

For example, in the Mbembe language there are three words:  

*epe* covers the area of meaning "lake," "pond," and "well" in English. In fact is is used to refer to any body of non-flowing water.  
*ekeka* has a meaning more or less the same as English "stream," a small body of flowing water.  
*oraanga* means a large body of flowing water; whether the water is salt or not does not make any difference in the Mbembe system. "Sea" is expressed in Mbembe as *oraanga kwiden* 'big river.'  

From this study it can be seen that the Mbembe and English words do not match. The translator will need to be careful to choose the appropriate word in each context.  

---

#### EXAMPLE 2  

In English there are many different words for different ways of cooking. Here are some of them:  

to roast  
to bake  
to fry  
to boil  
to toast  

**to roast** means "to cook something in an oven or over a fire, using fat or oil."  
**to bake** means "to cook something in an oven (without using fat or oil)."  
**to fry** means "to cook something in fat in a pan."  
**to boil** means "to cook something in water in a pan."  
**to toast** means "to cook bread or something similar, by holding it up to the heat of a fire."  

---

#### EXERCISE 8  

List as many words as you can in your own language for different ways of cooking. Show how the words differ in meaning from each other. Compare the meaning of these words with the English words listed above.  

---

#### EXERCISE 9  

List as many words or expressions as you can in your own language for the idea of surprise or amazement. Show how the words differ in meaning.  

---

#### EXERCISE 10  

Compare the meaning of the following words in English:  

he was surprised  
he was amazed  
he was astounded  
he was shocked  
he was puzzled  

---

#### EXERCISE 11  

Do a similar study for the following sets of words in English. Use your dictionary to help you.  

1. error, crime, sin, fault, misdemeanour  
2. proverb, parable, story, history, riddle, allegory, fable  
3. to shout, to cry, to scream  



---

From *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, pages 60 - 62  