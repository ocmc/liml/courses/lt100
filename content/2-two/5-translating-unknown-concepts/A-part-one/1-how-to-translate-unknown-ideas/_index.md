+++
title = "1. How to Translate Unknown Ideas"
date =  2021-09-14T14:25:41-07:00
weight = 5
+++

Later in the course, we will be considering other ways of providing the reader with essential information that he needs to understand the message correctly. Some of this information can be provided outside the text. For example, by the use of aids such as pictures, glossary, and footnotes. (See Chapter __ for further suggestions on this point.)  

It is strongly recommended that such aids be included in published liturgical books. **But this is not a substitute for meaningful translation in the text itself. You must still aim to make the translation as meaningful as possible.** Remember that many people will only hear or sing the text, rather than reading it in a liturgical book.  

How then, can you communicate the meaning of these unknown ideas? Follow the *two steps in translating.* First **discover the exact meaning of the idea you are translating.** Then **find a way to re-express that meaning** in the Receptor Language. There are **three main methods by which the meaning of an unknown idea can be communicated.** These will be discussed below. Each method has certain advantages and disadvantages.    

{{% notice note %}}
**REMEMBER**  
**Step 1** is always DISCOVER THE MEANING. You cannot translate until you are sure that you yourself know the meaning of the original word or phrase accurately.  
Use a DICTIONARY or GLOSSARY and other aids in order to be sure of the exact meaning of the original idea.  
**Step 2** is RE-EXPRESS THE MEANING. There are at least three possible ways in which unknown ideas can be translated. Consider each case separately to find the solution which fits that particular idea and context best.  
*Three possible solutions are:*  
1 Use a descriptive phrase.  
2 Substitute something similar that is known to the Receptor Language speakers.  
3 Use a foreign word from another language.
{{% /notice %}}

---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 36.