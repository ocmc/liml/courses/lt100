+++
title = "2. Use a Word That Is More General in Meaning"
date =  2021-09-23T13:13:18-07:00
weight = 5
+++

What is meant by a word that is more general than another word?  

Here are some examples:  

Compare the two words "cat" and "animal." A cat is one kind of animal; there are also other kinds of animal, like "dog" and "horse." So the word "animal" is **more general** than the word "cat," because it includes other kinds of animals besides cats.  

Similarly, the word "furniture" is more general than the word "table" because a table is one example of a piece of furniture. There are also other kinds of furniture, like "chair," "cupboard" or "bed."  

The word "building" is more general than the word "house" because a house is a kind of buidling. There are other kinds of building, like a "school" or "mosque."  

The word "to kill" is more general than the word "to strangle" because strangling is one way of killing a person.

---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 45.