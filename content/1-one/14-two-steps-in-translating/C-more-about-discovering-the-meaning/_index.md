+++
title = "C. More About Discovering the Meaning"
date =  2021-09-08T13:34:45-07:00
weight = 15
+++

1. Read the whole text before you translate. If it is a fixed hymn or prayer in a service, read the whole service. If it is a moveable part (a hymn from the menaion, for example), read a whole service with that part inserted. If possible, attend a service where the text you will be translating is used, and familiarize yourself with its role in the service. Or pray the whole service yourself (in addition to just reading it), including the parts you are preparting to translate. Talk with people who chant the hymn or say the prayer, and ask them about its significance. Understand the main point of the whole text before you begin.  

1. Read each whole unit (each whole prayer, whole hymn, whole petition, etc) before you translate. Understand the main point of the whole unit before you begin. 

1. When translating liturgical texts from a language other than the original language, always use at least two versions together as your source text. Comparing two versions will help you think about the meaning, so that you do not just follow the words of one version literally. The two versions should be:  
a. One version that follows the form of the original language fairly closely; and  
b. One meaning-based version.  

1. Refer to a translation of the Bible in the language into which you are translating. If your language has multiple versions of the Bible, use the one that is used in your liturgical services. Compare it with other versions, if they are available. 

1. Refer to a translation of the *Byzantine text-type* or *Textus Receptus* of the New Testament. When liturgical texts quote the New Testament, this is the text-type they are quoting. The *New King James Version* NKJV is an English translation of the Textus Receptus.  

1. Refer to a translation of the *Septuagint* (LXX), the Old Greek version of the Old Testament. When liturgical texts quote the Old Testament, this is the text they are quoting. The *Saint Athanasius Acadamy Septuagint* (SAAS), found in the Orthodox Study Bible, is an English translation of the Septuagint.  

1. Use an English dictionary to look up any words you are not sure about. Like words of every language, English words sometimes have more than one meaning. Make sure you have understood the right meaning of the word in the text.

1. Use a Liturgical Translator's Handbook, if one is available for the texts you are translating.  

Later in this course we will be considering further ways of discovering the exact meaning of the text.  

Studying the source text and discovering the exact meaning is the first step.  

When the meaning of the passage is clear in your mind, you are ready to re-express that meaning in a clear and natural way in the Receptor Language.  

{{% notice note %}}
**REMEMBER:**  
When preparing to translate, the first step is *always* to study the meaning of the source text.  
When the meaning of the source text is clear in your mind, the second step is to re-express that meaning clearly in a way that is natural in the Receptor Language.
{{% /notice %}}

---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, pages 31 - 32.  