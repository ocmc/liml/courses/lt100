+++
title = "D. How to Translate Special Terms"
date =  2020-08-06T14:35:16-07:00
weight = 18
+++

#### STEP 1. First STUDY the meaning of the original term.  

The first step in translating is always **DISCOVER THE MEANING**.  

Look up the term in:  
(a) a special LITURGICAL DICTIONARY  
(b) a BIBLE DICTIONARY  
(c) a list of KEY BIBLICAL TERMS (This is a list of word studies prepared to help translators.)  
(d) DOXA liturgical translation software. This will show you the different contexts in which the term occurs.
(e) a BIBLE CONCORDANCE. This will show you the different biblical contexts in which the term occurs.  

If you observe from this study that the term has more than one sense, then you may need to do the other three steps listed below for each sense of the term separately.  

#### STEP 2. COMPARE the term with other terms that have similar meanings.  

In the list below, terms that have similar meanings are listed together. If you compare these terms with similar meaning, this will show up more clearly the exact differences in meaning between them (the "contrastive components"), and so help you to understand the meaning of each one more precisely.  

#### STEP 3. THINK about possible solutions for re-expressing the idea and tentatively CHOOSE the most appropriate.  

Remember the five ways in which UNKNOWN IDEAS can be translated. Make a list of different possibilities for translating the idea, considering alternative possible solutions.  

Consider the list of possible solutions you have made. Try to evaluate these possibilities. Here are some points that you should consider to help find the most appropriate translation:  

* **Is the proposed word or phrase meaningful?** In particular, how will it be understood by non-Christians and people who have not had any Christian teaching?  

* **Does the proposed term focus on the most important part of the meaning of the original term?** It will probably be impossible to communicate the whole meaning of the original term, but that part which is most central to the meaning should be communicated.  

* **Does the proposed term include any meaning which is contrary to the meaning of the original term?** Even if the term does not have the full meaning of the original term, it may be that through teaching in the churches, it will gradually expand in meaning. **But it is important that you do not choose a word that has any sense that is contrary to, or incompatible with, the meaning of the original term.** It is possible to develop and expand the meaning of a word, but it is not possible to wipe out or remove meanings that the speakers of the language already consider part of that word. Some words will need to be carefully studied in the Receptor Language.  

* **How is the term used in any other versions of Orthodox worship** which are in use in the area? For example, versions in the national language, the trade language, or the language of education.  

* **Cross-confessional concerns:** The translator must translate accurately, but when there is room for different opinions, avoid using any word which would offend any of the Christian confessions in the area. For example, a language might have a word which means "spirit." But, existing Christian confessions might use that word to mean "evil spirit." If Orthodox worship employs that word for the Holy Spirit, or the spirits who serve God, other people might think that Orthodox Christians worship demons.  

* Listen carefully to views expressed by church leaders and others, and to any strongly held local feelings.  

* Tentative decisions on key terms should be presented for discussion to the translation committee and, where appropriate, the final decision should be made by the committee.  

#### STEP 4. CHECK existing service books, and Scripture, in your language to see how these terms have been translated in the past.

> For example, the Greek word διάκονος *diakonos* means "servant." However, English Orthodox service books consistently translate διάκονος as "deacon" when referring to the ordained ministry of the deacon. If service books (or the New Testament) in your language already have a standard word for "deacon," you should probably use this word rather than using your language's word for "servant."

> In Kiswahili, for example, the word for "servant" is *mhudumu* 'one who serves,' but service books do not use this word for deacons. Instead, they use the word *shemasi*, which comes from the Arabic word شماس _shmas_ which means "servant." In Arabic Orthodox texts, شماس *shmas* is always used to refer to a deacon.

#### STEP 5. CHECK to see if your Bishop has instructions about how this term is to be translated.  

Your Bishop will be the final authority regarding the translation. It is your responsibility to provide him with good information so that he can make decisions.

#### STEP 6. After making a tentative decision, TEST the term you have tentatively chosen by using it in your translation.  

In this way you will discover whether it does communicate the right meaning in the different contexts in which the term occurs.  

Questions to ask when testing: How is the term understood by non-Christians? Is the term acceptable to church leaders and others?  

In any case where there is a disagreement, go slowly in making a decision. Keep an open mind. Give people an opportunity to express their views. Prayer and reasoned discussion will result in friendly agreement and in right decision.  

It is helpful to prepare a list of special biblical terms in order that these can be studied by reviewers and other church leaders. In this way people will have an opportunity to make suggestions. When an agreement has been reached, the list can be distributed to clergy, cantors, teachers and interpreters in the language in order that everyone should begin to use the term consistently.  

---  

#### WARNING:  

The list should contain only those terms that have only one sense. It should not include terms that are likely to be translated in different ways in different passages.  

---

Look up the list of KEY LITURGICAL TERMS in the Appendix. Start to fill it in with some tentative proposals for ways to translate these terms in your language.  

---  

#### REMEMBER:  

When deciding how to translate special terms:  

1. First STUDY the meaning of the original term well.  
1. COMPARE the term with other biblical terms that have similar meaning. Words with similar meanings are listed near each other in the list below.  
1. THINK about possible solutions for re-expressing the idea in the Receptor Language and tentatively CHOOSE the most appropriate. Consider the different possible ways of translating unknown ideas.  
1. TEST the term you have tentatively chosen by using it for some time.  

> **EXERCISE 1**

> For each of the words listed below, make a study of the meaning of that word in liturgical service books. 

> For each word, make a short list of the facts about the meaning of the word which you feel are most important to consider when translating. Words to study:

>> (a) sacrament 

>> (b) bishop  

>> (c) epitrachelion

>> (d) gospel  

>> (e) litany  

>> (f) kathisma  

>> (g) chrismation  

>> (h) eucharist

--- 

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, pages 68 - 71