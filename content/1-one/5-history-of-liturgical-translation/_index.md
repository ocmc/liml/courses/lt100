+++
title = "History Of Liturgical Translation"
date = 2022-07-23T15:44:34-07:00
weight = 20
chapter = true
pre = "<b>5. </b>"
+++

### Chapter 5

# History of Liturgical Translation



- LXX
- Ancient Translations of Scripture
- Cyril and Methodios
- 14th - 19th centuries
- modern era

