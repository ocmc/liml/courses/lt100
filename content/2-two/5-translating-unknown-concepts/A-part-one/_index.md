+++
title = "A. How to Translate Unknown Ideas - Part One"
date =  2021-09-14T13:53:47-07:00
weight = 5
+++

Think about the differences between life in the places and times where the Bible was written, where liturgical texts where written, and life in the country where you live today. There is a difference in **time**-- many of the hymns were written centuries ago, and the Psalms were written over two thousand years ago. There are differences of **geography** and **culture.**  

Because of these differences, there are some things mentioned in the Bible and in liturgical texts which are not known by the people in the area where the Receptor Language is used. For example:  

For many people in Africa the following will be "unknown ideas": snow, wolf, chariot, angel, centurion, synagogue.  

For people in Alaska, sheep and locusts are unknown.  

There will also be many **unknown customs**; for example, the custom of resting on a housetop, or of washing the feet of a visitor.  

The most common groups of unknown ideas are:    

* **names of animals** such as bear, camel  
* **names of plants and trees** such as grapevine, fig, sycamore, oak, hyssop, cumin, dill  
* **features of geography** such as desert, sea  
* **weather differences** such as snow, ice, summer, winter  
* **money and measurements** such as denarius, pound, bushel, mile  
* **things that people wear** such as crown, helmet, breastplate  
* **housing and household objects** such as cornerstone, upper room, millstone, scales  

Other very important groups are:  

* **unfamiliar names of places and people:** If someone hears a name which is unknown to him (for example, Ataparo), he will not know if it is the name of a town or a country or perhaps even a person. These will be discussed in Appendix 1 at the end of this chapter.  

* **weights and measurements and money systems:** for example, bushel, cubit, denarius, talent. These will be discussed in Appendix 2 at the end of this chapter.  

* **special biblical terms** referring to the religion of the Jews: for example, Temple, synagogue, priest. These will be discussed in Chapter 9.  

* **special theological terms** referring to the Orthodox Christian faith: for example, supersubstantial, oblation, dynamis, catechumen, incorruption, liturgy

* **technical liturgical terms** for example, antimension, antiphon, presbyter, litany, censer, aer, paten

Not all of these ideas are unknown in all areas. This will depend on each individual area. For example, camels are well known in Northern Nigeria, but unknown in Papua New Guinea. By contrast, the sea is well known in the Solomon Islands, but unknown in Northern Nigeria. Notice too that many ideas and customs that are unknown in Northern Europe are known in Africa because the culture is often more similar to that of Palestine or the Mediterranean.  

When an idea is **known** to the people of the area, your task as a translator is to choose the right word or expression to refer to that idea. But when the idea is **unknown** to the people, then your task is harder. You must find a way to communicate a completely new idea. You have to help people understand something that is previously outisde their experience.  

---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, pages 35 - 36.