+++
title = "The Qualities of a Good Translation"
date = 2020-08-06T12:43:51-07:00
weight = 45
chapter = true
pre = "<b>12. </b>"
+++

### Chapter 12

# The Qualities of a Good Translation

## **REMEMBER:**

A good translation should be:  

### **ACCURATE**

> The translator must re-express the meaning of the original message **as exactly as possible** in the language into which he is translating.

### **CLEAR**  

> The translation should be **clear and understandable.** The translator aims to communicate the message in a way that people can readily understand.  

### **NATURAL**  

> A translation should not sound "foreign."  It should not sound like a translation at all, but like someone speaking in the natural, everyday way.  

Thus the three most important qualities of a good translation are:

### ACCURACY  
### CLARITY   
### NATURALNESS

---
From *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 24.  