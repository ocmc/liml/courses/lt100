+++
title = "4.8"
date =  2021-12-14T15:51:15-08:00
weight = 40
+++

#### 8.  Kathisma of Ode 3 for the Sunday of Pentecost 

| τὸ | Πνεῦμα | τὸ | ἅγιον | σήμερον | ἐξ | ὕψους |  
| -- | ------ | -- | ----- | ------- | -- | ----- |  
| to | Pnevma | to | agion | simeron | eks | ipsus |  
| the | spirit | the | holy | today | from | height |  

&nbsp;

| κατῆλθεν | ἐπὶ | τὸν | οἶκον | τῶν | Μαθητῶν |  
| -------- | --- | --- | ----- | --- | ------- |  
| katilthen | epi | ton | ikon | ton | mathiton | 
| came down | on | the | house | of the | disciples |  

**SOT:** The Holy Spirit today came down from the height on the house of the disciples  

**LASH:**  the Holy Spirit came down to-day upon the house of the Disciples  

**GEV:**  Today, the Holy Spirit came from high up down to the disciples' house

**MOT:**  Today, the Holy Spirit came down from heaven into the house where Jesus' disciples were together  

---

* **How is οἶκος ikos ‘house’ translated in these versions?**  

* **What is the real meaning of οἶκος ikos ‘house’ in this hymn?**  

* **What word(s) would you use in translating οἶκος ikos ‘house’ into your own language, for this hymn?**