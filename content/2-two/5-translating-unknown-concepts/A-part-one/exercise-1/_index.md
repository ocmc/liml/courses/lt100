+++
title = "Exercise 1"
date =  2021-09-16T13:00:14-07:00
weight = 15
+++

For the **bold text** in each of the following passages,

(1) Underline any word which expresses an idea that is unknown to people of your own area,  
(2) In English, re-express that word using a descriptive phrase.  

Use your DICTIONARY or GLOSSARY to check the meaning of the words wherever necessary. Where necessary, look up related Biblical passages if there are any. Or look up the saint or event being commemorated in the text. Remember to describe that part of the meaning of the word which is important in the particular context.  

1. Your tongue, Father, was **truly the pen of a scribe swiftly writing,** teaching a knowledge wholly devout, **engraving on the tablets of hearts the law of the Spirit.**  
-- *Canon for St Ephraim the Syrian, Ode 6*  (See Exodus 31:18 and 34:1-4)
1. When it saw you crucified, O Christ, all creation trembled; the foundations of the earth quaked with fear of your might. For when you were lifted up today, the Hebrew race perished; the veil of the Temple was rent apart; the graves were opened, and the dead arose from the tombs. **The Centurion, seeing the marvel, was afraid;** while your Mother, standing by, cried out, lamenting as a mother, ‘How should I not lament, and beat my breast, as I see you naked as one condemned, hanging on a tree?’ Crucified, buried and risen from the dead, Lord, glory to you!  
-- *Lauds for Matins of Holy Friday* (See Matthew 27:54, Mark 15:38, Luke 23:47)    
1. Creation, why do you lament **as you behold God the Master of life on a Cross and on a bier?** For he will arise and radiantly renew you by his rising on the third day, having destroyed death and raised with himself those who sing his praise.  
-- *Canon for the Sunday before the Nativity, Ode 9*  
1. While, in the words of the psalm, **you stand, pure Virgin, as Queen on the right-hand of the King who shone forth from your womb**, beseech him to make me stand at his right hand on the day of recompense, O Bride of God.  
-- *Canon for St John the Theologian, Ode 1*  
Note: the word "queen" has several senses in English. It may mean "a woman who rules a country" (like Queen Elizabeth, the queen of England.) Or it may mean a close relative of a king-- especially a king's mother or a king's wife. Make sure you translate according to the correct sense in each context.  
1. Venerable Father, all-honoured Gregory; axe that fells the assaults of heretics, two-edged sword of the Advocate; knife that cuts out adulterated seeds; fire that burns up the undergrowth of heresies; **true farmer's winnowing fan** by which the weight and lightness of doctrines is well distinguished; measuring rod of strict accuracy that directs all into paths of salvation. Ever entreat Christ, implore Christ, who refashioned the world in the streams of Jordan, to save our race.  
-- *Vespers Stichera for St Gregory of Nyssa*  
1. The king of Edessa, recognising you as King of all things, who do not offer sceptre and army, but with a word multitudes of wonders, implored you, the God-man, to come to him. **But seeing your impress on the towel** he cried out, ‘You are Lord and God’.  
-- *Kathisma of the Canon for the Icon Not Made With Hands* (See the Synaxarion for August 16)  
1. O marvellous wonder! **The pure Mother of God each day unfolds her Protecting Veil,** and with her motherly care and grace manifestly protects from every assault those who look to her and cry: Hail, Full of grace, protection and help of the your race that faithfully magnifies you, O Virgin.  
-- *Vespers Stichera for the Protecting Veil* (See the Synaxarion for October 1)  
1.  The earth shuddered, the sun was turned back and with it the light was darkened, **the sacred veil of the Temple was rent in two** and the rocks were split; for the Just One had been made away with through a cross; the God of our Fathers. praised and glorified above all.  
-- *Resurrection Canon in the First Mode, Ode 7* (See Exodus 16:31-33, Matthew 27:50-53, Mark 15:37-38, Luke 23:44-46)  
1. The beauty of your unapproachable glory, O Unity of triple sun, the Seraphim cannot bear to see **and veil themselves with their wings** and with thrice-holy songs unceasingly glorify you.  
-- *Midnight Office Canon in the Fourth Mode, Ode 1* (See Isaiah 6:2)  
1. **Worship of pagans** and temples of demons you hurled to the ground with the weapon of faith, and you were offered to the heavenly temple as living treasures, all-honoured Martyrs.  
-- *Canon for September 1, Ode 1*  
1. Blessed and all-wise John, with fervent abundance of the love of Christ you appeared more loved than all Disciples; to the all-seeing Word, **who judges the whole inhabited world by just scales of purity and chastity,** shining brightly with beauties in body and mind, O blessed of God.  
-- *Lauds for St John the Theologian*  
1. So that we might be chastened you provoked **an assault by barbaric cavalry** with all their host, do you shatter it yourself and their violence against us; and fight with the King who has shown his trust in you, who achieve all things, at the confident prayers of the Fathers whose memory we celebrate.  
-- *Canon for the Holy Fathers, Ode 9*  
1. The company of the elect, **armed with the breastplate of hope and love** and fenced about by faith, rejoiced as they nobly endured all the threats of tyrants, the tortures and scourges, for as outstanding witnesses of truth their wealth was Christ who gives victory in contests.  
-- *Canon for Sunday of All Saints, Ode 9*  

---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, pages 37 - 38.