+++
title = "Exercise 16"
date =  2021-11-25T10:28:46-08:00
weight = 5
+++

In each of the following texts, for each name, fill in the general word that is appropriate. Use a dictionary or map to check the accuracy of your information.

1. All-wise Fathers, you rightly over­threw _____ Honorius, once greatly puffed up on the throne and rule of the ______ of old Rome, and finally proclaimed him unworthy of both throne and rule. (Ode 4 of the Matins Canon of Sunday of the Fathers of the Fourth Ecumenical Council)
1. ...I have fallen together with a sea monster of evils; but bring me up from corruption, O God, as you one brought _______ Jonas... (Ode 6 of the Matins Canon of Wednesday in the Fourth Mode)
1. ...O Jesus, who traced the tables of the law on ______ Sinai for your servant _____ Moses. (Ode 9 of the Matins Canon for the Sunday of the Myrrh-Bearing Women)

---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 51.  