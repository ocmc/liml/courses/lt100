+++
title = "B. From Meaning to Meaning"
date =  2021-09-08T12:19:55-07:00
weight = 10
+++

Your aim as a translator is to communicate the meaning of the source text. The grammar and expressions of each language are different, so you cannot just translate the Source Language words, one by one. You have to think of the meaning of the message you are translating.  

Translation can be diagrammed like this:  

![TwoStepsInTranslating](https://gitlab.com/ocmc/liml/courses/lt100/-/raw/main/images/TwoStepsInTranslating.png)

<details>
{{<mermaid align="left">}}
graph TD;
    A{SOURCE TEXT} -->|Discover the meaning| B((MEANING))
    B --> |Re-express the meaning| C[RECEPTOR LANGUAGE TRANSLATION]

{{< /mermaid >}} </details>

Thus, there are two steps in translating:  

STEP 1:  Study the source text and discover the meaning that is expressed by the words and grammatical patterns of the Source Language.  

STEP 2: Re-express that meaning using different words and grammatical patterns. The meaning should be expressed in a way that is clear and natural in the Receptor Language.  

The **form** will be different, but the **meaning** must be the same.  

Never leave out STEP 1! Never translate before you have studied the meaning. If you do, the result will always be a bad translation.  

