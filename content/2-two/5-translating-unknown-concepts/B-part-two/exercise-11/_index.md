+++
title = "Exercise 11"
date =  2021-10-04T13:48:51-07:00
weight = 25
+++

Imagine you are translating into a language which has no word for the ideas which are highlighted in the following passages. Re-express each bolded  idea using a more specific word or words, without changing the meaning of the passage. Look up the passage in its context.  

1. I am held fast by an abyss of sins, O Christ, and am storm-tossed on the sea of life. But as you did Jonas from the **beast**, bring me up too from the passions, O Saviour, and save me.  
-- Canon for the Myrrhbearers, Ode 6  (See Jonah 2:1-2)  

1. From agonising pain in the feet, paralysis of knees, fracture of **limbs** and joints, weakness of hands and feet, rescue me, O All Pure, and from the harm that comes from different fluxes, keeping me undefiled by all of these by the love of my God and Fashioner.  
-- Vespers Stichera for Wednesday in the First Mode  

1. live in newness of life... let us discipline all our **members**.
-- Vespers Stichera for the Forefeast of the Elevation of the Holy Cross, Sept 13


---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 46.  