+++
title = "Case Study: Spiritual Entities"
date =  2022-03-14T13:01:32-07:00
weight = 50
+++

Liturgical texts have a variety of words for spiritual entities. These could both be beings without material bodies (like an angel or a demon or the Holy Spirit), or a part of a being (like a soul, a spirit, or a mind).

1. Study the following texts from the Baptism service and underline each word that refers to a spiritual entity.    
2. Look these words up in a bible dictionary or liturgical lexicon.
3. Write down the ways in which these words are similar to, or different from, each other.
4. Write down as many words as you can think of in your language for spiritual entities. How does the list of words in your language compare to the list of words in English?

All translations are by Fr. Ephraim Lash

1. **First Exorcism**  

The Lord rebukes you, O Devil, the Lord who came into the world and dwelt among mortals so that he might destroy your tyranny and deliver humanity; the Lord who on the Tree crushed the hostile powers, when the sun was darkened, the earth shaken, the graves opened and the bodies of Saints arose; the Lord who by death abolished death and destroyed the one who had the power of death, that is you, the Devil. I adjure you by God, who revealed the tree of life and set in place the Cherubim and the flaming sword which turned this way and that to guard it: Be rebuked and withdraw! I adjure you by the One who walked on the surface of the sea as on dry land and rebuked the tempest of the winds, the One whose gaze dries up the deeps and whose curse melts mountains. For it is he who now commands you, through us: Be afraid, come out, withdraw from this creature and return no more. Do not hide in him/her, nor encounter him/her, nor influence him/her either by night or day, early or at noon. But go back to your own Tartarus until the great day of the judgement that has been prepared. Be afraid of God, who is seated upon the Cherubim and looks upon the deeps; before whom Angels, Archangels, Thrones, Dominions, Principalities, Authorities, Powers, the many-eyed Cherubim and the six-winged Seraphim tremble; before whom heaven and earth, the sea and all that is in them tremble. Come out, and withdraw from the sealed and newly-enlisted soldier of Christ our God. For it is by him that I adjure you, the One who walks on the wings of the winds, who makes his Angels spirits and his ministers a flaming fire. Come out, and withdraw from this creature with all your power and your angels. For the name of the Father and of the Son and of the Holy Spirit has been glorified, now and for ever, and to the ages of ages.  

2. **Third Exorcism**  

Lord Sabaoth, God of Israel, who heal every disease and every sickness, look upon your servant, search out, seek and drive from him/her all the activities of the devil. Rebuke the unclean spirits and expel them, and cleanse the work of your hands; and using your swift force, crush Satan speedily under his/her feet and grant him/her victories against him and all his unclean spirits, so that, obtaining mercy from you, he/she may be found worthy of your immortal and heavenly Mysteries and may give glory to you, the Father, the Son and the Holy Spirit, now and for ever, and to the ages of ages.

3. **Prayer at the Blessing of the Font**  

We pray you, Lord, let all airy and invisible spectres withdraw from us, and do not let a demon of darkness hide itself in this water, and do not let an evil spirit, bringing darkening of thoughts and disturbance of mind, go down into it with the one who is being baptized. But do you, Master of all things, declare this water to be water of redemption, water of sanctification, cleansing of flesh and spirit, untying of bonds, forgiveness of offences, enlightenment of soul, washing of rebirth, renewal of spirit, gift of adoption, garment of incorruption, source of life. For it was you, Lord, who said, 'Wash and be made clean, and put away evils from your souls.' It is you who have given us the grace of rebirth from on high through water and Spirit. Manifest yourself, Lord, in this water, and grant that the one being baptized in it may be transformed for the putting off of the old self that is corrupted after the desires of deception, and may put on the new that is renewed after the image of the One who created him/her. So that, planted in the likeness of your death through Baptism, he/she may also become a partaker in your Resurrection, and having guarded the gift of the Holy Spirit and increased the deposit of grace, may receive the prize of his/her high calling and be numbered with the firstborn, whose names are inscribed in heaven, in you our God and Lord, Jesus Christ.