+++
title = "Exercise 7"
date =  2021-09-14T10:26:19-07:00
weight = 50
+++

Translate the following poems into your own language. Focus on translating the meaning, even if the translation is not in the same poetic form as the English version.  

As in previous exercises, underline places in your translation where you have used expressions or grammar which are different from the English version. Underline places in your translation where the literary form (rhyme, meter, etc) is different from the English poem. Discuss these with an instructor or colleague. Also, if possible, discuss your translation with other speakers of the language.

**from *OMEROS***
**by Derek Walcott**

The sky cracked asunder   
and a forked tree flashed, and suddenly that black rain   
which can lose an entire archipelago  

in broad daylight was pouring tin nails on the roof,   
hammering the balcony. I closed the French window,   
and thought of the horses in their stalls with one hoof  

tilted, watching the ropes of rain. I lay in bed  
with current gone from the bed-lamp and heard the roar   
of wind shaking the windows    

---

**from *SONG OF LAWINO***  
**by Okot p'Bitek**  
**translated by the author from the Acoli-language original *Wer pa Lawino***  

O how young girls  
Labour to buy a name!  
You break your back  
Drawing water  
For the wives  
Of the teachers,  
The skin of your hand  
Hardens and peels off  
Grinding millet and simsim.  
You hoe their fields,  
Split firewood,  
You cut grass for thatching  
And for starting fires,  
You smear their floors  
With cow dung and black soil  
And harvest their crops.

---

**from *KRISHNAKALI***  
**by Rabindranath Tagore**

Her cows were lowing in the meadow,  
when the fading light grew grey.  
With hurried steps she came out  
from her hut near the bamboo grove.  
She raised her quick eyes to the sky,  
where the clouds were heavy with rain.  
Ah, you call her dark! let that be,  
her black gazelle eyes I have seen.  

---

**from *EVANGELINE***  
**by Henry Wadsworth Longfellow**  

This is the forest primeval. The murmuring pines and the hemlocks,  
Bearded with moss, and in garments green, indistinct in the twilight,  
Stand like Druids of eld, with voices sad and prophetic,  
Stand like harpers hoar, with beards that rest on their bosoms.  
Loud from its rocky caverns, the deep-voiced neighboring ocean  
Speaks, and in accents disconsolate answers the wail of the forest.  

---

**from *THE BALLAD OF THE WHITE HORSE***  
**by G. K. Chesterton**

Before the gods that made the gods  
Had seen their sunrise pass,  
The White Horse of the White Horse Vale  
Was cut out of the grass.  

Before the gods that made the gods  
Had drunk at dawn their fill,  
The White Horse of the White Horse Vale  
Was hoary on the hill.  

Age beyond age on British land,  
Aeons on aeons gone,  
Was peace and war in western hills,  
And the White Horse looked on.  

For the White Horse knew England  
When there was none to know;  
He saw the first oar break or bend,  
He saw heaven fall and the world end,  
O God, how long ago.  

For the end of the world was long ago,  
And all we dwell to-day  
As children of some second birth,  
Like a strange people left on earth  
After a judgment day.  

{{% notice note %}}
**REMEMBER:**  
When checking a translation, ask:  
**Is it accurate?** Is the meaning as nearly as possible the same meaning that the original author intended?  
**Is it clear?** Will people understand the message?  
**Is it natural?** Is this the way that people speak?
{{% /notice %}}


---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 33.