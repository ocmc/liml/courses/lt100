+++
title = "4.3"
date =  2021-12-14T15:45:28-08:00
weight = 15
+++

#### 3.  Ode 4 of the Matins Canon for St Eumenius the Wonderworker, September 18

| Οἶκος | γέγονας | Πνεύματος, | θείοις | ναοῖς | σχολάζων | Ἱεράρχα, | καὶ | σεπταῖς | μελέταις | ὡραϊζόμενος. |  
| ----- | ------- | ---------- | ------ | ----- | -------- | -------- | --- | ------- | -------- | ------------ |  
| Ikos | gegonas | Pnevmatos, | Theiis | nais | s-holazon | Ierarha | ke | septes | meletes | oraizomenos. |  
| House | you became | of the Spirit | to divine | temples | devoting yourself | Hierarch, | and | with sacred | care | adorning yourself. |  


**SOT:** You became a house of the Spirit devoting yourself to divine temples, Hierarch, and with sacred care adorning yourself.  

**MOT:** O Hierarch, you devoted yourself to worship in the holy churches, and your pious worship was like sacred clothing. By this, your soul became a home for the Holy Spirit.  

**GEV:** You became home to the Holy Spirit, O Hierarch, as you devoted yourself to divine temples and clothed yourself with sacred care. 

---

* **How is οἶκος ikos ‘house’ translated in these versions?**  

* **What is the real meaning of οἶκος ikos ‘house’ in this hymn?**  

* **What word(s) would you use in translating οἶκος ikos ‘house’ into your own language, for this hymn?**

