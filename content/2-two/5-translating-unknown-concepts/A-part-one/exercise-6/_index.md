+++
title = "Exercise 6"
date =  2021-09-22T13:51:56-07:00
weight = 50
+++

List five examples of adopted words in:  
(1) Your own language.  
(2) Any other language which is known to you (preferably a national language other than English).  

### EXAMPLE

The following is a translation of Mark 1:1-4 in an early Bible version in a Nigerian language. (The spelling has been slightly modified.) Underline any foreign words that you recognize in the translation. What problem do these words give to the reader?  

1. Eritono gospel Jesus Christ Eyen Abasi.  
1. Kpa nte ewetde ke nwed prophet Isaiah, ete,  
Sese, mmodon isunutom mi ebem Fi iso,  
Emi edinamde usun Fo;  
1. Uyo anifiori ke desert, ete, Mbufo edion usun Jehovah,  
Eneng ere okpousung Esie;  
1. John oto edi edinim owo baptism ke desert, edinyun okworo baptism erikabare esit, man efen mme idiok-knpo.  

The **disadvantage** of using foreign words in a translation is that many ordinary people do not know what the words mean. The words are unfamiliar to them, and so they mean nothing. The use of foreign words should, therefore, be avoided in translation as much as possible.  

There are, however, some places where it does seem best to use a foreign words. In these cases the foreign word should be introduced in such a way as to give the reader some general idea of its meaning:  
(a) Either use the foreign word together with a descriptive phrase (solution 1) that explains its meaning, *or*  
(b) Use the foreign word together with a more general word that tells the reader what kind of thing the foreign word is.  

Examples of some possible translations are as follows:  

Mark 1:6  a garment made with the hair of **an animal called "kamel"**  
In this example, the more general word "animal" provides the information that a camel is a kind of animal.  

Mark 14:1 **the festival they call "Passover"**  
The more general word "festival" shows that the foreign word "Passover" is the name of a particular festival. In this way, at least some of the meaning is communicated.  

It is essential to consider carefully what aspect of the meaning is relevant and importand for each particular context. Compare, for example, the following three references to **myrrh.**  

In the first of these, it is the property of myrrh as a medicine that is most important. In the second, it is the fact that myrrh was a costly gift such as kings would give and receive (there may also be further symbolism). And in the third, it is the fact that myrrh smells sweet that is relevant.  

Mark 15:23  And they offered him wine mingled with a **medicine called "myrrh."**  
Matthew 2:11 they offered him **costly** gifts: gold and frankincense and **myrrh.**  
Song of Solomon 1:13 My beloved is to me like a bag of **sweet-smelling myrrh**, that lies between my breasts.  



---

From *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 41 - 42.  