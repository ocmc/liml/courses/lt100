+++
title = "Exercise 7"
date =  2021-09-22T14:33:10-07:00
weight = 55
+++

In each of the following passages:  

(1) Underline any idea which might be unknown to people of your own area.  
(2) Consider how you might translate this idea using a foreign word together with either a descriptive phrase or a more general word that communicates the most relevant part of the meaning of the word.  

1. Numbers 11:5, 6  
We remember the fish we ate in Egypt for nothing, the cucumbers, the melons, the leeks, the onions, and the garlic; but now our strength is dried up, and there is nothing at all but this manna to look at.  
1. Numbers 11:31  
And there went forth a wind from the Lord, and it brought quails from the sea, and let them fall beside the camp.  
1. Numbers 13:23  
And they came to the Valley of Eshcol, and cut down from there a branch with a single cluster of grapes, and they carried it on a pole between the two of them; they brought also some pomegranates and figs.  
1. Isaiah 41:19  
I will put in the wilderness the cedar, the acacia, the myrtle, and the olive; I will set in the desert the cypress, the plane, and the pine together;  
1. Matthew 23:23 Woe to you, scribes and Pharisees, hypocrites! for you tithe mint and dill and cumin, and have neglected the weightier matters...  




---

From *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 42 - 43.  