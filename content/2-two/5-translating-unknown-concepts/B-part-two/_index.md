+++
title = "B. How to Translate Unknown Ideas - Part Two"
date =  2021-09-23T13:07:20-07:00
weight = 10
+++

In addition to the three ways of translating unknown ideas that have been described in Part 1, there are two other possible methods:  

4. Use a word that is more general in meaning  
5. Use a word or phrase that is more specific in meaning   

---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, pages 45 - 46.  