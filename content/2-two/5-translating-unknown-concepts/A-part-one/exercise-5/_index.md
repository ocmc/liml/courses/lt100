+++
title = "Exercise 5"
date =  2021-09-22T13:47:37-07:00
weight = 45
+++

In the following passage:  

(1) Underline three examples of words that have been **adopted** into English.  
(2) Put a circle around three examples of **foreign** words.  

> John went into the hotel and sat down. He took up the menu and began to choose what he would eat. The choice was between tin-sam and salad, or laksa, or gado-gado, with something called "escargots." He had decided to order a laksa with a cup of coffee when he saw Kim Sai enter the restaurant. She was wearing a song-sam and holding a wok in her hand.  

In this exercise, you may find it difficult to recognize the words that have been adopted into English. This is because words that have been adopted have become just like other words of the language. Unless you happen to know something about the history of the language, it is not possible to tell that they have been adopted.

---

From *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 41.  