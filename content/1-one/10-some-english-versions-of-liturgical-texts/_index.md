+++
title = "Some English Versions of Liturgical Texts"
date =  2020-08-06T12:26:07-07:00
weight = 40
chapter = true
pre = "<b>10. </b>"
+++

### Chapter 10

# Some English Versions of Liturgical Texts

There are several different translations of Orthodox Christian service books in English, and there are many different translations of the Bible in English. Nearly all English translations of the New Testament have been translated from Greek. Most English translations of the Old Testament and the Psalter have been translated from Hebrew, but some are based on the Greek Old Testament.  