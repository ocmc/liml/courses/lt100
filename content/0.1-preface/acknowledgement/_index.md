+++
title = "Acknowledgements"
date =  2021-08-25T15:34:30-07:00
weight = 5
+++

This course is adapted by the Translation Program of the Orthodox Christian Mission Center (OCMC Translation Program) from the third (2017) and fourth (2020) editions of a book called *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, SIL International Publications.

The book written by Katharine Barnwell is an introductory course in translating the Bible. Many of the principles used in Bible translation are used in translating Orthodox Christian liturgical texts. Katharine Barnwell's book teaches these principles very well. Some of the examples and exercises in this course are borrowed from Katharine Barnwell's book. Many of the explanations of translation principles are quotations or adaptations of Katharine Barnwell's text. All quotations or adaptations of Barnwell are acknowledged on the pages where they occur.

The OCMC Translation Program has created this course by supplementing Barnwell's work with examples and exercises from the liturgical texts of the Orthodox Church. We have also added explanations of translation principles that are specific to Orthodox Christian liturgical translation.  