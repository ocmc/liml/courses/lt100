+++
title = "Important"
date =  2022-01-24T11:02:03-08:00
weight = 5
+++

For each translation project, the situation will be different.  

1. In some areas, there may be as yet not Christians, or very few, and no terms are yet in use for these special biblical terms.  
1. In some areas, there may be many Christians, and the Bible has already been translated into your language, but it is mostly used by people who are not Orthodox. Certain biblical terms may already be in common use. However, these terms still need to be reviewed and carefully evaluated to check whether they are in fact the best terms to communicate accurately the original meaning.  
1. In some areas, there may be many Orthodox Christians, but the liturgical texts have not been translated into their languages. Orthodox Christians might be familiar with special biblical terms from another language like Greek or English or Kiswahili. These "imported" terms might be different from words used by the Bible in your language. (For example, you might use a word in Liturgy that sounds like "baptize," but the Bible in your local language might use a local word that means "immerse in water.") These terms will need to be reviewed. You will have to decide whether it's better for special terms in your language's liturgy to sound more like the Bible in your language, or to sound more like special terms in other languages used in worship.  

Where some terms are already in common use, common problems that occur are:  

a. There are several words in use for a particualar idea. Sometimes people living in one town or region use one word, while people living in a different place or dialect aread use a different word. Sometimes different Christian confessions are using different words. The translator will need to decide which of these words should be used in the translation.  
b. It may sometimes happen that the word that is in general use in the church is not a good word because it has some wrong meaning.  

Notice that the choice of SPECIAL BIBLICAL TERMS is something about which many people feel very strongly. Decisions should be made in careful consultation with Church leaders and others.  

---  

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 68    