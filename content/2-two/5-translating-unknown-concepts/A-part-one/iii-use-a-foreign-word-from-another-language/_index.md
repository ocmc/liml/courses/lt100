+++
title = "iii. Use a Foreign Word From Another Language"
date =  2021-09-22T13:41:57-07:00
weight = 40
+++

A **foreign word** is a word taken from another language. Foreign words are words that are not usually used by speakers of the language.  

A **foreign word** is different from an **adopted word**.  

What is an **adopted word**? An adopted word is a word originally taken from another language, that has become a part of the language. All languages adopt words from other languages in this way. Usually they take them from major languages that are spoken in the area, to refer to ideas that have recently become known. For example, English has taken many loan words from Latin, Greek, and French. Kiswahili has taken many loan words from Arabic, Portuguese, and English.

Words are adopted over a period of time. Gradually the word is used so much that everybody comes to know its meaning. Everybody uses it, even those people who do not know the language that it comes from. It is pronounced just like other words in the language. Words that have been adopted into a language in this way are no longer foreign words.  

Adopted words are also sometimes called "borrowed words" or "loan words."  

The present discussion is not concerned with words that have been adopted in this way, but with words that are still foreign and are not known to speakers of the Receptor Language.  

---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, pages 40 - 41.