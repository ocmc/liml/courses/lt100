+++
title = "4.6"
date =  2021-12-14T15:49:20-08:00
weight = 30
+++

#### 6. Anaphora of St Basil the Great, after "All of Creation Rejoices in You"

 | Αὐτὸς | τοῖς | πᾶσι | τὰ | πάντα | γενοῦ, |  
 | ----- | ---- | ---- | -- | ----- | ------ |  
 | aftos | tis | pasi | ta | panta | genu |  
 | [your]self | to | all | the | all | be |  

 &nbsp;
 
 | ὁ | εἰδὼς | ἕκαστον, | καὶ | τὸ | αἴτημα | αὐτοῦ, |  
 | - | ----- | -------- | --- | -- | ------ | ------ |  
 | o | idos | ekaston | ke | to | etima | aftu |  
 | the | knowing | each | and | the | request | his |  

 &nbsp;
 
 | οἶκον, | καὶ | τὴν | χρείαν | αὐτοῦ. |  
 | ------ | --- | --- | ------ | ------ |  
 | ikon | ke | tin | hrian | aftu |  
 | house | and | the | need | his |  

 **SOT:** [your]self to all be all, knowing each and his request, house, and his need.  

**LASH:** Be all things to all people, you who know each and the request of each, their household and their need.  

**HC:** Be all things to all, You who know each person, his requests, his household, and his need.  

**GEV:** You be all things for all people, since you know each person, their request, their household, and their need.  

**MOT:** You, yourself, be everything for everyone, since you are the one who knows each person and who knows what each person asks for, each person's household and each person's need.  

---

* **How is οἶκος ikos ‘house’ translated in these versions?**  

* **What is the real meaning of οἶκος ikos ‘house’ in this hymn?**  

* **What word(s) would you use in translating οἶκος ikos ‘house’ into your own language, for this hymn?**

