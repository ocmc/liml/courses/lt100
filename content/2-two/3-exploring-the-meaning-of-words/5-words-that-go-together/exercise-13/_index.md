+++
title = "Exercise 13"
date =  2021-12-23T13:52:20-08:00
weight = 5
+++

Translate the following sentences into your own language. Note any places where the natural collocation of words is different from English.  

1. He ate an orange.  
1. He ate some sugarcane.  
1. He put up an umbrella.  
1. He built a shelter.  
1. He smoked a cigarette.  
1. He loaded his gun.  
1. He has a cold.  
1. He is cold.  
1. He is hungry.  
1. He made a sacrifice.  
1. He made an idol.  
1. He gave the children his advice.  
1. He gave way to his brother's appeal.  
1. She gave birth to a son.  



---

From *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 64