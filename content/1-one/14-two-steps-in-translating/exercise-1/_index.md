+++
title = "Exercise 1"
date =  2021-09-09T15:12:08-07:00
weight = 20
+++

Take any short story or text in your own language. It could be the story you have written in WRITING PRACTICE sessions, or any story. Make a translation of this story or text into English or into any other national language.  

Then underline places in your translation where the grammar or idiomatic expressions you have used are different from those of the version in your own language. Discuss these with your advisor or with a colleague.

{{% notice note %}}
**REMEMBER:**  
When checking a translation, ask:  
**Is it accurate?** Is the meaning as nearly as possible the same meaning that the original author intended?  
**Is it clear?** Will people understand the message?  
**Is it natural?** Is this the way that people speak?
{{% /notice %}}

---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 32.  