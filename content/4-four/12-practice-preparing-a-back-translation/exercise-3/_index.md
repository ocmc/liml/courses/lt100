+++
title = "Exercise 3"
date =  2024-04-11T11:39:56-07:00
weight = 10
+++

### Exercise 3: Making Back-Translations from Kiswahili into English

Make a word-for-word key of the following prayer. 

* Do not look at any text except for the prayer that you are back-translating. The back-translation should reflect ONLY what this text says.
* Where there is an idiom or figure of speech, put the literal form in your word-for-word key and the actual meaning in brackets. Here is an example:

| Mungu | Mtakatifu | Mweza | Mtakatifu | Usiyekufa | Mtakatifu | utuhurumie |
| ---- | ----- | ------ | ---- | ---- | ---- | ---- |
| God | Holy | Abled/ Mighty | Holy | That-cannot-die | Holy | Have-mercy-on-us |

Holy God, Holy Mighty, Holy Immortal, have mercy on us.

* Include grammatical information in square brackets, where it is needed. For example, indicate whether a pronoun back-translated as "you" is singular or plural.
* Where two or more words are needed to back-translate one word in the translation, link the words with hyphens.
* Where two or more alternative words are needed to indicate the area of meaning of a word in the translation, separate these words with a slash. Examples: love/kindness, he/she.
* If words from the language you are back-translating from are included in your back-translation because they seem untranslatable, put an explanatory note enclosed in square brackets after the word.

> Pia Mfalme wa huruma, sikia sauti ya maombi yangu wakati huu na Siku hii Takatifu, na kila wakati katika kila mahali; na hata kwa hawa, watumishi Wako, wanao ugua nafsi na mwili, kidhii uponyaji Wako, wasamehe dhambi, na usamehe uasi wao wanaojua na wasiojua, ukiwaponya vidonda ndugu, kila maradhi na udhaifu. Wape afya ya nafsi, Wewe, Uliye guza mama mkwe wa Petro, homa ilipomwacha; naye akaondoka, akakutumikia.

Your key should look something like this:

| | | | | | | |
| ---- | ---- | ---- | ---- | ---- | ---- | ---- |
| | Pia | Mfalme | wa | huruma, | sikia | sauti |
| *Key* | | | | | | |
| | ya | maombi | yangu | wakati | huu | na | 
| *Key* | | | | | | | 
| | Siku | hii | Takatifu, | na | kila | wakati | 
| *Key* | | | | | | | 
| | katika | kila | mahali; | na | hata | kwa | 
| *Key* | | | | | | | 
| | hawa, | watumishi | Wako, | wanao | ugua | nafsi | 
| *Key* | | | | | | | 
| | na | mwili, | kidhii | uponyaji | Wako, | wasamehe | 
| *Key* | | | | | | | 
| | dhambi, | na | usamehe | uasi | wao | wanaojua | 
| *Key* | | | | | | | 
| | na | wasiojua, | ukiwaponya | vidonda | ndugu, | kila | 
| *Key* | | | | | | | 
| | maradhi | na | udhaifu. | Wape | afya | ya | 
| *Key* | | | | | | | 
| | nafsi, | Wewe, | Uliye | guza | mama | mkwe | 
| *Key* | | | | | | | 
| wa | Petro, | homa | ilipomwacha; | naye | akaondoka, | akakutumikia. |
| *Key* | | | | | | | 