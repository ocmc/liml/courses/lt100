+++
title = "History of Liturgical Texts"
date = 2022-07-23T15:49:23-07:00
weight = 17
chapter = true
pre = "<b>4. </b>"
+++

### Chapter 4

# History of Liturgical Texts

<!--->

- Scripture (Psalms and Odes)
- Ancient hymns
- Anaphoras
- Neo-Sabaitic synthesis
    - Octoechos
    - Menaion
    - Triodion & Pentecostarion

(Taft, The Byzantine Rite, a Short History)

--->