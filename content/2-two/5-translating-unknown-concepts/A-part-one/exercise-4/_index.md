+++
title = "Exercise 4"
date =  2021-09-22T13:11:57-07:00
weight = 35
+++

For each of the passages below,  

(1) First underline any words that refer to ideas that may be unknown to people in your own language area.  
(2) Note down how you might translate each idea, using a word for something similar in your own culture.  

1. Having you, O Prophet, as a sweet-scented rose, a fragrant cypress, a unfading lily, precious myrrh, I, who hasten to your protection, will, by your prayers, be cleansed from the foul stench of works.  
-- *Matins Canon for Tuesday in the Second Mode, Ode 9*  

1. The lawless people dragged you like a sheep to the slaughter, O Christ, who are the Lamb of God, and who wish to rescue from the bitter wolf the sheep whom you loved for you love mankind.  
-- *Matins Canon for Friday in the Fourth Mode, Ode 3*  

1. The whole world, let us praise as its champions the Disciples of Christ and foundations of the Church, the true pillars and bases, and inspired trumpets of the doctrines and sufferings of Christ, the Princes, Peter and Paul. For they passed through the whole breadth of the earth as with a plough, and sowed the faith, and they made the knowledge of God well up for all, showing forth the understanding of the Trinity. O Peter, rock and foundation, and Paul, vessel of election; the yoked oxen of Christ drew nations, cities and islands to knowledge of God. While they have brought Hebrews again to Christ and the intercede that our souls may be saved.  
-- *Great Vespers Liti for Saints Peter and Paul*

1. You have been shown to be a fruitful olive tree in the house of your God, Mother of your Creator, though whom the world has appeared full of mercy; save the ailing with the touch of your intercessions.  
-- *Canon for Holy Unction, Ode 6* 

---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 40.  