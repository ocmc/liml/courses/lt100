+++
title = "Exercise 12"
date =  2021-10-15T13:38:17-07:00
weight = 30
+++

Each of the following passages includes one or more ideas that may be unknown in the Receptor Language area. There may also be some ideas that, though known, may not be easily expressed in the Receptor Language by a single word.

(1) Underline all the unknown ideas, or problem words.  
(2) Write down how you might re-express the meaning of the word or phrase you have underlined in a way that could be readily translated into your language. For each example, state which of the five possible methods for translating an unknown idea you have used. The five methods are:  

* Use a descriptive phrase  
* Substitute something similar  
* Use a foreign word from another language  
* Use a word that is more general in meaning  
* Use a word or phrase that is more specific in meaning  

1. You were born for us from a Virgin, and endured crucifixion, loving Lord. (Vespers Theotokion in the Plagal of the Fourth Mode)  
1. Be dedicated anew brethren! And putting off the old man, live in newness of life, placing a bridle on all those things from which death comes. (Vespers Stichera for Sept 13)  
1. As spiritual shepherds, as lambs of the Shepherd, as sheep of the Lamb and God our Redeemer, O Apostles who saw God, intercede without ceasing that I may be delivered from the spiritual wolf and from the bitter portion of the goats. (Ode 8 of Octoechos Canon for Thursday in the First Mode)  
1. You were revealed as a godlike river for your flock, watering the ploughland of their souls, High Priest, with your godlike gifts. (Ode 5 of General Menaion Canon for a Bishop)  
1. Let not my soul be taken like a sparrow by their teeth (2nd Antiphon at Sunday Matins, Mode 6)  
1. By your virtues you were exalted as a lofty cedar (Ode 3 of the Canon to the Martyr Centurion Cornelius, Sept 13)  
1. The Master of all things, standing in the Temple at the mid-point of the feast of sacred Pentecost, addresses the Hebrews and as King and God clearly refutes their tyrannous daring: but through compassion he gives us all his great mercy. (2nd Matins Kathisma for Mid-Pentecost)  
1. With the spiritual Angels as witnesses of our deeds, let us be drawn to lead pure lives as we cry out: Blessed are you, O Lord, the God of our fathers. (Ode 7 of the Canon for Monday in the Third Mode)  
1. Death has been slain, Hell taken prisoner, those in chains freed by the Resurrection of Christ. (Ode 5 of the Canon for Sunday of the Samaritan Woman)  
1. At the dread assize, without accusers I am convicted, without witnesses I am condemned; for the books of the conscience are opened, and hidden deeds are disclosed; then before you are to examine my actions in that most public theatre, O God, have mercy on me and save me. (2nd Kathisma for Monday Matins in the Second Mode)  
1. Make smooth, then,
our path for our good, Master,
through what lies before us, according
to the need of each: sail
with those sail, journey with those
who journey, heal the sick, for you
are the physician of our souls and
bodies. (Prayer at the Bowing of the Heads in Divine Liturgy)  
1. Let us now in harmony call Mary, the all-immaculate, blessed, the divine Ark who contained the Giver of the law, who takes away all our iniquities through his divine compassion, the unfathomable sea. (Ode 1 of the Canon for Thursday in the Third Mode)   
---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 47.  