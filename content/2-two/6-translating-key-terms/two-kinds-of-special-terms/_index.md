+++
title = "B. Kinds of Special Terms"
date =  2020-08-06T14:28:29-07:00
weight = 10
+++

There are several kinds of SPECIAL TERMS:  

> (1) Those words which communicate precise technical meaning to the person reading the liturgical text. These are especially common in rubrics (liturgical instructions) that tell people what actions to perform. Examples are: *bishop, priest, deacon, catechumen, sacrament.* These are also known as TECHNICAL TERMS.  

> (2) Those words that (with very few exceptions) have the same meaning in every context in which they occur. Examples are: Holy Spirit, baptism, apostle, martyr, evangelist. These are also known as KEY TERMS.

> (3) Words that have several different senses according to the context in which they occur. Examples are: believe, flesh, spirit, grace, law, save. Such words may need to be translated in different ways in different contexts, in order to give the correct sense in each context.  

When a SPECIAL TERM is a SPECIAL BIBLICAL TERM (that is, it is used in the Bible) then you should consult the Bible in your language to see how the term is translated. You should consider translating this SPECIAL BIBLICAL TERM in the same way that Bible translators did in your language. If there is more than one Bible translation in your language, then you should consult all the translations available to you. You should prefer the translation that is officially used by the Orthodox Church in your language, but in some cases you might decide that a different translation would be better suited to the liturgical text.

--- 

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 68  