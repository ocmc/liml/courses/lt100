+++
title = "Exercise 9"
date =  2021-09-23T13:25:43-07:00
weight = 10
+++

For each of the following words, give an example of a word which would be more general than that word (in English):  

1. shirt  
1. sparrow  
1. car  
1. gun  
1. to devour  
1. to slice 

**In some passages where there is an unknown idea, a more general word can be used without the meaning of the message being lost:**  

1. The One who adorns **the lilies of the field** commands us not to be concerned over our clothing.  
-- *Antiphon for Resurrectional Matins in the Plagal of the Fourth Mode* (See Matthew 6:28)  
--> The One who adorns **the flowers in the bush** commands us... 
"Flowers" is a more general word than "lilies." In this hymn, the exact kind of flower does not matter. However, it should probably be the same kind of flower used in the Scripture translation that it is referencing.  
1. Placing all their hope in heaven, the Saints treasured up for themselves an inviolable treasure; freely they received, freely they give remedies to the sick; in accordance with the Gospel **they possessed neither gold nor silver;** their kind deeds they shared out to humans and beasts alike; that being in all things obedient to Christ they might intercede with confidence on behalf of our souls.  
-- *Vespers Stichera for Saints Cosmas and Damian, November 1*  (See Acts 3:6)
--> ...in accordance with the Gospel they possessed **no money**... 

{{% notice note %}}
**REMEMBER:**  
Consider the whole hymn or prayer carefully, as well as any Scripture passages that it quotes or references. A word must always be translated according to its meaning and significance in the particular context in which it occurs.  
{{% /notice %}}


---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 45.  