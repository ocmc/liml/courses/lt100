+++
title = "Literary Form"
date =  2022-09-29T10:09:30-07:00
weight = 15
+++

Texts can be composed either in **prose** or in **verse.** Prose and verse are two **literary forms.**  

**Prose** is a form of language based on grammatical structure and the natural flow of speech. In prose, the **meaning** of the text is important, but the **sound** of the text is not very important.  Prose is usually written in sentences and paragraphs.

**Verse**, also called **poetry**, is a form of language based both on grammatical structure and on the **sound** of speech. In verse, the **meaning** of the text is important, and the **sound** of the text is also very important. The sounds of verse are typically organized into regular patterns. The patterned nature of verse allows a text to be set to music and sung or chanted. The language used in **songs** is usually composed in verse. Verse is usually written in lines and stanzas.

Here are two texts with similar topics. One is **prose** and one is **verse.**  

### Prose  

> Nighttime frustrates me because I read by the light of an unreliable and coverless kerosene lantern. Paraffin means money and there are days when the lamp has no oil. Most times I rely on the firelight of an unreliable duration. Daylight is always welcome. It allows the book of magic to tell me stories without interruptions except when I have to do this or that chore.  --Ngũgĩ wa Thiong'o, *Dreams in a Time of War* page 67[^1]  

[^1]: Ngũgĩ wa Thiong'o, *Dreams in a Time of War* (New York: Anchor Books, 2010), 67.  

### Verse  

> The left was fighting the right  
> And the right was fighting the light  
> And the light was blind in the night  
> And the night woke up in tears  
> When my fingers went to wipe them  
> They became a poem but to write them  
> They would have to speak tears in another language;  
> That would take years  
> -- K'Naan, "With God on Our Side"[^2]  

[^2]: K'Naan, "With God on Our Side," recorded 2012, track 1 on Disc 3 of *Chimes of Freedom: The Songs of Bob Dylan Honoring 50 Years of Amnesty International*, Fontana, compact disc.  

### Discuss  

Discuss the similarities and differences between these texts with someone else. Here are some questions to help you:  

1.  Which text is easier to understand?  
1.  Which text is easier to read aloud?  
1.  Which text is easier to sing?  
1.  Which text is easier to remember?  
1.  Which text creates stronger feelings or memories?  
1.  How would a translation of the verse text be different from a translation of the prose text?  

---  