+++
title = "Questions"
date =  2022-07-23T15:33:25-07:00
weight = 10
+++

Based on your reading, answer the following questions:  

1. Why is worship important?  
1. What are the two dimensions of Orthodox Christian worship?  
1. Name the two principal modes in which Orthodox Christian worship is expressed.  
1. Name two liturgical texts in the Bible.  
1. According to many scholars, is the oldest passage in the Bible a historical text, a liturgical text, or a geological text?  
1. Name two places in the Bible where a vision of heavenly worship is described. What do both of these visions have in common?  
1. When human beings worship God, who are they representing?  
1. How do we know that people should be able to understand the words they are using to worship God?  
1. Why is it impossible to *fully* understand the meaning of the worship of God?  
1. What role do liturgical translators have in Orthodox Christian worship?  

---  

