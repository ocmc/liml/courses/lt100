+++
title = "Questions"
date =  2022-09-29T11:51:17-07:00
weight = 5
+++


Discuss these questions with another student. Review Chapters 1 and 2 as needed.  

1. Who is involved in worship? 

1. Where is God when we worship? 

1. How do we respond to God in worship? 

1. What are the two modes of Orthodox Christian worship? 

1. What liturgical texts are in the Bible? 

1. Why is the oldest part of the Bible a liturgical text? 

1. How is heavenly worship described in the Bible? 

1. When human beings worship God, who are we representing? 

1. Does it matter whether or not someone can understand the words they are using when they worship? 

1. Is it possible to fully understand the words of worship? 

1. Why is liturgical translation important? 

1. What is the basic principle of translation? 

1. How are prose and verse different from each other? 

1. When you translate a text, what is your goal? What do you want the reader of your translation to understand? 

1. How do you define “translation”?   