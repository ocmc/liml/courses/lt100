+++
title = "Exercise 6"
date =  2021-09-09T15:15:28-07:00
weight = 45
+++

Take a song or poem in any language of your country and translate it into English. Try to translate the meaning accurately, even if the literary form in English is different. The Kiswahili poems given below are suitable for those who know Kiswahili.  

As in EXERCISE 1, underline every place in your translation where you have used an idiomatic expression or grammatical construction that is different from that of the Source Language. Discuss these with an advisor or colleague.

Underline every place in the original where there is a poetic or literary device that you have been unable to put into English. How does this change the effect or impact of the song or poem?

Could your translation be sung to the same melody as it can be sung in the Source Language? How might it be adjusted so that it can be sung?  

**Excerpt from KUKU NA MWEWE**  
***The Chicken and the Hawk*, by K W Wamitila, Muwa Publishers Limited, 2006.**  

Paukwa!  
Paukwa!  

Nyakati zilizopita, hizo zama za zamani,  
Njaa iliwakeketa, wanyama wote mwituni,  
Nyani, sungura na bata, *wakengia* taabani,  
Wakaulizana kwani, sasa tutafanya nini?  

Wakazipiga akili, wanyama wote porini,  
Kupima lile na hili, wafanye shauri gani?  
Njaa imewakabili, hawana mlo nyumbani,  
Wakaulizana kwani, sasa tutafanya nini?  

Sungura kajitokeza, kaingia ukumbini,  
Kasema, "inashangaza, sisi akina fulani,  
Njaa *inatuchakaza*, mwewe kiruka angani,"  
Wakaulizana, "kwani, sasa tutafanya nini?"  

"Mimi vile ninaona, mwewe akiwa angani,  
Kote anawezaona, kote kote aridhini,  
Chakula anakiona, akiwapo mawinguni,"  
Wakaulizana kwani, sasa tutafanya nini?  

Wakatoa na mawazo, wote pale ukumbini,  
"Ni nani siri anazo, za kutupaza angani?  
Na tuone siri hizo, tutie kitu tumboni,"  
Wakaulizana kwani, sasa tutafanya nini?

---

**Excerpts from UTENDI WA AYUBU**  
***Ballad of Job*, a classical Swahili ballad**

Mtume wetu Ayubu  
apeweo na Wahabu  
mali mangi ya ayabu  
&nbsp; &nbsp; &nbsp; yakaenea dunia  

Kwanda na tutaye mbuzi  
yali alifu mazizi  
alopowa na Azizi  
&nbsp; &nbsp; &nbsp; mangi mali yasosia  

Na watumwa wa kukifu  
kulla zizi maarufu  
wali watunga alifu  
&nbsp; &nbsp; &nbsp; waliokimtungia  

Na ng'ombe na ngamiaze  
na farasi na pundaze  
hesabuye niweleze  
&nbsp; &nbsp; &nbsp; ni kama nimezotaya  

Na anwai na mali  
kulla kitu mbalimbali  
kuzitaya ni muhali  
&nbsp; &nbsp; &nbsp; siwezi kuwatayia  

Na wanawe tuwaseme  
ni sabaa wanaume  
kana simba wenye tume  
&nbsp; &nbsp; &nbsp; wote marika mamoya  

Na watatu wanawake  
wenye nuru taambake  
jumla zijana zake  
&nbsp; &nbsp; &nbsp; ashara metimilia  

Umuwshiye Wadudi  
ibada akashtadi  
kwa mali na auladi  
&nbsp; &nbsp; &nbsp; yasimpe kushangaa  

Akendo mno ibada  
kwa Molawe Sarimada  
pakaitoka husuda  
&nbsp; &nbsp; &nbsp; ya kuya kuhusudia

...

Kisha cha tumwa Ayubu  
nimekoma kukutubu  
kwa auni ya Wahabu  
&nbsp; &nbsp; &nbsp; Rabbi menisahilia  

<details>I have finished writing  
of the prophet Job  
by the help of the Giver  
&nbsp; &nbsp; &nbsp; the Lord who has made it easy for me</details>  

Ameifanya sahali  
illahi Rabbi Jalali  
kiarabu kubadili  
&nbsp; &nbsp; &nbsp; lugha ya kiswahilia  

<details>He has made it simple  
Lord God of Majesty  
from Arabic to turn it  
&nbsp; &nbsp; &nbsp; to the Kiswahili language</details>  

Tumeyanoa zuoni  
na katika Quruani  
na nyinyi angaliani  
&nbsp; &nbsp; &nbsp; sikusaza neno moya  

<details>We found it within [book] bindings  
and in the Quran  
and you all observe  
&nbsp; &nbsp; &nbsp; I have not omitted one word</details>  

---

**Hiki Si Kile**  
by Ahmad Nassir bin Juma Bhalo

Natamba mtambi tambo &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; na watambi hatambile  
hajifungata masombo  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; kwa hayano na hayale  
nakuwaonya vigambo  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  ukumbi wa watu tele  
hiki na hiki si kile  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  hikino ni kiki hiki.  

Yaweni yasiyokuwa  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; yaweni yawe yawele  
huko ni kujiatuwa  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; pelelezato uole  
mambo ni majaaliwa  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; kiumbe sisahawile  
hiki na hiki si kile  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; hikino ni kiki hiki.  

Siwi bangu habananga  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; mwendo wangu ni u'ule  
bongo langu limejenga  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; akili kitwani tele  
sipigani na mjinga  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; nyuma akwitaye mbele  
hiki na hiki si kile  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  hikino ni kiki hiki.  

Dunia ichigaluka  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; penye mafupi na male  
wakumbwa hupapatika  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; leo hivi kesho vile  
ni wapi pa kuwaweka  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; dhiki zisiwadhikile  
hiki na hiki si kile  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; hikino ni kiki hiki.  

Tammati baiti tano  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; shairi nalikomele  
sipendi kusema nao  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ningaweza kwenda mbele  
mwisho wangu ni hapano  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ajuaye n'atambule  
hiki na hiki si kile  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; hikino ni kiki hiki.

{{% notice note %}}
**REMEMBER:**  
When checking a translation, ask:  
**Is it accurate?** Is the meaning as nearly as possible the same meaning that the original author intended?  
**Is it clear?** Will people understand the message?  
**Is it natural?** Is this the way that people speak?
{{% /notice %}}

---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 32.  