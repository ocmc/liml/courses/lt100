+++
title = "C. Part Three"
date =  2021-11-24T15:43:21-08:00
weight = 15
+++

## TRANSLATING UNKNOWN NAMES

Names that that are unfamiliar to the speakers of the Receptor Language may cause difficulties for several reasons:

1. The name may be hard to pronounce. It is usual that proper names (that is, names of people or places) are adapted to a form which is easy to pronounce in the Receptor Language. They are re-spelt according to the spelling system of the language.  
1. People will not know to what they refer. For example, a name might refer to a region or a town, or a river or a mountain. In some contexts it might even not be clear whether a place or a person is referred to.

Therefore, whenever necessary, the translator should provide any information that the hearer needs in order to understand the passage correctly. This is usually done by including a general word which indicates the kind of place referred to, as illustrated in the examples below.

The translator should be careful to check all unfamiliar place names on a map to make sure that he himself knows what is referred to.

1. As Elias of old dwelt on **Carmel**, so he too dwelt on Mount Athos... (Ode 8 of the Canon to St Athanasios of Athos, July 5)
--> As Elias of old dwelt on **Mount Carmel**, so he too dwelt on Mount Athos...
1. Christ has come, whom the Prophets declared would come from **Sion**, and call back the world. (Ode 3 of Matins Canon for Mid-Pentecost)
--> Christ has come, whom the Prophets declared would came from **Mount Zion**, and call back the world.
1. ...the Word, whom you saw after the resurrection at **Emmaus**, and ate with him with heart aflame along with Cleopas... (Vespers Aposticha for the Apostle Luke, October 18)
--> ...the Word, whom you saw after the resurrection at **Emmaus town**, and ate with him with heart aflame along with Cleopas...
1. When the Lord Jesus was born in **Bethlehem of Judea**... (Vespers Aposticha for Nativity, December 25)
--> When the Lord Jesus was born in **the town of Bethlehem in the region of Judea**...
1. **Isaias** dance: the Virgin has conceived and given birth to a Son, Emmanuel... (Crowning Hymn in the Sacrament of Holy Matrimony)
--> **Prophet Isaiah** dance: the Virgin has conceived and given birth to a Son, Emmanuel...
1. ...when you see the mountains tremble and **Jordan** being turned back... (Glory at the Matins Aposticha for the Forefeast of Theophany, January 2)
--> ...when you see the mountains tremble and **the River Jordan** being turned back...
1. You reached **Samaria**, and talking with a woman, sought water to drink, my all-powerful Saviour... (Exapostelarion for the Sunday of the Samaritan Woman)
--> You reached **the region of Samaria**, and talkiing with a woman, sought water to drink, my all-powerful Saviour...
1. ...they refuted the impiety of accursed **Antiochos**... (Glory at the Vespers Aposticha for the Maccabeean martyrs, August 1)
--> ...they refuted the impiety of accursed **King Antiochos**...

---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 50.