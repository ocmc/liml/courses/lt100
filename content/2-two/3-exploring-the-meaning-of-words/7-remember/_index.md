+++
title = "Remember"
date =  2022-01-19T16:27:58-08:00
weight = 35
+++ 

### REMEMBER:  

Check that words are used in the natural order.  

The natural order in the Receptor Language may be different from that of the Source Language.  

---  

### REMEMBER:  

When studying the meaning of a word, either in the Source Language or in the Receptor Language, you need to study:  

1. The different senses of a word; does the word have different meanings in different contexts?  
1. Words that have similar meanings; how do they differe from each other in meaning?  
1. The other words with which that word occurs; what are the natural collocations of that word?  

---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 66