+++
title = "Same Form, Different Meaning"
date =  2022-03-14T12:05:33-07:00
weight = 35
+++

Sometimes it may be that something exists in the Receptor Language culture, but its significance and meaning in that culture is very different from its significance and meaning in the biblical culture. For example, tax collectors may be known in the Receptor Language culture, but they are not regarded as traitors to their own people, as the Jewish tax collectors were, because they served the hated Roman conquerors. Leprosy (or a similar disease) may be known in the Receptor Language cultures, but lepers may not be social outcasts as they were in biblical times, needing not only healing but cleansing from ritual contamination. Customs like circumcision may exist, but their function in the Receptor Language culture is likely to be very different from their function in the biblical culture. The concept of a martyr might be familiar, but it might refer to somebody who fights in violent conflict against other human beings. So translators need to study not only surface similarities of ideas and customs, but also their deeper significance and meaning within the culture.

--- 

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 78