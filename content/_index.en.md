---
title: "LT-101 Introduction to Liturgical Translation"
---

# LT-101 Introduction to Liturgical Translation

_Every hymn fails_  
_that seeks to match_  
_the multitude of your many mercies._  
_For even if we offer you, O holy King,_  
_songs equal in number to the sand,_  
_we achieve nothing worthy_  
_of what you have given us, who cry to you:_  
  
_Alleluia!_   

-- Akathistos Hymn, Kontakion 10, trans. Ephrem Lash 


LT 101 **Introduction to Liturgical Translation** is designed for people who are translating, or preparing to translate, liturgical texts of the Eastern Orthodox Christian Church into a language that they speak fluently. It can be used as a textbook during courses or in less formal training situations. This training programme was created by mission specialists of the Translation Program of OCMC - [Orthodox Christian Mission Center](https://ocmc.org).



## What You Will Learn

By completing this training programme, you will:

* Gain an in-depth understanding of the principles of Eastern Orthodox Christian liturgical translation;

* Develop skills and learn methods for using these principles to achieve meaningful translations that can be used in Eastern Orthodox Christian worship, and that represent the original texts faithfully and effectively;

* Discover the beauty of your own language and develop a vision for empowering people to worship God "in spirit and truth" using the rich resources of that language; and

* Experience the benefits of cooperation and teamwork in the translation process, adapting the methods to fit the local situation.

## What You Must Know  

Before starting this course, you must:

* Be able to read, understand, write, and speak English at a secondary-school level.  

## What You Need  

In order to successfully complete this training programme, you will need:

* Access to a computer or mobile device with an internet connection; and  
* Time to study every day.

* You will need a reliable internet connection.
* You will need to dedicate some time every day towards studying the materials and completing the exercises.

## How to Take the Course  

* Starting at the beginning, study Modules I, II, III and IV and complete all exercises.
* After the first four Modules, you may study the rest of the course in any order.

