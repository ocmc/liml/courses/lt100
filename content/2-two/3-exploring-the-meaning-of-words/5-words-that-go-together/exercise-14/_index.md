+++
title = "Exercise 14"
date =  2021-12-23T13:55:17-08:00
weight = 10
+++

Translate the following phrases into your own language. Be careful to use natural collocations. Please give a word-for-word key in English.  

1. Abate the wild tempests of my soul, O God of pity.  
1. They stopped the mouths of wild beasts.  
1. I weep bitterly and I look downcast.  
1. A great sword was sharpened against you, O my Christ.  
1. ... [Jesus gives] life to those in Hades' deepest depths.  
1. raise our minds from the heavy sleep  
1. I am filled with amazement.  
1. Stretch out your mighty hand, that is filled with blessings.  
1. The precious blood of your Christ ... poured out for the life and salvation of the world.  
1. Prepare the way of the Lord.  
1. the blast of the trumpet  
1. Establish this house  

---  

#### REMEMBER:  

Check that each word is used in its natural collocation. Avoid carrying over collocations from the source text that are not natural in the Receptor Language.  

---

CITATIONS

1. Ode 6 of the Matins Canon for Thursday in the Fourth Mode.  
1. Vespers Stichera for the Sunday of All Saints  
1. Vespers Stichera for Wednesday in the Fifth Mode  
1. Encomia for the First Section of Holy Friday Lamentations  
1. Encomia for the Second Section of Holy Friday Lamentations  
1. Prayer of St Basil from the Midnight Office  
1. Vespers Stavrotheotokion for Tuesday in the Fourth Mode  
1. Prayer at the Bowing of the Heads in the Divine Liturgy of St James  
1. Epiclesis of the Divine Liturgy of St Basil  
1. Matins Kathisma for the Forefeast of Theophany, Jan 2  
1. First Ode of the Matins Canon for the Ascension  
1. First Oikos for Matins on the Forefeast of the Exaltation of the Cross, September 13


---

From *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, pages 64 - 65  