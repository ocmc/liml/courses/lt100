+++
title = "ii. Substitute Something Similar"
date =  2021-09-20T10:58:58-07:00
weight = 20
+++

Sometimes the exact thing referred to in the text may be unknown to the people of the Receptor Language area, but there may be something similar that is known that can be used instead.  

Again, it is essential to consider the context carefully and to make sure that the idea which is important in the context has been communicated.  

**(a)**  
> By your visitation, pure Maiden, destroy the insolence of the hostile **lion** who goes roaring each day to swallow down your inheritance, and overthrow his arrogance and pride.  
> -- *Canon for the Protecting Veil, Ode 5*  

In this hymn, the important idea is that the lion is a dangerous animal that prowls around looking for an animal to pounce on and eat. It is a reference to 1 Peter 5:8, "your adversary the devil walks about like a roaring **lion**, seeking whom he may devour." In an area where lions are unknown, but where leopards are found, the word "leopard" could be used instead of "lion." A leopard, like a lion, is a fierce beast which attacks and eats other animals. To substitute leopard for lion in the passage does not change the message. If the Bible in this language has translated 1 Peter 5:8 using the word for "leopard," then using the same word to translate this hymn also preserves the reference.  

**(b)**  
> Clothed in **snow white robes** the shining Angels appeared, symbolically making clear to the godly Disciples your second Coming, O Christ. With them, as we contemplate your godhead, we all magnify you.  
> -- *Monday Canon in the Second Mode, Ode 9*   

In one language in Africa, the phrase "white as snow" was translated in the Bible as "white as egrets' feathers." This translation communicates the main poin, that is, extreme whiteness.  

**(c)**  
> Offering your whole self to the Lord, you bared yourself to the hostile elements of **snow,** ice and heat.  
> -- *Canon for Symeon the Stylite (September 1), Ode 5*  

St Symeon the Stylite is a historical figure who historically endured the weather event of snow. From context, even if the word "snow" is unknown, it is clear that St Symeon endured extreme weather. However, other words for extreme weather that St Symeon would have encountered-- hail or windstorms, for example-- would not make the hymn less historically accurate.

**REMEMBER:**  

In deciding whether or not it is possible to substitute something from the Receptor Language culture, take the following points into account:  

1. Choose something that is as similar in form as possible to the original idea, provided that it also communicates the idea that is important for the context. For example, in the Canon for Symeon the Stylite, "hail" is a good substitute for "snow" because it is frozen water that falls from the sky and exposure to it is dangerous. It is historically likely that St Symeon encountered hail as well as snow. It would not be as good to substitute something like "lightning" because, although lightning is also something dangerous that comes from the sky, it is very different from snow in form.

1. The idea must be something that fits in with the time in which the text was written, as well as the time that it refers to, or which at least doesn't seem inappropriate in the context.  

1. This solution is good where a teaching illustration is being given, or where a figure of speech, such as a comparison, is being used. But it should not be used where an actual historical event is being referred to. For example, several hymns refer to Mark 11:13 where Jesus cursed a tree and that tree was a fig tree. To substitute any other kind of tree would be untrue to a historical fact.  

1. Notice that certain themes run right through the Bible and have symbolic meaning in the New Testament because of Old Testament references. Hymns often make use of both Old Testament and New Testament references. For example, references to the vine and the vineyard in some contexts symbolize Israel (Isaiah 5:7) and the Church (John 15:5). References to the fig tree, and to the shepherd and his sheep, also have strong Old Testament associations. Care should be taken to translate all such references in the same way, wherever possible, so that this connection of thought is not lost.

1. Orthodox Christian liturgical texts quote Scripture or refer to Scripture frequently. The connection between liturgical texts in the Receptor Language and Holy Scripture in the Receptor Language should be as clear as possible. When the Greek text quotes Greek Scripture, the Receptor Language translation should quote Scripture in the Receptor Language whenever possible. Sometimes this is difficult, because most translations of the Old Testament are based on the Hebrew Old Testament, but liturgical texts quote the Greek New Testament. However, quoting Scripture in the Receptor Language is always an option that you should consider seriously.  

---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, pages 38 - 39.