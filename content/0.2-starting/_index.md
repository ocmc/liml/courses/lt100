+++
title = "STARTING TO TRANSLATE"
date = 2020-08-05T10:49:33-07:00
weight = 15
chapter = false
pre = "<b>ii. </b>"
+++

A very important part of the training programme for a translator is translation practice. The best way to learn to translate is by translating.

For a first translation project, choose texts that are:
* Particularly relevant, interesting and helpful for your people;
* Not too long;
* Not too difficult.

Below are some suggestions for translation projects that would be a good starting point:

1. Fixed Hymns at the Divine Liturgy
    1. Only-Begotten Son
    1. Entrance Hymn (Eisodikon)
    1. The Thrice-Holy Hymn
    1. The Cherubic Hymn
    1. The Symbol of Faith
    1. Holy, Holy, Holy
    1. We Praise You, We Bless You
    1. Axion Estin
    1. The Lord's Prayer
    1. One Is Holy
    1. We Have Seen the True Light
    1. Let Our Mouths Be Filled
1. Troparia of the Resurrection
1. The Resurrection Apolytikia in the Eight Modes
1. The Dogmatic Theotokia in the Eight Modes
1. Gladsome Light
1. Kataksioson
1. The Song of Symeon

---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 7.