+++
title = "Exercise 15"
date =  2021-11-24T15:29:13-08:00
weight = 45
+++

Choose one of the hymns from Exercise 14, and translate it into your own language. Pay particular attention to the translation of any ideas which may be unknown to your people.

Make a back-translation to your translation, in English. Discuss with your advisor or a colleague the advantages and disadvantages of the ways in which you have translated these unknown ideas.

Next, choose a different hymn from Exercise 14 and do the same thing, paying particular attention to the translation of unknown ideas.

---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 49.  