+++
title = "What are Liturgical Texts?"
date =  2020-08-05T11:14:42-07:00
weight = 35
chapter = true
pre = "<b>9. </b>"
+++

### Chapter 9

# What are Liturgical Texts?

In Orthodox Christian worship, liturgical texts are the words used in worship. These include Scripture, prayers, litanies, hymns, and rubrics. They are traditionally found in *service books*. Different types of texts are spoken, intoned or chanted by clergy, by cantors or choirs, or by the congregation. *Rubrics* are not delivered aloud, but rather give instructions about liturgical actions.    