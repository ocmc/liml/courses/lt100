+++
title = "Technical Terms"
date =  2022-01-27T16:13:56-08:00
weight = 5
+++

*Technical Terms* are most frequently found in the **rubrics** (liturgical instructions) of liturgical books. Some of these words can have different meanings when they are used in hymns. For example, **mystery** is a translation of **το μυστήριον** *to mystirion* that means the same thing as **sacrament** when it is used in rubrics. But, in hymns, the word "mystery" can have other senses.  



##### ABLUTION  

##### εὐχή τῆς ἀπολύσεως *efhi tis apolyseos*  

At the end of the rite of Christmation, the priest washes Holy Chrism off of the person who has been christmated. This act of washing is called **ablution**, and the prayers said during the washing are called prayers of **ablution**. The word **ablution** means "washing for ceremonial reasons." The original Greek word, **ἀπολύω**, means 'take [something] away.'

---  

##### ACROSTIC  

##### ἡ ἀκροστιχίς *i akrostihis*  

A hymn in which the first letter of each line spells out a word, a phrase, or the alphabet.  

---  

##### AINOI  

*see* **PRAISES**  

---  

##### ALTAR TABLE  

*see* **HOLY TABLE**

---  

##### APOSTLE or APOSTOLOS  

##### ὁ ἀπόστολος *o apostolos*  

1. The liturgical book containing the lectionary readings for the Acts of the Apostles and the New Testament Epistles.  

2. A particular daily reading from this book.  

---

##### AUTOMELON (plural AUTOMELA)  

##### Αυτόμελον *aftomelon*  

A **STICHERON** with its own melody, serving as a model for other **STICHERA** (called **PROSOMIA**) composed for the same melody.

---

##### BAPTISM  

##### το βάπτισμα *to vaptisma*  

As a technical term, **baptism** or **Holy Baptism** is the sacramental rite of baptism.  

---

##### BEAUTIFUL GATE  

*see* **HOLY DOORS**

---

##### BETROTHAL  

##### ἐπί μνήστροις *epi mnistris* or ὁ ἀρραβών *o arravon*  

The first portion of the order of the service of Holy MATRIMONY, including the exchange of rings.  

---  

##### BIBLICAL ODE *or* BIBLICAL CANTICLE  

#####  

Nine hymns that are taken directly from Scripture. These hymns were originally sung at Matins, but have now been replaced with the **ODES** of the **CANON**.

---

##### CANON  

#####  ὁ κανών *o kanon*  

A structured set of hymns used especially at Matins, as well as other liturgical services, composed of up to nine hymns called **ODES**.  

---  

##### CANTICLE  

*see* **ODE**  

---

##### CATECHUMEN  

##### ὁ κατηχούμενος *o katihumenos*  

This is a person receiving instruction before baptism. The Greek word **ὁ κατηχούμενος** *o katihumenos* means 'one who is taught.' (See Galatians 6:6)  

---  

##### CENSE  

##### θυμιάω *thymiao*  

The act of offering incense. As a technical term, **cense** means the deacon or priest or bishop swinging the liturgical **censer** so that smoke travels from the censer towards the object to which incense is being offered.

---

##### CENSER or CENSOR  

##### το θυμιατήριον *to thymiatirion*  

A small dish used for burning incense, suspended on chains so that the deacon or priest or bishop can swing it.

---

##### CHRISM  

##### το χρῖσμα *to hrisma* or το μύρον *to myron*  

The consecrated oil used in the Sacrament of Holy Chrismation.

---  

##### COMMUNION  

##### ἡ κοινωνία *i kinonia*  

As a technical term, this is the act of eating and drinking the Body and Blood during the Divine Liturgy. The Greek word means 'fellowship' and can also be used as a non-technical term.  

---  

##### CONFIRMATION or CHRISMATION  

##### το χρῖσμα *to hrisma* or το μύρον *to myron*  

As a technical term, this is the sacramental rite of Holy Chrismation, immediately following the rite of Holy Baptism. In a service book, the office of Holy Baptism usually includes **confirmation** or **chrismation**. **Confirm** means 'to make [someone or something] official'. So, the word **confirmation** means 'making [someone] officially a Christian.' **Chrism** is the technical name for the consecrated oil used in the Sacrament of Holy Christmation. So, the word **chrismation** means 'putting chrism on [someone].'

--- 

##### CROWNING  

##### το στεφάνωμα *to stefanoma*  

The second portion of the order of the service of **MATRIMONY**, where crowns are placed on the heads of the bride and bridegroom.  

---

##### DEACON  

##### ὁ διάκονος *o diakonos*  

The title for the ordained Orthodox Deacon serving in the Divine Services. The Greek word **ὁ διάκονος** *o diakonos* means 'servant' or 'person who ministers.' 

---  

##### DISMISSAL  

##### ἡ ἀπόλυσις  

A final, formal blessing at the end of a liturgical service. The Greek word means 'letting [someone] go away.'  

---  

##### EUCHELAION  

*see* **HOLY OIL**  

---

##### EXAPOSTILARION or EXAPOSTEILARION or PHOTAGOGIKON  

##### Ἐξαποστειλάριον *eksapostilarion* or Φωταγωγικόν *fotagogikon*  

The hymn following the **CANON** at **MATINS**.  

---  

##### EVLOGITARIA or EVLOGETARIA  

##### Εὐλογητάρια *evlogitaria*  

A series of hymns with the refrain "Blessed are you, O Lord, teach me your statutes."

---

##### EXORCISM  

##### ὁ αφορκισμός *o aforkismos*  

Part of the Order before Holy Baptism. The priest prays three prayers over the catechumen, telling evil spirits to leave the catechumen and asking God to protect the catechumen against evil spirits. The Greek term for these prayers,**ὁ αφορκισμός** *o aforkismos* is NOT the same as the word that is translated as *exorcism* (or *exorcise*, or *exorcist*) in the Bible. The Bible uses the noun **ὁ ἐξορκιστης** *o eksorkistis* 'a person who expels an evil spirit from a demon-possessed person' (Acts 19:13) and the verb **ἐξορκίζω** *eksorkizo* 'make somebody swear an oath.' This technical term in the Order before Holy Baptism, and the Biblical word share the same root, **ὁρκίζω** *orkizo* 'to makes somebody sewear an oath.' They have different prefixes. The Biblical word has the prefix **ἐκ** *ek* or **ἔξ** *eks* meaning 'from out of.' The technical term in the Order before Holy Baptism has the prefix **ἄπο** *apo* or **ἄφ** *af* meaning 'away from.' So, **ὁ αφορκισμός** *o aforkismos* means 'ordering evil spirits to *go away from* someone,' but **ὁ ἐξορκισμός** *o eksorkismos* would mean 'ordering evil spirits to *come out of* someone.' English does not have separate words to distinguish between these slightly different meaings, but if your language does have separate words, then for this technical term you should use a word that means 'order something evil to go away.'

---  

##### FONT  

#####  ἡ κολυμβήθρα *i kolymvithra*  

The large container of water in which a person is baptised.  

---  

##### GODPARENT  

*see* **SPONSOR**  

---  

#####  GOSPEL  

##### το εὐαγγέλιον *to evangelion*  

As a technical term, this is the ornately bound liturgical book, containing the liturgical Gospel readings, which is kept on the **HOLY TABLE**.

---  

##### **THE GREAT CHURCH**  

##### ἡ Μεγάλη Ἐκκλησία *i megali eklisia*  

The liturgical tradition of the Ecumenical Patriarchate of Constantinople.  

---

##### GREAT LITANY  

*see* **LITANY OF PEACE**  

---

##### HOLY DOORS or ROYAL DOORS or BEAUTIFUL GATE

##### Ωραία Πύλη *Oraia Pyli*  

The doors in the centre of the iconostasis, in front of the Holy Table

---  
##### HOLY OIL or UNCTION  or EUCHELAION or EFCHELAION  

##### το ἅγιον ἔλαιον *to agion eleon* or το εὐχέλαιον *to efheleon*  

1. The liturgical service for consecrating oil used to anoint sick people.

2. Oil used to anoint sick people.  

---

##### HOLY TABLE or ALTAR TABLE

##### ἡ ἁγίᾳ τράπεζα *i agia trapeza*  

The table in the centre of the **ALTAR** where the Divine Liturgy is celebrated.  

---  

##### IDIOMELON (plural IDIOMELA)  

#####  ἰδιόμελον *idiomelon*  

A **STICHERON** with a unique melody that is not used for other **STICHERA**.  

---

##### KATHISMA (HYMN) or **SESSIONAL HYMN** or **SEDALEN** 

##### το κάθισμα  

A set of hymns chanted after each **PSALTER KATHISMA** at **MATINS**. 

---

##### KATHISMA (PSALTER)  

##### το κάθισμα  

A division of the **PSALTER** for liturgical use.

---  

##### KONTAKION (plural KONTAKIA)  

##### κοντάκιον *kontakion*  

1. A hymn sung after the Sixth **ODE** at **MATINS**, and repeated in the **DIVINE LITURGY**.  

2. A lengthy thematic hymn with many sections, no longer used liturgically with the exception of the **AKATHISTOS HYMN**.  

---

##### LAUDS or PRAISES or AINOI 

##### Οἱ Αἶνοι *i eni*  

Verses from Psalms 148 - 150, interspersed with **STICHERA** at **MATINS** after the **EXAPOSTILARIA** and before the **DOXOLOGY**.  

---

##### LITANY or EKTENIA  

##### ὁ ἐκτενής or ἡ συναπτή  

A series of petitions and responses in the liturgical services. The deacon or priest intones each petition, and the choir or congregation responds.  

---  

##### LITANY OF PEACE or GREAT LITANY  

##### τα ειρηνικά *ta irinika* or μεγάλη συναπτή *megali synapti*  

The **LITANY** at the beginning of the **DIVINE LITURGY**, which begins with the petition "In peace let us pray to the Lord." Most sacraments and liturgical services begin with the **LITANY OF PEACE**.

---

#####   MARRIAGE  

*see* **MATRIMONY**

---

##### MATRIMONY *or* MARRIAGE

##### ὁ γάμος *o gamos*  

The liturgical rite of a wedding.

---  

##### MEMORIAL  

##### το μνημόσυνον *to mnimosynon*  

*see* **TRISAGION**  

---

##### MODE or TONE  

##### ἦχος *ihos*  

A musical system used to chant liturgical hymns. There are eight **modes**: First, Second, Third, Fourth, First Plagal (Fifth), Second Plagal (Sixth), Grave or Varys (Seventh) and Fourth Plagal (Eighth).  The OCTOECHOS is divided into eight parts: one for each **mode**. You can transliterate **ἦχος** *ihos* or use a word in your language that means 'a particular way of doing something.'  

---  

##### MOTHER OF GOD or THEOTOKOS  

##### Θεοτόκος *theotokos*  

A common liturgical and theological title for the Virgin Mary.

---

##### NARTHEX  

##### ὁ νάρθηξ *o narthiks*  

The porch or entry-way to the Church building, west of the **NAVE**.

---  

##### ODE *or* CANTICLE  

##### ἡ ᾠδή *i odi*  

A hymn that is part of the **CANON**. Each **ODE** is a set of several verses, linked in theme to one of the nine **BIBLICAL ODES**.  

---

##### ORDER  

##### ἡ ἀκολουθία *i akoluthia*  

The **Order** of a Divine Service is the portion of that Divine Service which is fixed-- which is unchanging, no matter the date or season. In this sense, **Order** means "arrangement" or "protocol" or "procedure." It does not mean "command."  

---  

##### PHOTAGOGIKON  

*see* **EXAPOSTILARION**  

---  

##### PLAGAL  

##### πλάγιος *plagios*  

The last four of the Eight **MODES** of Byzantine music. 

---

##### PRAISES  

*see* **LAUDS**  

---

##### PRAYER  

##### ἡ εὐχή *i efhi*  

This is the technical term for the **prayers** said by the Priest in Divine Services. In English, a number of technical Greek terms for different types of prayers are all translated as "prayer." If your language has many words for different types of prayers, you can use one specific word for these prayers said by the Priest, called **ἡ εὐχή** *i efhi* in Greek.  

---

##### PRIEST  

##### ὁ ἱερεύς *o ierefs*  

This is the title for the ordained Orthodox Priest serving in the Divine Services. **ὁ ἱερεύς** *o ierefs* is a ritual term, as opposed to **ὁ πρεσβύτερος** *o presvyteros* 'presbyter' or 'elder' and is sometimes translated into English as *priest*. An ordained priest has the ritual function of **ὁ ἱερεύς** *o ierefs* in the Divine Services, and the pastoral function as **ὁ πρεσβύτερος** *o presvyteros* In liturgical rubrics, the English word **priest** always means **ὁ ἱερεύς** *o ierefs*, not **ὁ πρεσβύτερος** *o presvyteros*. Both Greek words can have different meanings when they are used in hymns.  

---  

##### PROKEIMENON  

##### ὁ προκείμενον *o prokimenon*  

A short hymn which is sung multiple times, usually just before a Scripture reading. The Greek word means 'coming before.'  

---  

##### PROSOMION (plural PROSOMIA)  

##### προσόμοιον *prosomion*  

A **STICHERON** composed to be sung to the same melody as a particular **AUTOMELON**. 

---  

##### ROYAL DOORS  

*see* **HOLY DOORS**

---

##### SACRAMENT or MYSTERY  

##### το μυστήριον *to mystirion*   

When it is used as a technical term in rubrics, "sacrament" is one of seven liturgical rites: **Baptism, Chrismation, Eucharist (Divine Liturgy), Confession, Holy Unction, Marriage,** or **Ordination.** When referring to one of these rites, the English word **sacrament** (or **mystery**) should always be translated the same way.  

---  

##### SANCTUARY or ALTAR

##### το βῆμα *to vima*

The part of the Church behind the iconostasis, where the Holy Table is located.

---  

##### SEDALEN  

*see* **KATHISMA (HYMN)**  

---  

##### SESSIONAL HYMN  

*see* **KATHISMA (HYMN)**  

---  

##### SMALL LITANY 

##### μικρᾷ συνάπτη *mikra synapti*  

A **LITANY** that is used frequently in many liturgical services, comprised of only three petitions, beginning "Again and again in peace let us pray to the Lord." There are two **SMALL LITANIES** in the Divine Liturgy, following thre **LITANY OF PEACE**.

---

##### SPONSOR  or GODPARENT

##### ὁ ἀνάδοχος *o anadohos*  

In the Sacrament of Holy Baptism, the **sponsor** is a baptized and christmated Orthodox Christian who stands with the person being baptized and guarantees that the person being baptized has a sincere desire to become a Christian. This technical term is used in the Sacrament of Holy Baptism. Sometimes a **sponsor** is called a "godmother" or "godfather." Use a word that means 'a person who agrees to be responsible for somebody or for making sure that something happens or is done.'  

---  

##### STICHERON (plural STICHERA)  

##### στιχηρόν *stihiron*  

A genre of hymn sung at **MATINS** and **VESPERS**, usually stung interstitially with **STICHOI**.  

---  

##### STICHOS (plural STICHOI)  

##### ὁ στίχος *o stihos*  

A line of Scripture (usually from a **PSALM**) sung interstitially with **STICHERARIA** at **MATINS** and **VESPERS**.  

---

##### THEOTOKION  

#####  Θεοτοκίον  

A hymn to the **MOTHER OF GOD**.

---  
##### THEOTOKOS  

*see* **MOTHER OF GOD**

---

##### TONSURE  

##### ἡ κουρά *i kura*  

The ritual cutting of hair in the Service of Baptism, making a reader, or becoming a monk.  

---  

##### TRISAGION  

##### Τρισάγιον

1. The name of the prayer (the "Trisagion Prayer") "Holy God, Holy Mighty, Holy Immortal, have mercy one us."  

2. A set of prayers (the "Trisagion Prayers") that form the standard opening to many liturgical services, including the Trisagion Prayer.  

3. The memorial service for the departed, which begins with the Trisagion Prayers. Also called the "Trisagion for the Departed" or "The Memorial Service."  

---  

##### TROPARION (plural TROPARIA)  

##### τό τροπάριον *to troparion*  

General term for any hymn sung in liturgical services.  

---  

##### UNCTION  

*see* **HOLY OIL**  