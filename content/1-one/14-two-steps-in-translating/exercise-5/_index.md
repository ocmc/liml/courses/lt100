+++
title = "Exercise 5"
date =  2021-09-13T12:06:54-07:00
weight = 40
+++

Translate the following story into your own language.

As in previous exercises, underline places in your translation where you have used expressions or grammar which are different from the Source Language. Discuss these with an adviser or colleague. Also, if possible, discuss your translation with other speakers of the language.  

> Once upon a time, a long, long time ago, Tortoise wanted to be the wisest animal in the world. So one day he collected up all his knowledge and put it in a calabash, thinking that he would tie the calabash to the top of a tall tree; then he would be able to keep all that knowledge for himself.  
>
> When he had put all his knowledge in the calabash, he tied it round his middle and started to climb the tree. He tried and he tried and he tried, but his legs were so short that he could not reach round the calabash and round the tree.  
>
> Along came his friend Rabbit. "What are you trying to do, Friend Tortoise?" he asked. Tortoise replied, "I am trying to climb to the top of the tree so that I can fasten all my knowledge there and keep it for myself. Then I will be the wisest animal in the world." "Why don't you tie it on your back? Then you will be able to climb the tree," suggested Rabbit.  
>
> So Tortoise tied the calabash on his back and started to climb the tree. But he had only climbed a little way when he stopped. "I thought I had all the knowledge in the world in my calabash," he said, "but I see that there is some which I left out. Let me go down and put it in the calabash. Maybe there is some more knowledge which I have left out."  
>
> And that is why nowadays Tortoise always carries the calabash on his back. He is looking for all the knowledge he has left out, so that one day he will be the wisest animal in the world.

{{% notice note %}}
**REMEMBER:**  
When checking a translation, ask:  
**Is it accurate?** Is the meaning as nearly as possible the same meaning that the original author intended?  
**Is it clear?** Will people understand the message?  
**Is it natural?** Is this the way that people speak?
{{% /notice %}}

---

From *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, pages 33 and 34.  