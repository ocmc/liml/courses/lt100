+++
title = "B. Clarity"
date =  2021-08-26T14:55:42-07:00
weight = 10
+++

When judging whether a translation is good or bad, there are three questions to ask:

1. Is the translation **ACCURATE?**  
## 2. Is the translation **CLEAR?**  

Does the translation **communicate** the meaning? Do people understand what the translation means? Is what they understand in fact the original meaning that the author intended?  

It should, however, be recognized that there are some hymns and prayers that are hard to understand because of the content of the message. Spiritual insight, given by the Holy Spirit, is needed. There are also some parts where further teaching and background knowledge is needed before the message can be fully understood.  

For the translator, the important thing is that there is nothing in the wording of the translation that makes the message difficult to understand. The kind of language used should be that which makes the message as clear as possible.  

### EXERCISE 3  

In any language, there may be more than one way of expressing the same idea. One way may be easier to understand than another way. It is your aim as a translator to express the meaning in the way that is clearest, so that it communicates the message effectively.  

Below are quoted some liturgical texts in different English translations. For each passage, compare the translations and put a mark beside the one that seems to you to be the most clear. For any versions which seem to you to be less clear, list any reasons why they are more difficult to understand.  

1.
1.
1.
1.
1.
1.
1.
1.
1.
1.

### EXERCISE 4  

Follow the same instructions as for EXERCISE 3 above.  

1.
1.
1.
1.
1.




---
Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, pages 24 - 26.