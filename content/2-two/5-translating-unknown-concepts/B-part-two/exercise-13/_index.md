+++
title = "Exercise 13"
date =  2021-11-18T11:02:21-08:00
weight = 35
+++

Each of the following passages includes one or more ideas that may be unknown in the Receptor Language area. There may also be some ideas that, though known, may not be easily expressed in the Receptor Language by a single word.

(1) Underline all the unknown ideas, or problem words.  
(2) Write down how you might re-express the meaning of the word or phrase you have underlined in a way that could be readily translated into your language. For each example, state which of the five possible methods for translating an unknown idea you have used. The five methods are:  

* Use a descriptive phrase  
* Substitute something similar  
* Use a foreign word from another language  
* Use a word that is more general in meaning  
* Use a word or phrase that is more specific in meaning  

1. The multitude of the strength of the Hagarenes has been removed and has now given, like an Ark from among foreigners to the new Israel, your imprint on a towel, O Christ, and your glory which it had also gained; for it is not right for Holy Things to be thrown aside. (Ode 6 for the Canon of the Icon Not Made with Hands, August 16)  
1. Fair swallow, precious nightingale, all-beauteous dove, desert-loving turtle-dove, Baptist of the Lord, offshoot of the desert, make my soul, that has become a desert through unfruitfulness, fertile and productive of good. (Ode 9 of the Canon for Tuesday in the First Mode)  
1. Like a fruitful olive, the Virgin blossomed with you, the fruit of life, to bear for the world as fruit your great and rich mercy. (Final Theotokion for Monday Matins in the Second Mode)  
1. I was entrusted with a sinless and living country, but, having sown the ground with sin, with a sickle I have reaped the ears of indifference and piled up the heaps of the sheaves of my actions, which I have not spread out on the threshing 
floor of repentance. But I beg you, our God, husbandman before the ages, with the wind 
of your loving compassion winnow away the chaff of my works, and provision my soul with 
forgiveness; shut me in your heavenly storehouse and save me. (Vespers Stichera for Sunday of the Prodigal Son)  
1. Wandering in deserts, clothed in camel’s hairs, you dwell in the former as in a royal palace, wearing the latter as a kingly robe you reigned over your passions. (Ode 6 of Canon for St John the Baptist, August 29)  
1. Increasing our talent by good works, let us offer them instead of gold and frankincense and myrrh as gifts to Christ who gave them, as he comes to be born of a Maiden, Child of God. (Diode of Compline, December 21)  
1. He tasted gall, healing the tasting of old; but now with honeycomb Christ gives the Forefather a share in illumination and his sweet participation. (Ode 4 of the Matins Canon for St Thomas Sunday)  
1.   At a divine command the chief Apostles hastened from the ends of the earth to bury you, and when they saw you being taken from the earth to heaven they cried out with joy in Gabriel’s words: Hail, chariot of the whole Godhead; hail, who alone by your childbirth have joined together things on earth with those on high. (Lauds at Matins for the Dormition of the Theotokos, August 15)  
1. Who will recount your chains city by city and your afflictions, glorious Apostle Paul? The toils, the pains, the watchings, the sufferings from hunger and thirst, from cold and nakedness, the basket, the beatings, the stonings, the journeying, the deep, the shipwrecks? You became a spectacle to Angels and to humans. You endured all things in Christ you gave you power, that you might gain the world for Christ Jesus, your Lord. And so we beseech you, as we faithfully celebrate your memory, intercede without ceasing that our souls may be saved. (Vespers Aposticha for Saints Peter and Paul, June 29)  
1. With what garlands of words may we paltry creatures garland Athanasios? The falcon who flies heavenwards, lifted soaring on godlike wings of contemplation; the impregnable tower of humility; the unshakeable wall of discernment; the honoured beauty of the fair adornment of virtues; the one who on behalf of his children implores Christ God the only compassionate. (Vespers Stichera for St Athanasius, July 5)  
1. Sprinkle me with the hyssop of repentance, purify me from the stain of the passions, that I may appear pure before you when you are about to judge the universe, O Jesus, with your just judgement. (Ode 5 for Matins Canon of Tuesday in the Third Mode)  
1. Offering your whole self to the Lord, you bared yourself to the hostile elements of snow, ice and heat. (Ode 5 of the Matins Canon for St Symeon the Stylite, September 1) 

---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, pages 47 - 48.  