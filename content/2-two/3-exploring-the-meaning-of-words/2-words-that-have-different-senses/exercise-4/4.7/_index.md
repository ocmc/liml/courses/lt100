+++
title = "4.7"
date =  2021-12-14T15:50:14-08:00
weight = 35
+++

#### 7. Third Antiphon of Sunday Matins in the Plagal of the First Mode

| Ἐπὶ | οἶκον | Δαυΐδ, | τὰ | φοβερὰ | τελεσιουργεῖται· | πῦρ | γὰρ | ἐκεῖ | φλέγον, | ἅπαντα | αἰσχρὸν | νοῦν. |  
| --- | ----- | ------ | -- | ------ | ---------------- | --- | --- | ---- | ------- | ------ | ------- | ----- |  
| epi | ikon | David | ta | fovera | telesiurgite | pyr | gar | eki | flegon | apanta | es-hron | nun |  
| on | house | David | the | causing fear | will be accomplished | fire | for | there | burning | every | causing shame | thought |  

**SOT:** On the house of David the [things] causing fear will be accomplished, for fire [will be] there, burning every shame-causing thought.  

**LASH:** Fearful things will be accomplished upon the house of David; for a fire will be there, burning every shameful thought.  

**DEDES:** On the house of David are the fearsome
things to be fully accomplished; for therein
will be fire, burning every shameful thought.  

**GEV:** Fearful things will be done on the house of David, for fire will burn every shame-causing thought away.  

**MOT:** God will accomplish things in David's descendants that cause great fear; there will be a fire that burns up every thought which causes shame.  

---

* **How is οἶκος ikos ‘house’ translated in these versions?**  

* **What is the real meaning of οἶκος ikos ‘house’ in this hymn?**  

* **What word(s) would you use in translating οἶκος ikos ‘house’ into your own language, for this hymn?**