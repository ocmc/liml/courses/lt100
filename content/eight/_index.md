+++
title = "PART EIGHT"
date = 2020-09-28T11:21:14-07:00
weight = 50
chapter = true
pre = "<b>VIII. </b>"
+++

### PART EIGHT

# BOOKS FOR TRANSLATORS

A. Reference Books for Translators   
B. Further Reading on Translation Principles