+++
title = "What Kinds of Words Need to Be Studied?"
date =  2022-03-03T15:03:34-08:00
weight = 40
+++

Words to do with the spiritual world are especially important. The beliefs that Receptor Language speakers have about the supernatural world will be quite different from those of the Source Language speakers.  

For example, the word **spirit**. The translator needs to make a study in the Receptor Langauge of: 
1. all the words used for spiritual beings in the language, and  
1. all the words used to refer to the non-physical part of man.  

Too often in the past, **evil spirit** has been translated by an expression that means something like "bad temper." This gives a wrong meaning, hiding the fact that in Orthodox Christian spiritual life, evil spirits are independent spiritual beings that can harm or even take possession of a person. Thus, the reader does not understand that Christ has power over such spirits. Could this be the reason why nominal Christians in many areas of the world still live in fear of fetish powers and carry charms for protections against these spirits?  

The word for **God** is another word that needs to be very carefully studied, especially in an area where Christianity has only recently been introduced.  

| BE CAREFUL TO CHOOSE THE RIGHT WORDS TO TRANSLATE TERMS THAT REFER TO SPIRITUAL BEINGS. THIS IS ESSENTIAL IF THE FULL TRUTH OF THE CHRISTIAN MESSAGE IS TO BE COMMUNICATED. |  
| ---------- |

Also keep alert to find ideas, customs, and terms that could serve as a bridge to help people understand theological truths. For example, there may be something in the culture that reflects truths about forgiveness, cancelling of sin, or redemption. Use what people already know and believe to help them understand the full truth of the Christian faith.  


--- 

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 77