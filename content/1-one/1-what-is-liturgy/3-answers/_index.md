+++
title = "Answers"
date =  2022-09-22T16:43:48-07:00
weight = 15
+++

1. **Why is worship important?**  *Many possible answers, including:*  Worship is how humans interact with God. Worship is how all of the Church-- living, departed, yet to be born-- join together.   
1. **What are the two dimensions of Orthodox Christian worship?**  (1) Worship is God's presence with his people. (2) Worship is the people's response to God.  
1. **Name the two principal modes in which Orthodox Christian worship is expressed.**  Action and Language  
1. **Name two liturgical texts in the Bible.**  *Possible answers include:* The Psalter, the Book of Odes, the Song of the Sea, any individual Ode or Psalm, etc  
1. **According to many scholars, is the oldest passage in the Bible a historical text, a liturgical text, or a geological text?**  A Liturgical text. (The Song of the Sea, Exodus 15)  
1. **Name two places in the Bible where a vision of heavenly worship is described. What do both of these visions have in common?**  Isaiah and the Book of Revelation. Both of these visions show angels at the throne of God singing "Holy, Holy, Holy."  
1. **When human beings worship God, who are they representing?**  When human beings worship God, they represent the angels (cherubim and seraphim) who sing "Holy, Holy, Holy."  
1. **How do we know that people should be able to understand the words they are using to worship God?**  The texts of worship were always composed in language that the people using them could understand.    
1. **Why is it impossible to *fully* understand the meaning of the worship of God?**  The worship of God cannot be *fully* understood because God is too great to describe in words ("ineffable"), too great to understand using words ("incomprehensible"), and too great to imagine using words ("inconceivable").  
1. **What role do liturgical translators have in Orthodox Christian worship?**  If human beings are going to worship God in language that we can understand, then somebody has to translated the worship of God into every human language. This is what *liturgical translators* do.  