+++
title = "4.2"
date =  2021-12-14T15:43:03-08:00
weight = 10
+++


#### 2. Ode 5 of the Matins Canon for December 23  

&nbsp;  

| Τοῦ | Ἐφραθᾶ | οἶκος | Βηθλεέμ, | Ἄρχων | ἐξελεύσεται | ἐκ | σοῦ, | ἐν | Ἰσραὴλ |  
| --- | ------ | ----- | -------- | ----- | ----------- | -- | ---- | -- | ------ |  
| Tu | Efratha | ikos | Vithleem, | Arhon | ekselefsete | ek | su | en | Israil |  
| Of | Efratha | house | Bethlehem | ruler | will come out | from | you, | in | Israel |  

&nbsp;

| προσκαλούμενος, | Ἔθνη | ἀπωσμένα, | ὡς | προηγόρευσεν, | ἐν | Πνεύματι | Μιχαίας, | καταυγαζόμενος. |  
| --------------- | ---- | --------- | -- | ------------- | -- | -------- | -------- | --------------- |  
| proskalumenos, | ethni | aposmena | os | proigorefsen, | en | Pnevmati | Mihaias, | katavgazomenos. |  
| will be summoned | the nations | rejected | as | foretold | in | Spirit | Micah, | illuminated. |  

**SOT:** Bethlehem, house of Ephratha * a ruler will come out from you * the rejected nations will be summoned in Israel * as Micah said before in the Spirit * illuminated.

**GEV:** A ruler will come from you, Bethlehem Ephrathah. The rejected nations will be recalled to Israel, as prophet Micah said, illumined by the Spirit.

**MOT:** A ruler will come from the town of Bethlehem, in the land of Ephratha. All the people in the world who have been rejected will be called back to Israel. The prophet Micah said this when the Holy Spirit revealed it to him.  

#### This ode refers to Micah 5:2 in Greek. Compare your translation of the hymn to the Biblical text in your language.  

**Micah 5:2** (LXX)

| Καὶ | σύ, | Βηθλέεμ | οἶκος | Ἐφράθα, | ὀλιγοστὸς | εἶ | τοῦ | εἶναι |  
| --- | --- | ------- | ----- | ------- | --------- | -- | --- | ----- |  
| ke | sy, | Vithleem | ikos | Efratha | oligostos | i | tu | ine |
| and | you | Bethlehem | house | Efratha | few in number | you are | from | to be |  

&nbsp;

| ἐν | χιλιάσιν | Ἰούδα· | ἐξ | οὗ | μοι | ἐξελεύσεται | τοῦ | εἶναι | εἰς |  
| -- | -------- | ------ | -- | -- | --- | ----------- | --- | ----- | --- |  
| in | hiliasin | Iuda | eks | u | mi | ekselefsete | tu | ine | is |  
| in | thousands | Judah. | from | which | for me | will come out | from | to be | one |  

&nbsp;

| ἄρχοντα | τοῦ | Ἰσραήλ, | καὶ | ἔξοδοι | αὐτοῦ | ἀπʼ | ἀρχῆς | ἐξ | ἡμερῶν | αἰῶνος. |  
| ------- | --- | ------- | --- | ------ | ----- | --- | ----- | -- | ------ | ------- |  
| arhonta | tu | Israil, | ke | eksodi | aftu | ap | arhis | eks | imeron | aionos. |  
| ruling | the | Israel, | and | goings out | his | from | the beginning | out of | days | of ages. |  

**SOT:** And you, Bethlehem of the house of Ephrathah, you are few in number to be among the thousands of Judah, from which for me will come out one to be ruling Israel, and his goings out are from the beginning, out of the days of ages.  

**LES:**      And you, O Bethlehem, house of Ephrathah, 
       you are very small to be in the thousands of Judah, 
     from which for me one will come out to be for a ruler of Israel, 
       and his goings out are from the beginning, from days of old.” ]

**NETS:** And you, O Bethleem, house of Ephratha, are very few in number to be among the thousands of Ioudas; one from you shall come forth for me to become a ruler in Israel, and his goings forth are from of old, from days of yore.  

**SAAS:**  And You, O Bethlehem, House of Ephrathah, though you are fewest in number among the thousands of Judah, yet out of you shall come forth to me the One to be ruler of Israel. His going forth were from the beginning, even from everlasting.  

**LBS:** And thou, Bethleem, house of Ephratha, art few in number to be reckoned among the thousands of Juda; yet out of thee shall one come forth to me, to be a ruler of Israel; and his goings forth were from the beginning, even from eternity.  

1. In Hebrew, the same passage from the previous exercise does not include the word "house." Check Micah 5:1 in a Bible in your language. Does this change the way you understand the meaning of "house" in the hymn?  

**Micah 5:1** (MT) 

| וְאַתָּ֞ה | בֵּֽית־לֶ֣חֶם | אֶפְרָ֗תָה | צָעִיר֙ | לִֽהְיוֹת֙ | בְּאַלְפֵ֣י | יְהוּדָ֔ה | מִמְּךָ֙ | לִ֣י | יֵצֵ֔א | לִֽהְי֥וֹת | מוֹשֵׁ֖ל | בְּיִשְׂרָאֵ֑ל|   
| ------ | ---- | ----- | --- | -- | --- | ----- | ----- | ----- | ---- | ----- | ------- | --- |  
|    wʾattā    |    BYT LḤM   |   ʾPRTH    |  ṣaʿīr   |  l˙hyōt  |  b˙ʾalȧpē   |    YHWDH   | mim-mka   | lī | yiṣē  |   l˙hyōt    |  mōšil |  b˙YŚRʾL |  
| and you | Bethlehem | Ephrathah | small | to be | among the clans of | Judah | from you | for me | he shall go out | to be | a ruler | in Israel |  

&nbsp;  

| וּמוֹצָאֹתָ֥יו | מִקֶּ֖דֶם | מִימֵ֥י  | עוֹלָֽם׃|  
| ----- | ----- | ---- | -------- |  
| w˙mōṣaʾōt-aw | miqqadm | miyȧmē | ʿōlam |  
| and his origins | from primeval times | from day | eternity |  

**LEB:**  But you, O Bethlehem Ephrathah, too small to be among the clans of Judah, from you one will go out for me, to be ruler in Israel; and his origins are from of old, from ancient days.  

**The Bible in your language:**  

1. The Gospel of Matthew quotes the passage in Micah that the above exercises have discussed. However, the quotation in Matthew does not exactly match either the LXX or the MT. Does this change the way you understand the meaning of "house" in the hymn?  

**Matthew 2:6** (BYZ)

| Καὶ | σὺ | Βηθλεέμ, | γῆ | Ἰούδα, | οὐδαμῶς | ἐλαχίστη |  
| --- | -- | ------- | -- | ------- | ------- | -------- |  
| ke | sy | Vithleem, | gi | Iuda, | udamos | elahisti |  
| and | you(sg) | Bethlehem, | land | Judah | in no wise | smallest |  

&nbsp; 

| εἶ | ἐν | τοῖς |  ἡγεμόσιν | Ἰούδα· | ἐκ | σοῦ | γὰρ | ἐξελεύσεται | ἡγούμενος, |  
| -- | -- | ---- | --------- | ------ | -- | --- | --- | ----------- | ---------- |  
| i | en | tis | igemosin | Iuda. | ek | su | gar | ekselefsete | igumenos, |  
| you(sg) are | in | the | leaders | of Judah. | from | you(sg) | for | will come out | leader |  

&nbsp; 

| ὅστις | ποιμανεῖ | τὸν | λαόν | μου | τὸν | Ἰσραήλ. |  
| ----- | -------- | --- | ---- | --- | --- | ------- |  
| ostis | pimani | ton | laon | mu | ton | Israil. |  
| that | will shepherd | the | people | my | the | Israel. |  

**SOT:** and you, Bethlehem, in the land of Judah, are not at all the smallest among the leaders of Judah. For from you will come out a leader who will shepherd my people, Israel.  

**LEB:**  And you, Bethlehem, land of Judah, 
are by no means least among the rulers of Judah, 
for from you will go out a ruler 
who will shepherd my people Israel.’  

**NKJV:** But you, Bethlehem, in the land of Judah, Are not least among the rulers of Judah; For out of you shall come a Ruler Who will shepherd my people Israel.  

**GNT:** Bethlehem in the land of Judah,
you are by no means the least of the leading cities of Judah;
for from you will come a leader
who will guide my people Israel.  

**THE BIBLE IN YOUR LANGUAGE:**  

---

* **How is οἶκος ikos ‘house’ translated in these versions?**  

* **What is the real meaning of οἶκος ikos ‘house’ in these texts?**  

* **What word(s) would you use in translating οἶκος ikos ‘house’ into your own language, for this hymn?**