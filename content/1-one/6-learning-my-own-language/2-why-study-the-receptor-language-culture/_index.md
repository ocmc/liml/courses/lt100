+++
title = "Why Study the Receptor Language Culture?"
date =  2022-03-03T14:11:18-08:00
weight = 10
+++

1. Translators are well-educated people. You have learnt other languages, and been exposed to other cultures besides your own. Because of this education, you have become cut off in some ways from your own culture. You may have spent a number of years outside your own language area. For this reason, it is necessary to go back and sit with people in your home area in order to make a study of what they believe and do. You may need to rediscover the way in which your own people think and the way in which they use words. You may need to rediscover the shades of meaning and implications that may be carried by particular words. You may need to rediscover too the assumptions and presuppositions that people have, that affect the way that they interpret and understand any message.  

1. Sometimes a translator may think that he had found a good way to translate a key term. But when he tests that proposed translation, he discovers that people understand something quite different from the meaning that he wanted to communicate. For example: when preparing to translate the term "Christ" into one of the languages of Nigeria, the translator studied carefully the meaning of the original word. He learnt that it means literally "an anointed person." In this language, there is an expression for "someone who has been anointed." So the translator decided to use that term for "Christ." But when he studied the term further, he discovered that, in that culture, the "anointed one" referred to a new bride on whom oil had been poured before marriage. So he realised that the term was quite unsuitable to translate "Christ." If he had not studied his own culture, he might have made a serious mistake.  

| **REMEMBER:** |  
| ------------- |
| Even when you are a mother-tongue speaker of the language you are translating into, it is still very necessary for you to study your own culture. |  

--- 

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 76  