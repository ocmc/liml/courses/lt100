+++
title = "Exercise 8"
date =  2021-09-22T14:49:22-07:00
weight = 65
+++

Each of the following passages includes one or more ideas that may be unknown in the Receptor Language area (or that are not readily expressed by a single word in that language).  

(1) Underline all the unknown ideas.  
(2) Write down how you might re-express the meaning of the word or phrase you have underlined in a way that could be readily translated into your language. For each example, state which of the five solutions you have used.  

1. If the Lord had not been among us, none of us could have withstood in the contest with the foe; for those who are victorious are exalted from here.
**Let not my soul be taken like a sparrow by their teeth**, O Word; alas! How shall I, lover of sin, be delivered from the foe?  
-- *First Antiphon of Resurrection Matins in the Plagal of the Second Mode*  
1. Mystically you found beforehand for the world **the pearl of great price, hidden in the field of your heart**, Apostle, which once the nations found it they treasured it by the faith.  
-- *Matins Canon for the Apostle Andrew, Ode 3*  (See Matthew 13:44 - 45)  
1. ... it is easier for a camel to go through the eye of a needle... 
-- *Matthew 19:24*  
1. All-holy, ever-Virgin, Mother of God, **my firm shelter and guard, harbour and wall, ladder and tower,** have mercy, have pity; for to you alone have I fled for refuge.  
-- *Canon for Holy Unction, Ode 4*  
1. You flowered as a sweet-scented lily in the wilderness, honoured Saint; and so I cry out to you: Forerunner, chase from my soul the foul stench of evil.  
-- *Canon for Tuesday Matins in the Plagal of the First Mode, Ode 5*  
1. See, an acceptable time and a day of mercy; be willing to turn back then, O soul, producing the fruit of repentance, lest the fearful axe of death find you fruitless, and like the fig tree of old cut you down and cast you into fire in the other world.  
-- *Canon for Tuesday Matins in the Third Mode, Ode 9* (See Luke 13:6-9_)  
1. To-day is the spring of souls, for Christ, shining from the tomb like the sun, has dispelled the foggy winter of our sin. Let us sing to him, for he has been glorified.  
-- *Matins Canon for Thomas Sunday, Ode 1*  
1. You have appointed repentance, O Saviour, for those who turn back; grant me it, Good One Master, before the end of my life, giving me compunction and sighs, like the Harlot who once kissed your feet.  
-- *Canon for Monday Matins in the First Mode, Ode 4*  (See Luke 7:38)  
1. We honour your precious Cross, O Christ, the nails and the holy lance, with the reed and the crown of thorns. Through them we have been delivered from the corruption of Hades.  
-- *Canon for Resurrection Matins in the Plagal of the Second Mode, Ode 4* (See Mark 15:17)  


---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 44.  