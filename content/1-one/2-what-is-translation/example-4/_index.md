+++
title = "Example 4"
date =  2021-06-23T14:30:10-07:00
weight = 25
+++

### Genesis 49:10

*Hebrew text with word-for-word key in English:*


| לֹֽא־	יָס֥וּר | שֵׁ֙בֶט֙ | מִֽיהוּדָ֔ה | וּמְחֹקֵ֖ק | מִבֵּ֣ין | רַגְלָ֑יו |
| ------- | --- | ------ | ----- | ---- | ----- |
| lō'-yā·sūr | šê·ḇṭ | mî·hū·ḏā | ū·mə·ḥō·qêq | mib·bên | raḡ·lā |
| not-it-shall-depart | sceptre | from-Judah | and-commander's-staff | from-between | feet-his |

*English version A (The word-for-word key repeated):*  
Not-it-shall-depart sceptre from-Judah,  
And-commander's-staff from-between feet-his.

*English version B:*  
The sceptre shall not depart from Judah,  
nor the ruler's staff from between his feet, ...

*English version C:*  
Judah will hold the royal sceptre,  
And his descendants will always rule.  

{{% notice note %}}
A "sceptre" is a staff, a kind of stick, carried by a king as a symbol of his authority.  
"From between his feet" is a Hebrew idiom referring to a person's descendants.  
In this context, "Judah" refers to the tribe of Judah, not just to their ancestor Judah himself.
{{% /notice %}} 


*An alternative translation might be:*  
The kings of the people will come from the tribe of Judah forever.  
His descendants will always rule.

*Version D (An ancient Greek translation [LXX[^1]], with word-for-word key in English:)*

| οὐκ | ἐκλείψει | ἄρχων | ἐξ | ᾿Ιούδα | καὶ | ἡγούμενος | ἐκ | τῶν | μηρῶν | αὐτοῦ, |
| --- | -------- | ----- | -- | ------ | --- | --------- | -- | --- | ----- | ------ |
| uk | eklipsi | arhon | eks | Iuda | ke | igumenos | ek | ton | miron | aftu, |
| not | will-forsake | ruler | from | Judah | and | leader | from | the | thighs | his, |

*English version E (The word-for-word key of the Greek translation repeated):*  
Not will-forsake ruler from Judah  
and leader from the thighs his,  

*English version F (A translation from Greek into traditional English [LBS]):*  
 A ruler shall not fail from Juda,  
 nor a prince from his loins,  

 *English version G (A very literal translation from Greek into English [NETS]):*    
 A ruler shall not be wanting from Ioudas  
 and a leader from his thighs  

 *English version H (A translation from Greek into contemporary English [LES]):*  
          A ruler will not cease from Judah,  
         and one who leads from his thighs,  


{{% notice note %}}
"From his thighs" (or "loins") is a Greek idiom referring to a person's descendants.
{{% /notice %}}


### EXERCISE 4
1. Write down some of the ways in which the translator of version C re-expressed the message in English in order to communicate the meaning.
1. Write down some of the ways in which the ancient Greek translator re-expressed the message in order to communicate the meaning to a Greek-speaking reader.
2. Make a translation of this verse into your own language.

---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, pages 11-12.  

[^1]: Abbreviations for different Bible translations will be explained in **Chapter 4.A: Some English Translations of the Bible**.  