+++
title = "A. What Is an Idiom?"
date =  2021-04-07T14:18:06-07:00
weight = 10
+++


An **IDIOM** is an expression where the words taken together mean something different from the individual meaning of the words. Idioms are often special to a particular language.

Thus an "idiomatic expression" is a phrase that is natural and meaningful in a particular language, although not necessarily in other languages.

Examples of some idioms in English are:

* to break the silence
* to be caught red-handed
* an outstanding success
* to be scatter-brained
* How do you do?

In Psalm 89:17, "prosper for us the works of our hands" (SAAS) is an example of an **idiom** from Greek and Hebrew. It is not a natural idiom in English. Speakers of English would not use it in everyday conversation.

## EXERCISE 1

Underline all examples of English idioms in the following passage:

>'There is no point in getting all hot and bothered about it,' said his brother-in-law. 'Just be patient and bide your time a little. The whole thing will blow over before you can say "Jack Robinson."' 'It's all very well for you to talk,' grumbled the other.

---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 20.  