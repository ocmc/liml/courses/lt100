+++
title = "iv. Summary"
date =  2021-09-22T14:41:28-07:00
weight = 60
+++

Three possible solutions for translating an unknown idea are:  

1. Use a descriptive phrase.  
1. Substitute something similar that is known to the Receptor Language speakers.  
1. Use a foreign word from another language, together with either a descriptive phrase or a general word.  

### SOME GUIDELINES FOR CHOOSING THE BEST SOLUTION  

In deciding which solution is best in any particular example, **always consider the meaning of the word in its context.**  

**REMEMBER:**  

1. Avoid foreign words if possible.  
1. Where it is necessary to use a foreign word, introduce it with a general word or descriptive phrase to give some idea of the meaning.  
1. Think what is important about the meaning in the context. Is the main point clear?  
1. Avoid long descriptions which distract the reader from the main point of the text.  
1. Historical facts must not be changed.  
1. Ideas that would be out of place in the culture that the text is from, or that the text is about, must not be introduced.  
1. Other texts where the same idea is mentioned should be taken into account, especially if there is a liturgical or Scriptural "theme" involved.  
1. Use supplementary aids, such as pictures, to provide additional background information.  

---

From *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 43.