+++
title = "C. Naturalness"
date =  2021-09-01T15:46:05-07:00
weight = 15
+++

When judging whether a translation is good or bad, there are three questions to ask:

1. Is the translation **ACCURATE?**  
2. Is the translation **CLEAR?**  
## 3. Is the translation **NATURAL?**  

Is this the kind of language our people use? Is it "sweet"? Is it lively and interesting?  

If you are not very careful, you will often carry over expressions from the language you are translating from. Careful checking and testing is needed to find the natural, idiomatic expression in the language.  

### EXERCISE 5  

Below is an English translation of a liturgical text, the NEW version. Two English translations are given for comparison: a structure-oriented translations (GE-SOT) and a meaning-oriented translation (GE-MOT). From the NEW version give:  

(1) Three examples of points which are **not accurate** in the translation.  
(2) Three examples of points which are **not clear** in the translation.  
(3) Three examples of places where the language is **not natural**.  

1. GE-SOT:  
[He] uncovered [the] bottom of [the] deep, and brings [his] own through dry land, [he who] covered in it [those] opposing, the Lord, powerful in wars: for he has gained honour for himself.[^1]  

1. GE-MOT:  
The Lord, who is like a great soldier in a battle, exposed the bottom of the deep sea, and he brought his own people across the dry ground at the bottom of the sea; but there He covered their enemies with its water! Sing to the Lord, because he has gained honour for Himself![^2]  

1. NEW:  
God, who is powerful in wars, evaporated the bathic substructure, and brought His people across its dry land, He covered antagonistic there with its waters. Whereas He has gains reverential of Him![^3]  

---
Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 25.

[^1]: From *He Uncovered the Bottom of the Deep: A Liturgical Translator's Manual* by Dr. Michael Colburn, 2018.  
[^2]: From *He Uncovered the Bottom of the Deep: A Liturgical Translator's Manual* by Dr. Michael Colburn, 2018.  
[^3]: Adapted from *He Uncovered the Bottom of the Deep: A Liturgical Translator's Manual* by Dr. Michael Colburn, 2018.