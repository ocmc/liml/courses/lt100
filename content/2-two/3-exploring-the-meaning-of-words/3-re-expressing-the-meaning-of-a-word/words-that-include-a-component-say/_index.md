+++
title = "Words That Include a Component 'Say' "
date =  2021-12-15T13:36:49-08:00
weight = 5
+++

There are a number of words that include the meaning of something being said or thought. In some languages such words have to be re-expressed as a speech quotation.  

### EXAMPLES  

1. I wondered whether this was true.  
&#8594; <!-- right arrow --> I said to myself, "Is this true?"  
1. He asked whether the man had come.  
&#8594; <!-- right arrow --> He said, "Has the man come?"  
1. She exclaimed in surprise.  
&#8594; <!-- right arrow --> She said, "Ashe!" ("*Ashe* is an exclamation of surprise in the Hausa language.)  
1. Hebrews 11:21 Jacob blessed each of the sons of Joseph.  
&#8594; <!-- right arrow --> Jacob said to each of the sons of Joseph, "May God cause you to prosper."  

This way of expression is not, of course, used in all langauges. But there are quite a number of languages in which it is sometimes more natural and idiomatic to use a speech quotation to express such ideas, at least in certain contexts. Keep alert to find out whether there are examples of this in your own langauge.  

### EXERCISE 6  

Which of the following verbs in English include the component "say"?  
1. to walk  
1. to praise something  
1. to wonder whether something is true  
1. to follow  
1. to complain  
1. to fall  
1. to advise someone to do something.  

---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, pages 58 - 59  