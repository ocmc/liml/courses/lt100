+++
title = "Exercise 4"
date =  2021-09-13T11:59:49-07:00
weight = 35
+++

Translate the following passages into your own language.  

As in previous exercises, underline places in your translation where you have used expressions or grammar which are different from the Source Language. Discuss these with an adviser or colleague. Also, if possible, discuss your translation with other speakers of the language.  

(a)  
>Towards nightfall when Fatimeh came home she often took a pot and went down to the stream where she bathed and drew water. Sometimes Leibe went with her, sometimes Shaitu, but she was never alone. One evening... Hodio stalked hew down to the river and told her to make ready to run away with him because he loved her and wanted her to be his wife, but Fatimeh's refusal was quite definite. She must have known that as a slave girl she could never hope to marry a free-blooded and proud Fulani like Hodio Sunsaye. Hodio knew that too, but he waved aside her objections.[^1]   

(b)  
> Mai Sunsaye as outside the hut, reading under the durowa tree. He was much versed in the Koran, and he read and wrote Arabic with a fluency not unusual among high priests of the wandering Fulani. He made charms and amulets, he doctored the sick, he was a sage hightly respected in the village of Dokan Toro. From far and near his clients brought him their wounds of body and soul.[^1]  

(c)
> Once, soon after transferring to Manguo, I jumped over a low barbed wire fence around the school. One of the barbs caught the top of my left foot and tore deep into the flesh. Later it swelled and hurt so much that I could not walk. There were no medical clinics around and no doctor we could pay. My mother simply kept on washing the wound with salt water. My brother would literally cart me from place to place on the wheelbarrow. Somehow after weeks of my mother nursing my foot, I managed to begin walking again. An inch-long scar remains to this day. And a well of gratitude, for years later I learned of a child who had died of a similar wound, through tetanus poisoning.[^2]

(d)  
> Manguo, founded in 1928 on land given by the Kĩeya family, with Morris Kĩhang'ũ as the first head but later replaced by Fred Mbũgua and then Stephen Thiro, did not have a formal church building. On Sunday, the school hall became hallowed ground, the regular tables turned into a colorfully decorated altar and the regular benches into pews. The preacher on the first day that I attended was Morris Kĩhang'ũ, an ordinary teacher on weekdays, in the same school, not the most popular, prone as he was to using the stick to impose discipline and attentiveness in the class.[^2]

(e)
>The lines and the images in the various lyrics were familiar: I had read them in my copy of selections from the Old Testament, but from the lips of this mass of worshippers they carried a suggestion of sublime power. The soloists changed; any member of the congregation could join in, sometimes two taking over the next verse or repeating an earlier one. Some of the call-and-response was triadic: voices in unison, splitting into antiphony before coming together once more in triumphant reconciliation.[^2]  

[^1]: From *Burning Grass* by Cyprian Ekwensi, 1962.
[^2]: From *Dreams in a Time of War* by Ngũgĩ wa Thiong'o, 2010.

{{% notice note %}}
**REMEMBER:**  
When checking a translation, ask:  
**Is it accurate?** Is the meaning as nearly as possible the same meaning that the original author intended?  
**Is it clear?** Will people understand the message?  
**Is it natural?** Is this the way that people speak?
{{% /notice %}}

---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 33.  

