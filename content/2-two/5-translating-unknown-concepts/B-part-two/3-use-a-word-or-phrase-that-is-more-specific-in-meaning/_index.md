+++
title = "3. Use a Word or Phrase That Is More Specific in Meaning"
date =  2021-09-29T13:12:10-07:00
weight = 20
+++

Sometimes where there is no word in the Receptor Language to express a particular idea, the idea can be translated by giving **specific examples** of the thing referred to.

1. As you watched Christ nailed to the Cross, Magdalen, you wept and cried, ‘What is this sight? How is life dying? How is creation shaken, as it sees, and the great **lights** darkened?’ Therefore intercede that there may be given to our souls his peace and great mercy.  
-- Vespers Stichera for Mary Magdalen (22 July)  

If there is no word for "lights" in this sense, it may be possible simply to say "the sun and the moon," using the specific words.  

1. Sown with tears today,
the life-breathing **grain** of two-fold nature,
here within earth’s furrows the **grain** is sown,
but tomorrow it will burst once more to life.  
-- Lamentations of Holy Saturday Matins, Second Section  

It is not stated what kind of grain is being referenced. However, from background knowledge it is clear that it is almost certainly wheat. To use a general word for grain or seed may be possible in some languages, but in other languages it would be confusing. In that case, the translator might translate by using a more specific word for whatever cereal crop is best known in the Receptor Language area. In West Africa, this might be "guinea corn seed" (the nearest equivalent to wheat).  

1. unite him/ her to the **flock** of your inheritance  
-- Third Exorcism Prayer of Holy Baptism  

The English word "flock" could mean a group of sheep, a group of goats, or a group of birds. In many languages, there is no word with all three of these meanings. The Greek word that "flock" is translated from, ποίμνη *pimni*, specifically means a group of sheep. If the Receptor Language has a specific word for a group of sheep, this is the word that should be used.


---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 46.