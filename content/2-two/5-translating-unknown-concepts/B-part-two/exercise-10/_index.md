+++
title = "Exercise 10"
date =  2021-09-29T12:20:02-07:00
weight = 15
+++

For each of the following examples, re-express the meaning of each passage, in English, using a more general word instead of each word which is underlined. Consider the meaning in the context carefully.  

1. Do not reject me, held fast by sloth, my Saviour. Rouse my thought to repentance and show me to be a tried worker of your **vineyard**, granting the payment of the eleventh hour and your great mercy.  
-- *Matins Aposticha for Lauds in the First Mode* (See Matthew 20:1-16)  
1. Give us this day our daily **bread**;  
-- *The Lord's Prayer* (See Luke 11:3)  
1. Acquainted with tortures, close neighbour to fire, steadfast under the rending of the flesh, nobly enduring the boiling of **cauldrons**, you were not worsted by thoughts, you did not sacrifice to wooden idols; but bowing your neck to God, by the punishment of the **sword**, bearing the **wreath* of victory you mounted to the heights.  
-- *Vespers Stichera for a Woman Martyr* 
1. You appeared as wise in the midst of fire, Foot Soldiers of Christ, roasted like **lambs** and served on the table of God the universal sovereign, and inheriting ineffable gladness.  
-- *Canon for Wednesday Matins in the Third Mode, Ode 6*  
1. You carried the **ear of corn** that had not been husbanded, O Immaculate, which nourishes the hearts of all of us who faithfully sing your praise.  
-- *Canon for a Martyr, Ode 3*  


---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 46. 