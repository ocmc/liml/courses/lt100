+++
title = "Exercise 2"
date =  2021-09-13T12:54:24-07:00
weight = 25
+++

Take a short story in any language of your country (for example, Amharic, Hausa, Arabic, Fulani, Kiswahili) and translate it into English. The Kiswahili fable given below is suitable for those who know Kiswahili.  

As in EXERCISE 1 above, underline every place in your translation where you have used an idiomatic expression or grammatical construction that is different from that of the Source Language. Discuss these with your instructor or with a colleague.  

> **MAMBA MNAFIKI**
> 
> Hapo zamani za kale paliishi Mbuni, Kima na Mamba. Wote watatu waliishi kisiwani. Kisiwa hiki kiliitwa Kisiwa Mamba. Kisiwa Mamba kilizungukwa na ziwa lenye maji mengi sana. Marafiki hawa walipenda kupumzika kando ya ziwa hili.  
> 
> Kima aliishi juu ya mwembe mmoja mkubwa. Mwembe huo pia ulikuwa mrefu sana. Ulizaa maembe makubwa na matamu sana. Akiwa juu ya mwembe huo Kima aliona mbali. Aliona kila kitu hapo kisiwani. Mara nyingi Kima alikaa kileleni akile maembe.
> 
> ...
> 
> Wavuvi wale walizungumza mengi kumhusu Mamba. Hawakujua kwamba Kima alikuwa akiyasikia yote waliyoyasema. Hivyo basi waliendelea, "Rafiki yangu yule mamba si kidogo. Siku moja nilimsikia akipanga njama ya kumla mmoja wa marafiki zake. Siku hiyo walikuwa nyumbani na mkewe. Ole wao ikiwa Mbuni na Kima hawajui unafiki wake!"  
> 
> "Mbona hukuwaambia?" mvuvi mwingine aliuliza.  
> 
> "Nilidhani tu ni mzaha. Kwani si wale ni marafiki zake! Hata hivyo nikimsikia akisema hivyo tena nitawaambia," mvuvi wa kwanza alijibu.  
> 
> "Haya sawa! Tuyaache hayo. Hebu tuondokeni kwenda uvuvini ama mabibi zetu watalala njaa!" mvuvi wa tatu alisema. Kisha waliondoka.  
> 
> Maneno hayo ya wavuvi yalimshtua sana Kima. Hakujua ikiwa yalikuwa ya kweli au la. Aliamua kumwambia Mbuni. Lakini hakutaka Mamba ajue.  
> 
> Mbuni aliposikia maneno haya aliyapuuza.  
> 
> "Hiyo ni chuki ya binadamu dhidi yetu. Hawapendi urafiki wetu ukidumu," alisema Mbuni.  
> 
> "Si hivyo mwenzangu. Ingawa Mamba ni rafiki yetu lazima tujihadhari naye. Kinga ni bora kuliko tiba," Mbuni alijibu.  
> 
> "Tutajihadhari vipi na yeye ni rafiki yetu?" Mbuni alishangaa.  
> 
> "Kwa kukaa mbali naye," Kima alijibu.  
> 
> Basi Kima na Mbuni walianza kujitenga na Mamba. Kwa muda wa wiki mbili hawakumtembelea. Kima na Mbuni walishinda chini ya mwembe wakizungumza.

From *Mamba Mnafiki* by Bitugi Matundura, East African Educational Publishers Ltd, 2010.

{{% notice note %}}
**REMEMBER:**  
When checking a translation, ask:  
**Is it accurate?** Is the meaning as nearly as possible the same meaning that the original author intended?  
**Is it clear?** Will people understand the message?  
**Is it natural?** Is this the way that people speak?
{{% /notice %}}  

---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 32.