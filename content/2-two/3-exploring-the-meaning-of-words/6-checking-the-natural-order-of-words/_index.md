+++
title = "Checking the Natural Order of Words"
date =  2020-08-06T14:19:19-07:00
weight = 30
+++

In most languages there are words that are often used together. When this happens, often one order is bore natural than the other order.  

For example, in English the natural order is:  

* ladies and gentlemen - never "gentlemen and ladies." But some languages would use the order "gentlemen and ladies." Some East African languages say "men and mothers."  
* bread and butter - never "butter and bread"  
* black and white - not "white and black"  
* heaven and earth - not "earth and heaven"  
* chicken and rice - but in some East African languages, the natural order is "rice and chicken."  

The natural order may be different in another language.  

---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 65  