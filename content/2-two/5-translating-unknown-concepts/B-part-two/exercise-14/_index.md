+++
title = "Exercise 14"
date =  2021-11-24T09:51:43-08:00
weight = 40
+++

Each of the following passages includes one or more ideas that may be unknown in the Receptor Language area. There may also be some ideas that, though known, may not be easily expressed in the Receptor Language by a single word.

(1) Underline all the unknown ideas, or problem words.  
(2) Write down how you might re-express the meaning of the word or phrase you have underlined in a way that could be readily translated into your language. For each example, state which of the five possible methods for translating an unknown idea you have used. The five methods are:  

* Use a descriptive phrase  
* Substitute something similar  
* Use a foreign word from another language  
* Use a word that is more general in meaning  
* Use a word or phrase that is more specific in meaning  

1. Your womb, all-blameless Mother of God, is acknowledged to be a
heap of grain of a threshing floor, which carries ineffably, beyond
mind and beyond reason, a ear of grain untilled; which you bear in
the Cave of Bethlehem, and is about to nourish all creation by
grace with divine knowledge and to rescue humankind from starvation which destroys the soul. (Vespers Stichera for December 21)

1. Abraham once was sacrificing his son, foreshadowing the slaughter of him who possesses all things and who is now hastening to be born in a Cave; while you, inspired Father, offered your whole self as victim; and becoming fodder for the wild beasts, you appeared as pure wheat for your Creator, and you truly dwell eternally in heavenly barns, and delight in your love, for whom you left all the world behind, all-blessed, and were named Godbearer, glorious Ignatios. (Canon for St Ignatios the God-Bearer, Ode 6, December 20)

1. I was entrusted with a sinless and living country, but, having sown the ground with sin, with a sickle I have reaped the ears of indifference and piled up the heaps of the sheaves of my actions, which I have not spread out on the threshing floor of repentance. But I beg you, our God, husbandman before the ages, with the wind of your loving compassion winnow away the chaff of my works, and provision my soul with forgiveness; shut me in your heavenly storehouse and save me. (Vespers stichera for the Sunday of the Prodigal Son)

1. Unjustly driven from flock, venerable Father, you became familiar with afflictions and bitter exiles, by which you were counted worthy of a blessed end as a noble Athlete, throwing down the one of many wiles, and Christ crowned you with a diadem of victory, John Chrysostom, intercessor for our souls. (Lauds at Matins for St John Chrysostom, November 13)

1. Come, peoples, bearers of Christ, let us behold a wonder that amazes and holds fast every mind, and as we devoutly worship let us with faith sing its praise. Today a Maiden great with child is coming to Bethlehem to give birth to the Lord; choirs of Angels run before her. And seeing this Joseph, the Betrothed, cries out, ‘What is the strange mystery in you, O Virgin? And how will you bring forth a child, Calf who have never known the yoke?’ (Sixth Hour of the Great Hours for Christmas Eve, December 24)

1. Come then to me today with fervour, acceptable sacrifice of the faithful; as we stand together in choir let us garland with fitting songs Peter and Paul, the chosen weavers of grace; because they sowed the word unstintingly for all and enriched them with the gift of the Spirit; and being branches of the true vine, they have brought to perfection for us the ripe grape cluster, making glad our hearts. To them let us cry out, with faces unveiled and with pure consciences, as we say: Hail, guides of the unreasoning and servants of those with reason. Hail, fair chosen members of the maker and guardian of all. Hail, protectors of the good and persecutors of the deceitful. Teacher to give the world stable peace and our souls his great mercy. (Lity for Saints Peter and Paul, June 29)

1. Be dedicated anew brethren! And putting off the old man, live in newness of life, placing a bridle on all those things from which death comes. Let us discipline all our members, hating every evil eating of the tree, and so only remembering the old that we may flee it. Thus is mankind renewed, thus the day of the Dedication is honoured. (Vespers Stichera for the Forefeast of the Cross, September 13)

1. Ever pure, you are my hope, and you are my song, you are my harbour and you are my pilotage, you who without union bore God incarnate, the Word of the Father; therefore without doubting, empowered with your strength, I venerate your image. (Ode 4 of the Matins Canon for the Sunday of the Holy Fathers of the Seventh Ecumenical Council)

1. Hail throne of fire, hail lampstand all of gold, hail cloud of light, hail palace of the Word, and spiritual table, who fittingly carry Christ, the bread of life. (Ode one of the Matins Canon for the Sunday of the Samaritan Woman)

1. The whole world, let us praise as its champions the Disciples of Christ and foundations of the Church, the true pillars and bases, and inspired trumpets of the doctrines and sufferings of Christ, the Princes, Peter and Paul. For they passed through the whole breadth of the earth as with a plough, and sowed the faith, and they made the knowledge of God well up for all, showing forth the understanding of the Trinity. O Peter, rock and foundation, and Paul, vessel of election; the yoked oxen of Christ drew nations, cities and islands to knowledge of God. While they have brought Hebrews again to Christ and the intercede that our souls may be saved. (Lity for Saints Peter and Paul, June 29)

1. Once Daniel, with virtue as his companion, muzzled lions; imitate him, my soul, and by your return to God make the one who always roars like a lion and seeks to seize you ever without effect. (Ode 7 of the Matins Canon for Monday in the First Mode)

1. While the Alleluia is being sung, the Deacon takes the censer, receives the Priest’s blessing, and comes out and censes the holy Gospel. (Rubrics for the Alleluia at the Divine Liturgy of St James)

1. Persians, Ethiopians, Indians and Scythians and a multitude of Arabs recognised your wisdom, Father, and they glorified Christ who is glorified through you. (Ode 1 of the Matins Canon for September 1)

1. Having lived on earth a path of life unchanged by men, and crossed the stream of Jordan with his cloak, the Thesbite, as charioteer of the air, through the Spirit completed the strange path of his journey to heaven. (Ode 9 of the Matins Canon for the Prophet Elias, July 20)

---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, pages 47 - 48.  