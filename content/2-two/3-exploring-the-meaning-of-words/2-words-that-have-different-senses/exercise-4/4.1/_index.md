+++
title = "4.1"
date =  2021-12-14T15:42:05-08:00
weight = 5
+++


#### 1. Ode 3 of the Matins Canon for St Cyril of Jerusalem, March 18  

&nbsp;  

| Οἶκος | ἁγιάσματος | σοῦ | ἡ | ψυχὴ | ἀνεδείχθη, | ἐν | ᾧ | Πατὴρ | Υἱός | τε | καὶ |    
| ----- | ---------- | --- | - | ---- | ---------- | -- | - | ----- | ---- | -- | --- |  
| ikos | ayiasmatos | su | i | psyhi | anedihthi | en | o | Patir | Yios | te | ke |
| house | of holiness | your | the | soul | became, | in | it | Father | Son | both | and |  

&nbsp;

| Πνεῦμα | τὸ | ζωαρχικόν, | ὑπερφυῶς | κατῴκησεν, | ᾧ | ψάλλομεν· | Ἅγιος | εἶ | Κύριε. |
| ------ | -- | ---------- | -------- | ---------- | - | --------- | ----- | -- | ------ |  
| Pnevma | to | zoarhikon | yperfyos | katokisen, | o | psallomen. | Agios | i | Kyrie. |  
| Spirit | the | origin of life | extraordinarily | settled | to whom | we chant | holy | you.sg are | Lord. |

**SOT:** Your soul was shown to be a house of holiness * In it both the Father [and] the Son and the Spirit [who is] the origin of life settled extraordinarily * to whom we chant * Holy you are Lord *  

**DEDES:** O Saint, your soul became a house of holiness. In it marvellously dwelt the Father, Son, and life-originating Spirit, to whom we sing: "Holy are You, O Lord."

**NEW:** Holiness abides in your soul. Amazingly, the Father lives there, with the Son and the life-generating Spirit, to whom we sing: "Holy are you, O Lord."

**GEV:** Your soul was revealed as a home for holiness, where the Father, the Son and the life-making Spirit came to live wonderfully. We chant to God: "You are holy, O Lord."

**MOT:** Your soul was shown to be a place where holiness belongs. The Father and the Son and the Spirit who gives people life came to stay there in an extraordinary way. We chant these words to God: "Lord, you are holy."  

---

* **How is οἶκος ikos ‘house’ translated in these versions?**  

* **What is the real meaning of οἶκος ikos ‘house’ in this hymn?**  

* **What word(s) would you use in translating οἶκος ikos ‘house’ into your own language, for this hymn?**

