+++
title = "C. Special Words That Have One Sense"
date =  2020-08-06T14:33:41-07:00
weight = 15
+++

We will begin by concentrating on words that have the same meaning in every context. Words that have several different senses will be studied later.  

--- 

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 68  