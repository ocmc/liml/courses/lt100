+++
title = "E. Comparing Words With Similar Meanings"
date =  2020-08-06T14:54:05-07:00
weight = 19
+++

Words that have similar meanings should be studied as a group. Comparing them with each other helps to highlight more precisely:  

(1) the ways in which the meaning of the words are the same, and  
(2) the ways in which the meaning of the words are different.  

In the Bible, the words **synagogue**, **temple**, and **tabernacle** are similar in that they all refer to a building or shelter that is used for religious purposes. The differences in meaning between the three words can be compared as follows:  

| **tabernacle** | **temple** | **synagogue** |  
| -------------- | ---------- | ------------- |  
| place used by the Jewish people for religious purposes | same | same |  
| place where God was present in a special way | place where God was present in a special way | place where people regularly met for worship |  
| temporary shelter | permanent building | permanent building |  
| only one existed | only one existed (this one was in Jerusalem) | there were many; each town or village had its own synagogue |  
| people went there to take animals to be sacrificed | people went there to take animals to be sacrificed, also to pray, to teach and learn, to burn incense | people went there for the reading of the Laws of Moses, for prayer (sacrifices were not made there)  

In view of this comparative study, list three possible ways in which you might translate these three terms into your own langauge, arranging them in order of preference.  

---  

##### EXAMPLE 1  

The words **angel**, **cherubim**, **seraphim**, **evil spirit**, **demon**, and **Satan** all refer to spiritual beings. These are all personal beings; they have the power to speak and act. The chart below shows some of the **differences** between the words:  

| **angel** | **cherub** | **seraph** | **evil** or **unclean spirit** | **demon** | **Satan** |  
| --------- | ---------- | ---------- | ------------------------------ | --------- | --------- |  
| messenger from God | the throne of God | guardian of God's throne | enemy of God | enemy of God | enemy of God |  
| no true bodily (material) form | same | same | same | same | same | same |  
| represented as a person with wings | represented as an animal with wings, a human head, and the body of a lion or a bull | represented as a fiery serpent | sometimes represented as a small dark person | same | sometimes represented as a large person, sometimes with features of a goat or a bat |  
| serves in the presence of God | same | same | cannot stand the presence of God | same | same |  
| does not enter or possess a person | same | same | can enter a person an control him | torments people | torments people |  
| good | good | good | bad | bad | bad |  

##### EXERCISE 2  

Apply STEPS 1, 2, and 3 to the sets of Biblical words listed below. (Note: use their Biblical usage, not their usage in liturgical rubrics.)

(1) Levite, priest, high priest, scribe  
(2) apostle, disciple, believer


--- 

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, pages 71 - 72