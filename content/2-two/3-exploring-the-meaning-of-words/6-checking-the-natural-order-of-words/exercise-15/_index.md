+++
title = "Exercise 15"
date =  2022-01-06T15:59:05-08:00
weight = 5
+++

What is the natural order for the following pairs of words in your language?  

1. by night and day  
2. heaven and earth  
3. parents and children  
4. grace and compassion  
5. three hundred and sixty  
6. sun and moon  
7. old and new  

---

CITATIONS  

1. Vespers Stichera for Wednesday in the Plagal of the First Mode  
2. "Holy Holy Holy" in the Divine Liturgy  
3. Matins Kathisma for Saturday in the First Mode  
4. Prayer at the Bowing of the Heads in the Divine Liturgy of St John Chrysostom
5. Ode 1 of the Matins Canon for the Holy Fathers of the Fourth Ecumenical Council  
6. Lamentations for Holy Friday, Second Section  
7. Ode 9 of the Matins Canon for Tuesday in the First Mode  

---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 65