+++
title = "Meanings That Do Not Match"
date =  2020-08-06T13:10:17-07:00
weight = 5
+++

**EXAMPLE 1**

Here are three sentences in the Mbembe language of Nigeria, with an English translation:  
(a)  
| ochi | eten |
| ---- | ---- |
| he-eats | *eten* |  

"he eats **meat**"

(b)  
| owa | eten |
| --- | ---- |
| he-catches | *eten* |

"he catches a **fish**"

(c)  
| eten ndo | kk'erobha | z'egba |
| -------- | --------- | ------ |
| *eten* | has-run | to-bush |

"that **animal** has run off into the bush"

What does *eten* mean in English?  
In sentence (a) it means "meat."  
In sentence (b) it means "fish."  
In sentence (c) it means "animal."

The area of meaning which is covered by one Mbembe word, *eten*, is covered by three words in English: **meat**, **fish**, and **animal**. A translator translating from Mbembe into English would need to use different words to translate *eten* according to each situation.

This illustrates the fact that **words in some languages do not necessarily match up, one-to-one, with words in another language.**

**OTHER EXAMPLES**

In English there is one word **cloth**, but in Hausa there are three words:  
*zane* (a cloth which one ties round the waist),
*yadi* (cloth bought by the yard),
*kyalle*  

In Mundu (Sudan) there is one word *go*, in English there are three words that cover the same meaning:  

**grass**  
**flowers**  
**plants**  

All three of these English words could be translated by *go* in Mundu.

In Sudanese Arabic there is one word *mara*, in Greek there is one word γυνή gyni, in English there are two words:  

**wife**  
**woman**  

In Sudanese Arabic (and many African languages), there is one word *asma*, in English there are two words:  

**hear**  
**understand**  

*Asma* would be translated into English sometimes by "hear," sometimes by "obey" or "understand," depending on context.  

In Mbembe there is one word *okora*, in English there are three words,  

**red**  
**orange**  
**yellow**  

All these English words could be translated by *okora* in Mbembe.  

Similarly, in Mbembe there is one word *obina*, in English there are three words:  

**green**  
**blue**  
**black**  

Some languages have many more words for different colours than others do.  

{{% notice note %}}
**REMEMBER:**  
Because words in different languages do not match each other, the same word in the Source Language must sometimes be translated by different words in the Receptor Language.  
A word must always be translated by the meaning that it has in the particular context in which it occurs.
{{% /notice %}}

---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, pages 54 - 55.