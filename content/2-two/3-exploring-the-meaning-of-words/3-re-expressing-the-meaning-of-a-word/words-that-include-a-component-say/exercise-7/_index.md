+++
title = "Exercise 7"
date =  2021-12-22T12:43:59-08:00
weight = 5
+++

For each of the following passages:  

1) Underline any word that includes the component "say."  
2) Imagine you are translating into a language that always uses a speech quotation to express such words. Re-express in English each of the words you have underlined using a speech quotation. Be careful to check the context.  

---

1. [You Apostles] were commanded to declare [God's] mighty works to the end of the earth.  
1. [The Apostles] ask Christ God to grand peace to the faithful and his great mercy.  
1. confess all that you have done; make the Creator have pity on you.  
1. you did not refuse, O Good One, to show [the Apostle Thomas] your immaculate side, and the wounds in your hands and feet. But he, having handled and seen, confessed you to be not simply God and not merely man.  
1. [You], God of our fathers, [are] praised and glorified above all.  
1.  The One whom you insanely begged Pilate to hang upon a cross as a malefactor has abolished the power of death  
1. but permit me now without condemnation to cry out to you  
1.  you reproached the lawless union of a tyrant  (see Mark 6:18)  
1. You [martyrs] did not deny Christ  
1. Obey him then, or keep silence, misguided ones, who deny his Resurrection.  
1.  you condemned him to return again to the earth from which he had been taken, Lord, and to ask for rest.  
1. he... had blasphemed your gifts.  
1. God... approved you as a true Disciple  

---

#### CITATIONS

1. Ode 7 of the Matins canon for the Apostle Andrew, November 30  
1. Matins kathisma of Thursday in the Third Mode  
1. Vespers stichera for Monday in the First Mode  
1. Vespers stichera for Thomas Sunday  
1. Ode 7 of the Matins canon for Sunday in the First Mode  
1. Ode 4 of the Matins canon for Sunday in the First Mode  
1. Prayer at the Blessing of the Waters ("Trinity beyond all being...") on January 6  
1. Ode 3 of the Matins canon for the Feast of John the Baptist, October 29  
1. Vespers stichera for Two or More Martyrs  
1. Ode 4 of the Matins canon for Sunday of the Myrrh-Bearing Women  
1. Vesper aposticha for Saturday in the Grave Mode  
1. Ode 5 of the Matins Canon for September 6  
1. Matins kontakion for Mary Magdalen, July 22  

---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, pages 59 - 60