+++
title = "Key Terms: Spirit and Soul"
date =  2022-03-14T13:26:20-07:00
weight = 45
+++

Two key terms for spiritual entities that are important in Orthodox Christian liturgy are: **soul** (Greek πνεῦμα *pnevma*) and **spirit** (Greek ψυχή *psihi*).  

1. Look at the word **soul** in the following texts. Write down a list of definitions of **soul** based on the texts.  

**Biblical Texts**  

* And God said, "Let the water bring forth creeping things of living **souls** and winged things flying above the land." (Gen 1:20, LES)  

* And God formed the human with earth from the land and blew into his face the breath of life, and the human came into being as a living **soul**. (Gen 2:6, LES)  

* Righteous **souls** are in the hand of God... They seem to have died... but they are at peace. (Wisdom 3:1, LES)  

* 
