+++
title = "Exercise 2"
date =  2021-09-21T12:28:51-07:00
weight = 25
+++

In the following examples, which of the underlined words refer to things that happened at a specific time in history? And which refer to illustrations or teaching comparisons?  

1. You were lifted up, Lamb of God, on cypress, pine and **cedar**, that you might save those who worship in faith your voluntary Crucifixion. Christ our God, glory to you!  
*-- Matins Kathisma for Wednesday Morning in the Third Mode*  
1. By your virtues you were exalted as a lofty **cedar**, and you offered us sweet-scented fruits: the wealth of your teachings, the grace of your wonders and the operation of your healings, blessed High Priest Cornelius.  
*-- Canon for the Centurion Cornelius (September 13), Ode 3*  
1. Today the Saviour came to the city of Jerusalem to fulfil the Scripture. And all took **palms** in their hands, spread their garments before him, knowing that he is our God, to whom the Cherubim cry without ceasing, ‘Hosanna in the highest! Blessed are you, who have great compassion. Have mercy on us’.  
*-- Aposticha for Palm Sunday Vespers* (see John 12:12-13)  
1. All-immaculate Virgin, make my soul be bear fruits of virtues by driving away my unfruitful thoughts, and cutting out root and branch the **thorns** of sin that choke me.  
*-- Matins Canon for Wednesday Morning in the First Mode, Ode 5*  
1. Today is the **spring** of souls, for Christ, shining from the tomb like the sun, has dispelled the foggy **winter** of our sin. Let us sing to him, for he has been glorified.  
*-- Matins Canon for Thomas Sunday, Ode 1*  
1. Willingly you were bound with a **crown of thorns**, O Longsuffering, as true King, and you put an end to thorn-bearing sin by the roots; I confess your sufferings, O Saviour.  
*-- Matins Canon for Wednesday Morning in the Third Mode, Ode 4* (See Mark 15:17)  
1. The **seed** of your words, cast spiritually into the souls of men, multiplied the fruit of true religion, Apostle, and they shouted and sang: God of our Fathers, blessed are you! 
*--Matins Canon for an Apostle, Ode 7*  
1. The hearts of the godless and hostile have been wounded, for Christ, stretching the Cross taut like a **bow**, has discharged the Martyrs as sharpened **arrows.**  
*-- Canon for Two or More Martyrs, Ode 6*
1. Even as seven **trumpets** on the seventh circuit threw down the walls of Jericho; so the seven Councils brought down to chaos the whole band that had risen up against God, at the seventh assembly of the well-tuned **trumpets** of the Spirit.  
*--Canon for the Holy Fathers, Ode 4*



---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 39.  