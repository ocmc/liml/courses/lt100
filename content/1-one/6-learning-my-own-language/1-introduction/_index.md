+++
title = "Introduction"
date =  2022-03-03T14:12:08-08:00
weight = 5
+++

In order to communicate the meaning of the source text accurately and effectively in the Receptor Language, it is necessary to study carefully the culture of the area where the Receptor Language is spoken. In particular, study the meaning of words and expressions that refer to traditional beliefs and to the supernatural world.  

--- 

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 76