+++
title = "Some Definitions for Discussion"
date =  2022-08-10T17:32:01-07:00
weight = 10
+++

Look at the definition of "translation" that you wrote down in Exercise 1. Compare your definition with definitions that other classmates wrote down. Then, read the following definitions of translation, and discuss them with your classmates. Do these definitions all mean the same thing? What do you agree with? What do you disagree with? How would you change your definition?  

### What is Translation?  

> The task of the translator consists in **finding that intended effect upon the language into which he is translating whiche produces in it the echo of the original.**  
> -- Walter Benjamin, "The Task of the Translator," 1923  

> [Translation is] **to reproduce with absolute exactitude the whole text, and nothing but the text.**  
> -- Vladimir Nabokov, "_Onegin_ in English," 1955  

> [Translation is] **an interpretation of verbal signs by means of some other language.**  
> -- Roman Jakobson, "On Linguistic Aspects of Translation," 1959  

> Translation consists in **reproducing in the receptor language the closest natural equivalent of the source-language message**, first in terms of meaning and secondly in terms of style.  
>  -- Eugene Nida and Charles Taber, _The Theory and Practice of Translation_, 1969, page 12  

> Translation consists of **transferring the _meaning_ of the source language into the receptor language.**  
> -- Mildred Larson, _Meaning-Based Translation: A Guide to Cross-Language Equivalence, Second Edition_, 1998, page 3  

> [Translation is] **investing the foreign-language text with a domestic significance.**  
> -- Lawrence Venuti, "Translation, Community, Utopia," 2000  

> Translation is **the attempt to represent in one language a text produced in another language** (or in other languages).  
> Timothy Wilt, _Bible Translation: Frames of Reference_, 2002, page 233  

> [Translation is] **retelling an original message in a different language.**  
> -- Hill, Gutt, Hill, Unger, and Floyd, _Bible Translation Basics: Communicating Scripture in a Relevant Way_, 2011, page 97  

> Translation is **retelling, as exactly as possible, the *meaning* of the original message** in a way that will be understood by the speakers of the language into which the translation is being made.  
> -- Katharine Barnwell, _Bible Translation: An Introductory Course in Translation Principles, Fourth Edition_, 2020, page 4
