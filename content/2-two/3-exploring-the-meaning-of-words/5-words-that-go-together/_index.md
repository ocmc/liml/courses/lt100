+++
title = "Words That Go Together"
date =  2020-08-06T14:18:06-07:00
weight = 25
+++

#### EXAMPLE 1  

In English, it is idiomatic to say "he suffers" or "he **has** a lot of suffering." In many languages, it is natural to say "he **sees** suffering" or "he **drinks** suffering."  

Many words have a "natural partner." We say they **collocate**, or "go together with" certain other words.  

In each particular language, the natural **collocation** of a word is the other word or words with which it commonly occurs. For example, the words "bread and butter" are a common collocation in English. Other natural collocations are:  

"to sing a song"  
"to keep a law"  
"to drive a car"  

Often the meaning of the word changes depending on the collocation.  

--- 

#### EXAMPLE 2  

In the Mbembe language, the word *chi* 'to eat' can be used in a number of collocations:  

| chi | odaang |  
| --- | ------ |  
| eat | foufou |   

'eat foufou'  

&nbsp;

| chi | akpuka |  
| --- | ------ |  
| eat | money |  

'embezzle money'  

&nbsp;

| chi | eden |  
| --- | ------ |  
| eat | path |  

'go first'  

&nbsp;

| chi | ngwo |  
| --- | ------ |  
| eat | bribe |  

'take a bribe'  

&nbsp;

| chi | akpen |  
| --- | ------ |  
| eat | life |  

'live it up (high life)'  

&nbsp;

| chi | onong |  
| --- | ------ |  
| eat | person |  

'cheat someone'  

---

#### EXERCISE 12  

1. What is a word for "eat" in your own language?  
1. Make a list of words with which this word can naturally collocate.  
  Give (a) a word-for-word key in English  
  and (b) an idiomatic translation in English showing the natural phrase with the same meaning.  

  ---  

  ### REMEMBER:  

  A common mistake that translators make is to translate too literally, carrying over the collocation of words in the Source Language. The result is an unnatural and often confusing translation.  

  Avoid the mistake of carying over collocations from the source language that would be unnatural in the Receptor Language. Instead, use the collocation that is natural in the Receptor Language.  

  ---  

  **FURTHER EXAMPLES of collocations that do not match in different languages:**  

  **Mark 1:6**  
  English: **ate** locusts and wild honey  
  Jukun (Nigeria):  **ate** locusts and **drank** wild honey.  

  **Mark 1:9**  
  English:  **mending** their nets  
  Jukun:  **tying** their nets  

  **Luke 5:9**  
  English:  to **catch** fish  
  Many languages:  to **kill** fish  

  **Mark 2:22**  

  Greek:  

  | οὐδεὶς | βάλλει | οἶνον | νέον | εἰς | ἀσκοὺς | παλαιούς |  
  | ------ | ------ | ----- | ---- | --- | ------ | -------- |  
  | udis | valli | inon | neon | is | askus | paleus |  
  | no-one | casts | wine | new | into | wineskin | old |  

RSV:  No one **puts** new wine into old wineskins  
GNB:  Nor does anyone **pour** new wine into used wineskins

**Mark 6:2**  
English:  What is the wisdom **given** to him?  
Ezaa (Nigeria):  Who **taught** him this kind of wisdom?  

**Mark 3:6**  

Greek:  

| οἱ | Φαρισαῖοι | συμβούλιον | ἐποίουν | κατʼ | αὐτοῦ |  
| -- | --------- | ---------- | ------- | ---- | ----- |  
| i | Farisaei | simvulion | epiun | kat | aftu |  
| the | Pharisees | counsel/ plan | gave | about | him |  

Greek:  **gave** plan  
RSV:  **held** counsel  
KJV:  **took** counsel  
NIV:  began to **plot**  
GNB:  **made** plans  

**Revelation 11:1**  

| μέτρησον | τὸν | ναὸν | τοῦ | θεοῦ, | καὶ | τὸ | θυσιαστήριον, |  
| -------- | --- | ---- | --- | ----- | --- | -- | ------------- |  
| metrison | ton | naon | tu | theu | ke | to | thysiastirion |  
| measure | the | shrine | of | God | and | the | altar |  

&nbsp;

| καὶ | τοὺς | προσκυνοῦντας | ἐν | αὐτῷ |  
| --- | ---- | ------------- | -- | ---- |  
| ke | tus | proskynuntas | en | afto |  
| and | the | ones-worshipping | in | it |  

RSV: **measure** the temple of God and the altar and those who worship there  
GNB: **measure** the temple of God and the altar, and **count** those who are worshipping in the temple.



---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, pages 62 - 64    