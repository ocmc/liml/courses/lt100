+++
title = "Exercise 3"
date =  2021-09-22T12:40:09-07:00
weight = 30
+++

In the passages below, first underline any words that refer to ideas that are unknown in your language area. How might you translate each of the ideas you have underlined? Look up the context in an associated passage of the Bible when possible.

1. The Forerunner says to the peoples of the disobedient, `Who has showed you to flee the wrath that is coming, broods of vipers?* Demonstrate fruits worthy of repentance then, by crying out, "Spare our souls, Christ God, and save us".  
*--Canon for the Forefeast of Theophany (January 3), Ode 9*  (See Matthew 3:7)   

1. Like a burning lampstand
here the flesh of our God,
as beneath a bushel measure, now lies concealed
under earth and puts the gloom of Hell to flight.  
*--Lamentations of Holy Saturday Matins, First Section*  (See Matthew 5:15)  

1. The Champions, O Christ, have now clothed themselves in a most fair robe, devoutly empurpled with the blood of martyrdom and cloaked in your grace.  
*Canon for Two or More Martyrs, Ode 5*  

1. Finding you undefiled among thorns as a most pure lily and flower of the valleys, Mother of God, the Word the Bridegroom dwelt in your womb.  
*--Canon for St Demetrios, Ode 6*  

1. Being godly branches of the spiritual vine, glorious Apostles, you sprouted with godly grape clusters which pour out the wine of salvation. And so rescue me from the drunkenness of pleasures.  
*--Matins Canon for Thursday in the First Mode, Ode 9*  

1. I have appeared as a fruitless fig tree and I quail before the axe; better me by your mediation and make me fruitful, that I may call you blessed, O Forerunner of the Saviour.  
*-- Matins Canon for Tuesday in the Fourth Mode, Ode 7*  (See Luke 13:6-9)  

1. Wretch that I am, I have cast off the robe woven by God, disobeying your divine command, Lord, at the counsel of the enemy, and I am clothed now in fig leaves and in garments of skin. I am condemned to eat the bread of toil in the sweat of my brow, and the earth has been cursed so that it bears thorns and thistles for me. But, Lord, who in the last times were made flesh of a Virgin, call me back and bring me into Paradise again.  
*-- Vespers Stichera for Forgiveness Sunday* (See Genesis 3:18)  

1. You were revealed, Luke, as a harp, voicing sweet melodies from God, enlightening mortals as they cry, ‘Blessed are you, Lord God to the ages!’  
*-- Canon for the Apostle Luke (October 18), Ode 7*  

---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 40.  