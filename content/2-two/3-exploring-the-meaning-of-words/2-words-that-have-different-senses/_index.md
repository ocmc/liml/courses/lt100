+++
title = "Words That Have Different Senses"
date =  2020-08-06T13:11:45-07:00
weight = 10
+++

Within one language, the same word may have a number of different senses.

**EXAMPLES from English**

* A "nail" is a pointed piece of metal used for carpentry work, but a "fingernail" is the hard substance at the tip of a finger.  
* A "foot" is a part of the body, the lower end of the leg, but the "foot" of a mountain means the lower part of the mountain, and a "foot" is also a measurement, twelve inches in length.  
* A "board" is a long, thin, flat piece of wood, but the "board" of a school means the "governing committee" of the school.  

Notice that it is the **context** in which a word occurs that shows in which sense it is being used.  

{{% notice note %}}
**REMEMBER:**  
When translating from one language to another, the different senses of a word may need to be translated by different words in the Receptor Language.
{{% /notice %}}


**EXERCISE 1**  

For the examples above, write down how you would translate each of these words into your own language:

nail  
fingernail  

foot  
foot of a mountain  
foot (measurement)  

board  
the board of a school  

You will probably find that for at least some of these words you need to use different words in your language for the different senses of the Source Language word.  

{{% notice note %}}
**REMEMBER:**  
Always translate a word according to its sense in the particular context in which it occurs.
{{% /notice %}}


**EXERCISE 2**  

Give three examples from your own language of words that have different senses.  

For example, in one language,  

*ogbangbang* can mean  
1. "zinc roof of a house"  
1. "an enamel basin"  

*fohno* can mean  
1. "to wipe" as in *ofohno okpokoro* 'he wiped the table'  
1. "to iron" as in *ofohno ibara* 'he ironed the cloth'  
1. "to sacrifice" as in *ofohno eja* 'he made a sacrifice'  

NOTE: In many non-European languages, the **tone** on which a word is pronounced makes a difference to the meaning. 
For example, in one language, *èfá* (low-high tone) means "dog," while *éfà* (high-low tone) means "power." 
Such examples are **not** different senses of the same word, they are quite different words.

{{% notice note %}}
**REMEMBER**  
Someone who is translating from a language that is not his own mother tongue may sometimes not be aware of all the different senses that a word has
in the source language. He may think only of the most usual meaning of the word and may translate that meaning, without realising
that the word may have different meanings in different contexts.  
*KEEP ALERT* to recognize the correct sense of each word in the particular context in which it occurs.
{{% /notice %}}


**EXERCISE 3**

> For each of the following examples, check carefully the actual meaning of the word in bold. 
Use a good English dictionary for this purpose. How would you translate each of the bold words into your own language?

1.  The **Order** of the Sacrament of Holy Baptism (Baptism, tr. Lash)
1. Make him/her glad in all the works of his/her hands and in all his/her **race** (Baptism, Prayer for the Making of a Catechumen, tr. Lash)
1. Come and depart from one who is being made ready for holy **Enlightenment**. (Baptism, Second Exorcism Prayer, tr. Lash)
1. search out and try him (her), driving away from him (her) every **operation** of the devil (Baptism, Third Exorcism Prayer, tr. Lash)
1. nor despise those who **cry** out with faith (Canon of Holy Unction, 8th Ode, tr. Lash)  
1. Preserve them, Lord our God, as you preserved Noah in the **Ark**. (Matrimony, Second Crowning Prayer, tr. Lash)

---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, pages 55 - 57  