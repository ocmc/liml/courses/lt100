+++
title = "Other Areas of Meaning That Should Be Studied"
date =  2022-03-11T10:16:14-08:00
weight = 55
+++

1. Make a study of words in the Receptor Language that refer to **persons who have any kind of religious function in that culture**. Are there terms for:   

* people who speak on behalf of the political leader? (king, president, prime minister, parliament, council of elders, chief, etc)  
  (This may help you to find a good expression to translate "prophet".)  
* people who carry messages for the political leader?  
  (This may help you to find an expression to translate "angel.")  
* people who represent the political leader?  
  (This may help you to find an expression for "apostle.")  
* people whose job it is to offer sacrifices?  
  (This may help you to find an expression for "priest.")  
* diviners? (From whom do they get their insight?)  
* holy people who have died and who help living people?  
  (this may help you to find an expression for "saint.")  

In choosing known terms from within the culture to translate special terms, the translator should also test out the proposed words carefully, to find out whether they are suitable. Sometimes such terms are so closely associated with fetish worship that they cannot be used to refer to Christian ideas. But often the terms can be successfully modified. It is a great help to communication if you can build on an idea that is already known.    

There are several words in liturgical texts that express different ways of interacting with God or the saints: pray, worship, venerate, adore, reverence, hymn, sing to, cry out to, etc. In fact, liturgical Greek has more words for 'pray' than English does. In preparing to translate these terms, it will be helpful to list all the words for different ways that humans interact with the spiritual world that can be found in the Receptor Language, and to compare them. These will not match up one-to-one with English or Greek words. The important point in translating such terms is, of course, to convey the **total meaning** of the whole message, even if words do not match up one-to-one.

--- 

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, pages 77 - 8