+++
title = "Exercise 2"
date =  2021-06-22T13:58:50-07:00
weight = 17
+++

### Part One: Prose  

Take a short passage written in your own language. Two or three sentences is enough.   

1. First, word-by-word, underneath each word in the language,
write the English for that word. Does this make a translation that someone will understand?  
1. Then make a clear translation, expressing the meaning
of the passage in a natural way in English.  
1. Discuss your translation with someone who speaks English as their first language.  

If you know Kiswahili, you could use this passage:  

> Badala ya kulijibu, Dzombo alizinduka kwa ghafla. Huku akipumua kwa utaratibu, akajikuta akibembeshwa na kitanda. Ni baada ya macho kumwangaza sawa sawa ndipo alipomwona Mtu-Bint Fikirini mlangoni ni kisinia mkononi. --From *Walenisi* by Katanga Mkangi, page 137[^1]  

[^1]: Katama Mkangi, *Walenisi*. Nairobi: East African Educational Publishers Ltd, 1995

### Part Two: A Song  

Now do the same thing with a song written in your language. One verse-- between two and six lines, for example-- is enough.  
1. First, word-by-word,
underneath each word in the language, write the English for that word. Does this make a translation that someone will understand?  
1. Then make a clear
translation, expressing the meaning of the passage in a natural way in English.  
1. Now try to sing the song in English. Although the translation can
be understood, can it be sung in the same way as the original? How would your English translation have to be adjusted in order to be sung?  
1. How is translating a song different from translating a story?
1. Discuss your translation with someone who speaks English as their first language.  

If you know Kiswahili, you could use some lines from this song:  

> Penzi hujengwa na mapungufu yetu  
> Kubishana mara nyingi ndo kujifunza kwetu  
> Nayoona na kusikia hayawi mwisho wangu  
> Kumfanya abadilike kuwa mpenzi bora kwangu  
>  
> Ingawa nawashukuru kwa tarumbeta mnazopiga  
> Msipige sana mkakufuru kukosea kupo jama  
> Sala nasali sijali napenda pasi na shaka  
> Kuruka ruka ka kunguru si jambo ninalotaka  
>
> Atabadilika yatakwisha, ameshajifunza kwa makosa  
> 
> -- "Atabadilika" by Lameck Ditto[^2]  

[^2]: Lameck Ditto, "Atabadilika." Slide Digital, 2017. *Spotify*, open.spotify.com/track/5BHudwcksbPWZroYcyWAnz?si=189c331e905e417f

---

Adapted from *Bible Translation: An Introductory Course in Translation Principles, Fourth Edition* by Katharine Barnwell, 2020, page 4.   