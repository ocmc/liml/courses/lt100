+++
title = "Exercise 3"
date =  2021-09-13T11:52:58-07:00
weight = 30
+++

Translate the following story into your own language.  

As in previous exercises, underline places in your translation where you have used expressions or grammar which are different from the Source Language. Discuss these with an adviser or colleague. Also, if possible, discuss your translation with other speakers of the language.  

> One day, a poor man took a pot of milk to market. "I shall get a good price for this milk," he thought. "I will buy some eggs with the money. One of our hens  will sit on them and then I shall have a dozen little chicks. Soon the chicks will become hens. They will all lay eggs. Then I shall buy a calf with the money. The calf will grow into a cow too. Soon I shall have a dozen cows. I shall be a rich man."  
>  
> The man was very happy. He clapped his hands with joy. The pot of milk fell off his head. And that was the end of all his dreams.  

{{% notice note %}}
**REMEMBER:**  
When checking a translation, ask:  
**Is it accurate?** Is the meaning as nearly as possible the same meaning that the original author intended?  
**Is it clear?** Will people understand the message?  
**Is it natural?** Is this the way that people speak?
{{% /notice %}}

---  

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 33.  