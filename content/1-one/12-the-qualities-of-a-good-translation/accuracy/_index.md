+++
title = "A. Accuracy"
date =  2021-08-26T14:49:22-07:00
weight = 5
+++

When judging whether a translation is good or bad, there are three questions to ask:  

## 1. Is the translation **ACCURATE**?  

Does it communicate the **exact meaning** of the original message? Has the meaning been changed in any way?  

Notice that an accurate translation is **not** one that is as near to the grammatical form of the original message as possible, but one that expresses the same meaning as exactly as possible.  

A translation is **inaccurate** if the meaning of the translation is different in any way from the original message. This may include:  

**Omission:**  
The translation is inaccurate if part of the meaning is missing.  

**Addition:**  
The translation is inaccurate if anything has been added to the meaning.  

**Change:**  
The translation is inaccurate if the meaning has been changed or twisted in any way.

### EXERCISE 1  

For each pair of sentences, state whether the two sentences are:  
(1) The same in meaning (they are different ways of saying the same thing) or  
(2) different in meaning.  

1. (a) It rained all night.  
(b) Rain fell all night.  

1. (a) There is a book on the table.  
(b) There is a book on the desk.  

1. (a) John was very surprised when he heard the news.  
(b) The news very much amazed John when he heard it.  
1. (a) It was a hot day.  
(b) The day was hot.  
1. (a) Peter's house.  
(b) The house that belongs to Peter.  
1. (a) Mary's dress is too short for her.  
(b) Mary's dress does not fit well.  
1. (a) I bought cloth to make a new dress for Mary.  
(b) I made a new dress for Mary.  
1. (a) I bought vegetables in the market.  
(b) I bought tomatoes and onions in the market.  
1. (a) My parents are well.  
(b) My mother and father are well.  
1. (a) John is ill; he has malaria very badly.  
(b) John is very ill indeed.  
1. (a) There are four rooms in the house.  
(b) The house has four rooms and a kitchen at the back.  
1. (a) In my opinion the government is doing well and making many improvements in the country. But there are many people who do not agree that this is so.  
(b) Opinions are divided concerning the government. Some say they are doing well and making many improvements in the country. Others do not agree.  


### EXERCISE 3

For the following examples, selected from the Divine Liturgy, compare the NEW version with the Lash translation and, for each example, state:  

(1) Whether the NEW version is accurate or inaccurate.  
(2) If the NEW version is inaccurate, also state why it is inaccurate, i.e., what omissions, additions, or changes have been made.  

**NOTE:** You are not evaluating the style of the NEW translation; you are only evaluating its accuracy.

1. (Lash) For the peace from on high and for the salvation of our souls, let us pray to the Lord.  
(NEW) Let us pray to the Lord to give us peace that comes from above and to save our souls.  

1. (Lash) For the peace of the whole world, for the welfare of the holy Churches of God, and for the union of all, let us pray to the Lord.  
(NEW) For the peace of the whole world, for the welfare of the holy Churches of God, and for the union of all men, let us pray to the Lord.  

1. (Lash) At the prayers of the Mother of God, O Saviour, save us.  
(NEW) Through the intercessions of the Mother of God, Saviour, save us.  

1. (Lash) Stand upright.  
(NEW) Please get up out of your chairs.  

1. (Lash) Be quick to help us who cry to you with faith.  
(NEW) Help us quickly as we cry out to you faithfully.  

1. (Lash) Holy God, Holy Strong, Holy Immortal, have mercy on us.  
(NEW) Holy God, Holy Strong, Holy Indestructible, have mercy on us.  

1. (Lash) We, who in a mystery represent the Cherubim, and sing the thrice-holy hymn to the life-giving Trinity, let us now lay aside every care of this life.  
(NEW) Since we represent the Cherubim in a mystical way, and since we are singing the triple-holy hymn to the Trinity who gives us life, let's stop worrying about anything else in life.  

1. (Lash) Let our hearts be on high.  
(NEW) Lift up your hearts.  

1. (Lash) Greater in honour that the Cherubim and beyond compare more glorious than the Seraphim, without corruption you gave birth to God the Word.  
(NEW) You are more honourable and glorious than any of the angels. You are the mother of God the Logos even though you are a virgin.  

1. (Lash) Forgive us our debts, as we forgive our debtors, and do not lead us into temptation.  
(NEW) Forgive us our sins, as we forgive those who sin against us. Save us from the time of trial.

1. (Lash) Bow your heads to the Lord.  
(NEW) Let us bow our heads to the Lord.  

1. (Lash) Because you are our sanctification, and to you we give glory.  
(NEW) For You are our sanctification, and we ascribe glory to You.  

---
From *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, pages 24 - 25.