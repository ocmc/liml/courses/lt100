+++
title = "How to Study the Culture of Your Own People"
date =  2022-03-03T14:31:17-08:00
weight = 30 
+++

1. Spend time sitting with the older people of the area. Ask them to explain the meaning of words and phrases that have to do with traditional beliefs and customs. Listen to their discussion. From their agreements and disagreements you will learn a great deal. Often it will be helpful to record some of the discussion on a cassette or tape recorder, so that you can study it in more detail later.  

1. Studies of important words can also be very helpful. In order to discover the exact meaning of a particular word in the Receptor Language, compare and contrast it with other words in the same area of meaning. Do this in the way that has been explained in Chapter 8, section 4.  

| **REMEMBER:** |  
| ------------- |  
| Be ready to listen and learn. |    

--- 

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, pages 76 - 77