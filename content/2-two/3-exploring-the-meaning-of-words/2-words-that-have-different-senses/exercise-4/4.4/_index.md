+++
title = "4.4"
date =  2021-12-14T15:46:59-08:00
weight = 20
+++

#### 4. The Great Litany

| Ὑπὲρ | τοῦ | ἁγίου | οἴκου | τούτου, | καὶ | τῶν | μετὰ |  
| ---- | --- | ----- | ----- | ------- | --- | --- | ---- |  
| Yper | tu | agiu | iku | tutu | ke | ton | meta |  
| For the sake of | the | holy | house | this, | and | those | with |  

&nbsp;  

| πίστεως, | εὐλαβείας | καὶ | φόβου | Θεοῦ | εἰσιόντων | ἐν | αὐτῷ, | τοῦ | Κυρίου | δεηθῶμεν. |  
| -------- | --------- | --- | ----- | ---- | --------- | -- | ----- | --- | ------ | --------- |  
| pisteos | eflavias | ke | fovu | theu | isiondon | en | afto | tu | Kiriu | dheithomen |  
| faithfulness, | reverence | and | fear | of God | entering | into | it | the | Lord | let us ask |  

**SOT:** For the sake of this holy house, and those who enter it with faithfulness, reverence, and fear of God, let us pray to the Lord.  

**LASH:** For this holy house, and for those who enter it with faith, reverence and the fear of God, let us pray to the Lord.  

**GEV:** For the sake of this holy place, and of the people entering it who are faithful, respectful, and God-fearing, let us pray to the Lord.

**MOT:** Let us pray to the Lord for the sake of this holy place and for the sake of the people entering it who are faithful to God, who respect God, and who hold God in awe.

---

* **How is οἶκος ikos ‘house’ translated in these versions?**  

* **What is the real meaning of οἶκος ikos ‘house’ in this hymn?**  

* **What word(s) would you use in translating οἶκος ikos ‘house’ into your own language, for this hymn?**