+++
title = "Continuing to Study"
date =  2022-03-14T12:47:42-07:00
weight = 60 
+++

Studies of this kind need to be done over a period of time. You may have to wait for the right opportunity to discuss difficult words with those who can help you best. Look for people who have a reputation for being good speakers of the language, and who know the culture well.  

Remember that it is best not to make final decisions too quickly on how to translate key biblical words. Allow time for all decisions to be well tested and for new insights to come.

--- 

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 78  