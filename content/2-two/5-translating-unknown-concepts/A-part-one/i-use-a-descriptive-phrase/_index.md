+++
title = "i. Use a Descriptive Phrase"
date =  2021-09-15T14:22:49-07:00
weight = 10
+++

Instead of a single word, it may be necessary to use a phrase to describe the thing that is being talked about.    

### **EXAMPLES**  

(The symbol --> means "can be re-expressed as.")  

a. **pen** --> "a thing for writing with"  
b. **watch** --> "a thing for measuring how the sun goes."  
c. **aeroplane** --> "a canoe which travels in the sky"  
d. **altar** --> "place/ table where people sacrifice to God"  
e. **leaven/ yeast** --> "thing which makes bread swell"  
f. **gardener** --> "person who looks after the farm" (Canon for Mary Magdalen, Ode 7)

**IMPORTANT:** It will not be possible or good to give a **full** description of the meaning of the word. **Describe the part of the meaning which is important in the particular passage which is being translated.**  

**wolf** --> "fierce wild animal"  
*Canon of Theophany, Ode 3:*  
Seeking the sheep, O Christ, which the savage **wolf** had scattered by guile, you have entered the streams of Jordan, as you cry to the Forerunner: Come, baptize me! (Lash)  

There are many things which could be said to describe a wolf, but what is important in this hymn is that the worlf is a fierce animal which frightened the sheep.  

**anchor** --> "heavy iron weight used to keep a boat still"  
*Canon for Tuesday in the First Mode, Ode 4:*  
The godly Athletes sailed undrenched through the fierce and bitter storm of torments. Steered by saving hope, hearts filled with gladness, they were brought to **anchor** at the harbours on high.  

Here it is not the shape of the anchor that is important but what it is used for.  

**pearl** --> "valuable stone found at the bottom of the sea"  
*Canon for St Basil, Ode 7:*  
Sinking your mind deep into the unfathomable depths of God, and gathering the **pearl** of knowledge of great price, you enriched the world with wisdom and taught it to cry: Blessed are you the God of our Fathers.  

One disadvantage of this solution is that it can sometimes give a long and rather clumsy translation. The translator must be careful to fit the descriptive phrase neatly into the passage so that it does not upset the balance of the message or draw attention away from the main theme.  

If the word is mentioned several times in a prayer or hymn or service, it may not be necessary to use the full descriptive phrase each time it is mentioned. For example, "synagogue" might be translated "the house where the Jews met to worship God" in the first occurence, and then, when mentioned again later in the text, be referred to simply as "that meeting house."  

**Compound words.**  

A particular kind of "descriptive phrase" is a compound word. In some languages, two or more words can be joined together to have a special meaning:  

**throne** --> "chieftancy-seat"  
*Divine Liturgy of St John Chrysostom:*  
Blessed are you on the **throne** of glory of your Kingdom, who are seated upon the Cherubim, now and for ever, and to the ages of ages.  

**bank** --> "money-house"  

**shepherd** --> "sheep-watcher" or "sheep-watch"  
*Eothinon 11*  
Showing yourself to your Disciples
after the Resurrection, O Saviour,
you entrusted to Simon the pasturing
of the sheep, as repayment for
love, demanding the care of shepherding.
And therefore he said: If
you are my friend Peter, **shepherd**
my lambs, **shepherd** my sheep.
But he at once showing his affection,
inquired about the other Disciple.
At their intercessions, O Christ,
guard your flock from wolves that
ravage it.  

In making compound words, be careful to follow the natural patterns of the Receptor Language.

---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, pages 36 - 37.