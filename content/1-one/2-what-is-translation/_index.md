+++
title = "What is Translation?"
date =  2020-08-05T11:07:56-07:00
weight = 10
chapter = true
pre = "<b>2. </b>"
+++

### Chapter 2

# What is Translation?  

###  Introducing the Principles of Orthodox Christian Liturgical Translation  

---  

#### After completing this lesson, you will:  

* understand the basic principle of what translation is;  
* have a personal understanding of what your goal as a translator is;  
* be able to give a definition of translation in your own words.  
  