+++
title = "A. Some Terms to Remember"
date =  2021-09-03T14:59:30-07:00
weight = 5
+++

> **SOURCE LANGUAGE**  
> The source language is the language *from which* a translation is being made.  
> If a person is translating *from English* into Hausa, then the *Source Language* is *English*.  
> **RECEPTOR LANGUAGE**  
> The Receptor Language is the language *into which* a translation is being made.  
> If a person is translating from English *into Hausa*, then the *Receptor Language* is *Hausa*.  

If a person is translating from Greek into English, which language is the Source Language?  
Which language is the Receptor Language?  

If a person is translating from English into Arabic, which language is the Source Language?  
Which language is the Receptor Language?  

**REMEMBER:**  
1. The terms Source Language and Receptor Language are often shortened to SL and RL.  
1. The text or passage which is being translated is referred to as the **source text**.  
1. The Source Language is not necessarily the same as the Original Language. For example, if a translator is translating the Divine Liturgy from English into his own language, the Source Language is English. The Original Language of the Divine Liturgy, however, is Greek.  
1. Another word which is sometimes used instead of Receptor Language is **Target Language**. Receptor Language and Target Language mean exactly the same thing.  

---  

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, page 30.