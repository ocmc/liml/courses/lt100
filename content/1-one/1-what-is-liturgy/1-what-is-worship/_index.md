+++
title = "What Is Worship?"
date =  2022-07-23T15:32:06-07:00
weight = 5
+++

> **We make mention also of the Seraphim, whom Esaias by the Holy Ghost beheld encircling the throne of God, and with two of their wings veiling their countenances, and with two their feet, and with two flying, who cried, HOLY, HOLY, HOLY, LORD GOD OF SABAOTH. For this cause rehearse we this confession of God, delivered down to us from the Seraphim, that we may join in Hymns with the hosts of the world above.**  (Cyril of Jerusalem, "Mystagogical Catechesis V")[^1]  

> **During the Anaphora at the Divine Liturgy, we mention all of God's creation, including the Seraphim. Isaiah, inspired by the Holy Spirit, saw the Seraphim in a circle around the throne of God. They covered their faces with two of their wings. They covered their feet with two of their wings. They flew with two of their wings. They cried out, "HOLY, HOLY, HOLY, LORD GOD OF SABAOTH." We repeat this confession of who God is, a confession that the Seraphim themselves gave us. We repeat it so that we may join in hymns with the angels of heaven.**  (Paraphrase by Dn James Hargrave)
>  
[^1]: Cyril of Jerusalem, "Mystagogical Catechesis V on the Eucharistic Rite," *Lectures on the Christian Sacraments: The Procatechesis and the Five Mystagogical Catecheses*, ed. F. L. Cross and John Behr, Popular Patristics Series, Number 2 (Crestwood, NY: St Vladimir’s Seminary Press, 1977), 73–74.  

### Who is Involved in Worship?  

Worship is an activity done by the entire Church. When each of us comes together for worship, we do so as members of a Church which goes beyond the borders of our own place, our own time, our own culture, and our own language. When we come together for worship we are in a particular place, at a particular time, using a particular language. However, our words and actions reach beyond those particulars. Our words and our actions take place in the very Kingdom of God. We are worshiping together with all faithful people-- even the ones who have already died, or who haven't been born yet.

There are two dimensions to Orthodox Christian worship.

1. First: worship is a sign that **God is present with his people. God is acting in the middle of his people.** God is the one who gathers people together, even though they are distributed across space and time. God is the one who reveals himself when we come to the place where he is. The worship of the Orthodox Church expresses the truth that God has made his home among his people and that we are created to share in his life.  **Worship is where God comes close to us.**  
1. Second: worship is  **how we respond to God by giving thanks to him in his presense.** Worship is how we remember what he has done that saves us-- especially the life, death, and resurrection of Jesus Christ. Orthodox Christian worship is centered upon God. He has acted in history and he continues to act through the Holy Spirit. We are aware of his actions and we respond to his love by praising and thanking him. In so doing we come closer to God.  **Worship is where we come close to God.**[^2]  

[^2]: Adapted from Thomas Fitzgerald, "Worship in the Orthodox Church," [goarch.org](https://www.goarch.org/-/worship)  

### How is Worship Expressed?  

Worship in the Orthodox Church is expressed in two principal modes:
1. Actions
1. Language

In the Bible, Abraham worshiped God in this way when he first arrived in the Promised Land: he “built there an altar to the Lord and called on the name of the Lord.” (Gen 12:8, [LES](https://lexhampress.com/product/188040/the-lexham-english-septuagint-2nd-ed))

This is the pattern of worship that continues throughout Holy Scripture, from the children of Adam through the Temple in Jerusalem. When people want to encounter God, they go to his altar, offer a sacrifice, and they “call on his name.”

This is exactly what practicing Orthodox Christian believers do in worship. We go to the Church— the place where the Holy Altar is. We offer God a physical sacrifice— bread and wine— and we offer him our words. The words of worship are themselves a “sacrifice of praise” (Hebrews 13:15, [NKJV](https://www.bible.com/bible/114/HEB.13.NKJV)), and St Paul says that the “reasonable worship” (τὴν λογικὴν λατρείαν *tin logikin latrian*) of God is to “offer your bodies as a living sacrifice” (Romans 12:1, [NIV](https://www.bible.com/bible/111/ROM.12.NIV)). Using the organs of thought and speech to sing hymns is one of the ways that Christians use our living bodies to be a sacrifice to God in worship. In the words and actions of worship, not only do Orthodox Christians offer God a sacrifice made by humans— the bread and the wine— but we also offer our selves to God as a sacrifice.  

### What are the Words of Worship?  

The first song recorded in the Bible is a song of worship to God. This passage (Exodus 15:1-19) is written in a type of Hebrew that seems much older than the passages around it. Many scholars believe that this song, often called “The Song of the Sea,” reflects the very oldest language in all of Scripture. This means that the oldest text in the Bible begins with a call to worship: “Let us sing to the Lord, for He is greatly glorified.” ([Roumas Psalter](https://www.amazon.com/Psalter-David-Prophet-King-Nine/dp/B0B1885J4P)) This phrase is used by many Orthodox Christian hymns, especially hymns written for the First Ode of the Canon sung at every Matins service.

When we speak about worship, we often use the words *liturgy* or *service* to indicate what is being done and what is being said by people when they worship. The English word “liturgy” comes from the Greek word λειτουργία *liturgia* which means “service,” so these two terms are interchangeable. However, *liturgy* is only used in the context of worship, while *service* has many other meanings that have nothing to do with worship.

An action that is performed in worship is called a “liturgical act,” and a text that is used in worship is called a “liturgical text.” Books of liturgical texts are called *liturgical books*, *service books*, *hymnals*, or *hymn books*. Specific liturgical books may have their own special names, like *Menaion* or *Hieratikon*.  

The Bible contains many liturgical texts— that is, hymns and prayers— starting with the “Song of the Sea” in Exodus 15. Most of the hymns of the Bible are collected in the *Psalter*, also called the *Book of Psalms*. The Orthodox Church uses nine major Biblical hymns from parts of the Bible outside the Psalter. These hymns are called *Odes*, or *Canticles*. In addition to being found in the books of the Bible where they appear, the Nine Odes are often collected together into the *Book of Odes*, which is sometimes included as an appendix to editions of the *Psalter*. These Psalms and Odes are hymns that have been used to worship God for thousands of years.  

### What Happens When We Worship?  

Worship is the central means for human beings to be in the presence of God. We were created to be together with God, which means that we were created for worship. In fact, the theologian Alexander Schmemann speaks of humanity as the worshipping being, which he calls *homo adorans* (instead of *homo sapiens*). He says that because God blessed all of creation and gave it to humanity, the “only natural reaction” is for humans 

> **to bless God in return, to thank him, to see the world as God sees it and—in this act of gratitude and adoration—to know, name, and possess the world. All rational, spiritual, and other qualities of man[^3], distinguishing him from other creatures, have their focus and ultimate fulfillment in this capacity to bless God, to know, so to speak, the meaning of the thirst and hunger that constitutes his life… The first, the basic definition of man is that he is the priest. He stands in the center of the world and unifies it in his act of blessing God, of both receiving the world from God and offering it to God—and by filling the world with this eucharist, he transforms his life, the one that he receives from the world, into life in God, into communion with him. The world was created as the “matter,” the material of one all-embracing eucharist, and man was created as the priest of this cosmic sacrament.**  (Schmemann, *For the Life of the World*, page 22.) 

[^3]: Schmemann uses the word "man" to mean "human beings." He is not speaking about male adults only.  

If everything that people do is supposed to be oriented towards worship— and if human beings are the priests of worship that unites creation with God— then worship is the most important thing that people can do!

The theologian Stanley Harakas says that worship is 

> **the place where the Church affirms its existence as the “people of God,” where it proclaims the fact that their response to the saving work of Christ has made them members of the kingdom of God, an experience which begins in this life and will culminate in the eternal kingdom. The faithful experience a foretaste of that eternal kingdom through the liturgical experience… Liturgical living… is one of the most important aspects of what it means to be a Christian… it is in the divine liturgy that we find the reality of our union with Christ and one another as the Church of God.** (Harakas, *Living the Liturgy*, chapter 1.)

Vasileios Gontikakis, a theologian who served as abbot of two monasteries on Mount Athos, says

> **From its beginning, with the doxology of the Kingdom of the Godhead in three persons and the petition for the peace of the whole world, the Divine Liturgy broadens the horizon and the concerns of the faithful. Only in this way, by taking thought for everything, for things which are vast, world-wide and universal, by worshipping Him who created us out of non-being and by being mindful of His Second Coming, only thus can the believer order his own affairs, which are specific, personal, small and bounded by space and time. The origin and the destiny of each human being is such that he cannot find himself and his salvation except in the peace of the whole world and the salvation of the souls of all his brothers, for which the Divine Liturgy asks. And at the same time no one can do anything right or say anything true about the vast and universal problems and demands of the whole world— no one can love— unless he has set himself in order, unless he has been baptized, and has participated in the Liturgy and achieved personal stability through the painful process of repentance.** (Archimandrite Vasileios, *Hymn of Entry*, p. 71.)

### How Does the Bible Describe Worship?  

In addition to liturgical texts, the Bible also contains descriptions of worship. Early descriptions of worship usually include few details other than saying that somebody built an altar and “called on the name of the Lord.” The Book of Exodus gives a detailed description of how worship is to be conducted in the Tabernacle built in the desert by the children of Israel. Prophetic books, like Isaiah and the Revelation of St John, describe visions of what worship is like from the heavenly point of view.

The prophet Isaiah gives this description of heavenly worship:  

> **I saw the Lord seated on a high and raised throne, and the building was full of his glory. And seraphim[^4] stood around him; each one had six wings, and with two they were covering their face, and with two they were covering their feet, and with two they were flying. And one cried aloud to the other, and they were saying, “Holy, holy, holy, is the Lord Sabaoth[^5]; the whole land is full of his glory!”** (Isaiah 6:1-3, [LES](https://lexhampress.com/product/188040/the-lexham-english-septuagint-2nd-ed))

[^4]: *seraphim* are angelic beings who guard the throne of God; they are often described as resembling fiery serpents with wings.  

[^5]:  *Sabaoth* (σαβαώθ savaoth) is a Greek transliteration of the Hebrew word צְבָא֑וֹת  (ṣə·ḇā·’ō·wṯ or *tsevaot*) which literally means “of hosts.” The phrase יְהוָ֣ה צְבָא֑וֹת (Yah·weh ṣə·ḇā·’ō·wṯ or *YHWH tsevaot*) “Lord of Sabaoth,” is a title for God meaning that he commands the armies of angels. Somethimes יְהוָ֣ה צְבָא֑וֹת *YHWH tsevaot* is translated into Greek as Κύριος σαβαώθ *Kyrios savaoth* “Lord of Sabaoth,” (Isaiah 6:3, 1 Kingdoms 1:11), sometimes it is translated into Greek as Κύριος τῶν δυνάμεων *Kyrios ton dynameon* “Lord of the powers,” (2 Kingdoms 6:2), and sometimes it is translated into Greek as Κύριος Παντοκράτωρ *Kyrios Pantokrator* “Lord Almighty” (2 Kingdoms 7:8; Revelation 4:8).

St John the Theologian gives a similar description:

> **Four living creatures, each having six wings, were full of eyes around and within. And they do not rest day or night, saying: “Holy, holy, holy, Lord God Almighty, Who was and is and is to come!”** (Revelation 4:8, [NKJV](https://www.bible.com/bible/111/REV.4.NKJV))  

### We Worship With the Angels  

These two descriptions both show the highest ranks of angels— the seraphim and the cherubim[^6]— worshiping God by repeating the phrase “Holy, Holy, Holy.” This phrase, “Holy, Holy, Holy” is called the Τρισάγιον *Trisagion* “Three-Holy” hymn, and it is central to the language of Orthodox Christian worship. Every Orthodox Christian worship service includes the *Trisagion Hymn*, “Holy God, Holy Mighty, Holy Immortal, have mercy on us.” When human beings sing this hymn, we are taking on the role of the angels who guard the throne of God in the heavenly worship. By singing the Trisagion Hymn, we are communicating that we believe we are also participating in the heavenly worship, together with the angels. In the Divine Liturgy, before the Great Entrance the people sing: 

[^6]: *cherubim* are angelic beings who are often depicted as being the throne of God. Sometimes they are described as large animals with wings who God rides on, and sometimes they are described as the wheels of God's chariot.  

> **We, who in a mystery represent the Cherubim and sing the thrice-holy hymn to the life-giving Trinity, let us now lay aside every care of this life. For we are about to receive the King of all, invisibly escorted by the angelic hosts. Alleluia.**  

The third time that this “thrice-holy” hymn is referenced in the Divine Liturgy is during the Holy Oblation (Anaphora). The priest describes the worship of the angels:

> **there stand around you thousands of archangels and tens of thousands of angels, the Cherubim and Seraphim, six-winged and many-eyed, soaring upon their wings, singing, crying, shouting the triumphal hymn, and saying:**  

Next, the people say the words of the angels:

> **Holy, holy, holy, Lord Sabaoth; heaven and earth are full of your glory.** (Lash)

This means that when Orthodox Christians worship, we are worshiping at the throne of God himself, participating in the one eternal and timeless act of true worship. We are singing the hymn, “Holy, Holy, Holy” that is sung by the angels. A significant way that this worship is expressed is through *language*.

### The Paradox of Comprehension

At the beginning of the Holy Oblation (Anaphora) at Divine Liturgy, the priest says:

> **It is right and fitting to hymn you, to bless you, to praise you, to give you thanks, to worship you in every place of your dominion; for you are God, ineffable, incomprehensible, invisible, inconceivable, ever existing, eternally the same…** (Lash)  

This is a paradox. God is “ineffable”; that is, too great to describe in words. God is “incomprehensible” and “inconceivable”; that is, impossible to understand or imagine using words. But the Church has always used words to worship God. Even in the original languages, these have always been words that worshipers have been able to understand.

Sometimes the ideas are very difficult to understand, and God himself is impossible to understand. Sometimes, some texts are difficult to understand because they have preserved older forms of a language than is commonly spoken. But, when the texts were created, they were made and used in language that the people who made and used them could understand. The paradox is that we can’t understand God, but to worship God we must use words that we can understand. Because worship has to do with God, it is impossible to completely understand the true nature of worship. But the language of worship is language that human beings *can* use. It is language that human beings *can* understand.  

### The Need for Translation

If human beings are going to worship God in language that we can understand, then the worship of God must be translated into every human language.

In later lessons you will learn more about the history of Orthodox Christian worship and its translation. The important thing to understand right now is this:

From early times, the Orthodox Church has translated the texts of worship into many languages. As you begin to translate the texts of Orthodox Christian worship, you are joining an ancient tradition.

Welcome!  

---  

#### Further Reading:  

**Books**  

[*Living the Liturgy*](https://read.amazon.ca/kp/embed?asin=B007AM7LS0&preview=newtab&linkCode=kpe&ref_=cm_sw_r_kb_dp_VPHY4P2GR0PQZHYKMN3N) by Stanley Harakas. Light and Life Publishing, 1974. Kindle Edition.  

[*Hymn of Entry*](https://svspress.com/hymn-of-entry/) by Archimandrite Vasileios. SVS Press, 1984.  

[*For the Life of the World*](https://svspress.com/for-the-life-of-the-world-new-edition/) by Alexander Schmemann. SVS Press, 1973.  

**Free Online Resources**  

["Mystagogical Lecture V: On the Sacred Liturgy and Communion"](https://www.ccel.org/ccel/schaff/npnf207.ii.xxvii.html) by St Cyril of Jerusalem. Free online text is from [*A Select Library of the Nicene and Post-Nicene Fathers*, Series 2, Volume VII](https://www.ccel.org/ccel/schaff/npnf207.i.html) edited by Philip Schaff.  

["Worship in the Orthodox Church"](https://www.goarch.org/-/worship) by Thomas Fitzgerald, from the website of the Greek Orthodox Archdiocese of America.  