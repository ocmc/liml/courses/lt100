+++
title = "A. Examples"
date =  2020-08-06T14:27:45-07:00
weight = 3
+++

### EXAMPLE 1: Mark, Chapter 1

The following verses are taken from the first chapter of Mark's gospel. In these verses, write down every example of any idea that might be an UNKNOWN IDEA to many people in your area. (Think especially of any non-Christian people who have not had the opportunity of Christian education.)  

> **1** The beginning of the gospel of Jesus Christ, the Son of God.  

> **2** As it is written in the Prophets:  
*"Behold, I send My messenger before your face,*  
*Who will prepare Your way before You."*  
**3** *"The voice of one crying in the wilderness:*  
*'Prepare the way of the LORD;*  
*Make His paths straight.'"*  

> **4** John came baptizing in the wilderness and preaching a baptism of repentance for the remission of sins.  

> **9** It came to pass in those days that Jesus came from Nazareth of Galilee, and was baptized by John in the Jordan.  

> **10** And immediately, coming up from the water, He saw the heavens parting and the Spirit descending on him like a dove.  

> **11** Then a voice came from heaven, "You are My beloved Son, in whom I am well pleased."  

> **12** Immediately the Spirit drove Him into the wilderness.  

> **13** And He was there in the wilderness forty days, tempted by Satan, and was with the wild beasts; and the angels ministered to Him.  

> **21** Then they went into Capernaum, and immediately on the Sabbath He entered the synagogue and taught.  

> **22** And they were astonished at his teaching, for He taught them as one having authority, and not as the scribes.  

Notice that many of the words that you have underlined are words that refer to Jewish or Christian beliefs and religious systems. These are things that may have been unknown in the Receptor Language area before the introduction of Christianity. (This will depend on the local situation. In areas where Islam is established, some of these ideas will already be known.)  

The words you have written down will probably include:  

*gospel, Christ, Lord, baptize, preach, repent, sins, remission, heavens, Spirit, Satan, angel, Sabbath, synagogue, scribes*   

All of these words are examples of SPECIAL BIBLICAL TERMS. They are a particular kind of UNKNOWN IDEA.  

The translation of these SPECIAL BIBLICAL TERMS is extremely important for the full communication of the Christian message. The wrong choice of words will result in misunderstanding and distortion of that message.  

### EXAMPLE 2: THE SYMBOL OF FAITH  

Same instructions as above. Write down every example of any idea that might be an UNKNOWN IDEA to many people in your area, especially non-Christian people who have not had the opportunity of Christian education:  

> I believe in one God, Father
almighty, Maker of heaven and
earth, and of all things visible and
invisible.  

> And in one Lord, Jesus Christ, the
only-begotten Son of God, begotten
from the Father before all ages.
Light from Light, true God from true
God, begotten not made, consubstantial
with the Father; through
him all things were made;  

> for our
sake and for our salvation he came
down from heaven, and was incarnate
from the Holy Spirit and the
Virgin Mary and became man; he
was crucified also for us under Pontius
Pilate, and suffered and was
buried; he rose again on the third
day, in accordance with the Scriptures,
and ascended into heaven
and is seated at the right hand of
the Father; he is coming again in
glory to judge the living and the dead; and his kingdom will have no
end.  

> And in the Holy Spirit, the Lord,
the Giver of life, who proceeds
from the Father, who together with
Father and Son is worshipped
and together glorified; who spoke
through the Prophets. In one Holy,
Catholic and Apostolic Church; I
confess one Baptism for the forgiveness
of sins; I await the resurrection
of the dead and the life of
the age to come. Amen.


The words you have written down will probably include:  

*heaven, Lord, begotten, consubstantial, salvation, incarnate, Holy Spirit, crucified, Scriptures, worshipped, glorified, Prophets, Holy, Catholic, Apostolic, Church, Baptism, forgiveness, sins, resurrection*   

These are more examples of SPECIAL TERMS. Some of them, like *baptism* or *prophets*, are SPECIAL BIBLICAL TERMS. Others, like *consubstantial*, do not appear in the Bible. All of these SPECIAL TERMS have very specific meanings in Orthodox Christian teaching.

The translation of these SPECIAL TERMS is extremely important for the full communication of the Christian message. The wrong choice of words will result in misunderstanding and distortion of that message.  

--- 

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, pages 67 - 68  