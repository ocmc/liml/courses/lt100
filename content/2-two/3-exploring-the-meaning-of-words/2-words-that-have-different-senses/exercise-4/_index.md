+++
title = "Exercise 4: οἶκος"
date =  2021-11-25T13:12:04-08:00
weight = 5
+++

### A study of the Greek word οἶκος ikos 'house'

Look at the following texts in Greek and in several English translations.

Write down how the word οἶκος ikos 'house' is translated in each text.

Also think carefully about the real meaning that the word has in each passage, then write the word you would use in translating it in your own language also.  

---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, pages 55 - 57