+++
title = "Exercise 1"
date =  2024-02-16T20:52:54-08:00
weight = 5
+++

### Exercise 1: Make a word-for-word key

Make a word-for-word key of a liturgical prayer in your language. Make sure it is a prayer that you do not know in English.

* Do not look at any text except for the prayer that you are back-translating. The back-translation should reflect ONLY what this text says.
* Where there is an idiom or figure of speech, put the literal form in your word-for-word key and the actual meaning in brackets. Here are two examples from Mbembe:

| kaam | anọng | kinchi |
| ---- | ----- | ------ |
| I | people | do-not-eat |

*[I do not cheat people]*

and,

| ọtọr | bẹ | ọtọhng |
| ---- | -- | ------ |
| he-pulled | them | ears |

*[he-warned them strongly]*

* Include grammatical information in square brackets, where it is needed. For example, indicate whether a pronoun back-translated as "you" is singular or plural.
* Where two or more words are needed to back-translate one word in the translation, link the words with hyphens.
* Where two or more alternative words are needed to indicate the area of meaning of a word in the translation, separate these words with a slash. Examples: love/kindness, he/she.
* If words from the language you are back-translating from are included in your back-translation because they seem untranslatable, put an explanatory note enclosed in square brackets after the word.

If you know Kiswahili, you could use this prayer to make your word-for-word key:

> Ee Bwana wa majeshi, Mungu wa Israeli anayeponya kila ugonjwa na kila unyonge, mwangalie mtumishi wako; tafuta, chunguza na umwondolee kazi zote za shetani waondoe roho wote wachafu na uwaangamize na utakase kazi ya mikono yako; mkanyage shetani chini ya miguu yake (yao) mpatie ushindijuu ya shetani na roho wake wachafu ili kwa kupokea rehema zako astahili kushiriki katika sakramenti zako za mbinguni na aweze kutoa utukufu kwako, Baba, Mwana na Roho mtakatifu sasa-na siku zote hata milele na milele.

Your key should look something like this:

|        |    |       |    |          |       |    |         |
| - | -- | ----- | -- | -------- | ----- | -- | ------- |
|   | Ee | Bwana | wa | majeshi, | Mungu | wa | Israeli |
| *Key:* |    |       |    |          |       |    |         |
|        |    |       |    |          |       |    |         |
|   | anayeponya | kila | ugonjwa | na | kila | unyonge, | mwangalie |
| *Key:* |            |      |         |    |      |          |           |
|        |    |       |    |          |       |    |         |
|   | mtumishi | wako; | tafuta, | chunguza | na | umwondolee | kazi | 
| *Key:* |    |       |    |          |       |    |         |
|        |    |       |    |          |       |    |         |
|        | zote | za | shetani | waondoe | roho | wote | wachafu | 
| *Key:* |    |       |    |          |       |    |         |
|        |    |       |    |          |       |    |         |
|        | na | uwaangamize | na | utakase | kazi | ya | mikono |
| *Key:* |    |       |    |          |       |    |         |
|        |    |       |    |          |       |    |         |
|        | yako; | mkanyage | shetani | chini | ya | miguu | yake (yao) | 
| *Key:* |    |       |    |          |       |    |         |
|        |    |       |    |          |       |    |         |
|        | mpatie | ushindijuu | ya | shetani | na | roho | wake | 
| *Key:* |    |       |    |          |       |    |         |
|        |    |       |    |          |       |    |         |
|        | wachafu | ili | kwa | kupokea | rehema | zako | astahili |
| *Key:* |    |       |    |          |       |    |         |
|        |    |       |    |          |       |    |         | 
|        | kushiriki | katika | sakramenti | zako | za | mbinguni | na | 
| *Key:* |    |       |    |          |       |    |         |
|        |    |       |    |          |       |    |         |
|        | aweze | kutoa | utukufu | kwako, | Baba, | Mwana | na | 
| *Key:* |    |       |    |          |       |    |         |
|        |    |       |    |          |       |    |         |
|        | Roho | mtakatifu | sasa | na | siku | zote | hata | 
| *Key:* |    |       |    |          |       |    |         |
|        |    |       |    |          |       |    |         |
|        | milele | na | milele. | | | | |

### Exercise 2: Make a phrase-for-phrase back-translation

Looking at your word-for-word key, rewrite the back-translation so that each phrase follows understandable English syntax. As you make your phrase-for-phrase back-translation, remember the following:

* Do not look at any text except for the prayer that you are back-translating and your word-for-word. The back-translation should reflect ONLY what this text says.
* Be as literal as possible while still making grammatical sense. Make necessary adjustments to make the back-translation readable and understandable. But it should not read as a smooth English translation. Rather it should reflect the structure of the language of the text you are back-translating.
* Keep the order of sentences and clauses as it is in the text you are back-translating.
* Where there is an idiom or figure of speech, put the meaning in the text, with the literal form in square brackets.
* Include grammatical information in square brackets, where it is needed. For example, indicate whether a pronoun back-translated as "you" is singular or plural.
* Where two or more words are needed to back-translate one word in the translation, link the words with hyphens.
* Where two or more alternative words are needed to indicate the area of meaning of a word in the translation, separate these words with a slash. Examples: love/kindness, he/she.
* If words from the language you are back-translating from are included in your back-translation because they seem untranslatable, put an explanatory note enclosed in square brackets after the word.
* Add notes wherever needed to explain anything different or unusual in the translation. Add notes to explain background cultural information. The notes may be at the end of the text. Or you can use the footnote function if you are keying the back-translation on a computer.