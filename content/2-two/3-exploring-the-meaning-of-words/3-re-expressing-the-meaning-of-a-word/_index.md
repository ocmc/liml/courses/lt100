+++
title = "Re-Expressing the Meaning of a Word"
date =  2020-08-06T14:15:02-07:00
weight = 15
+++

One way to re-express the meaning of a word is to break it down into parts.  

### EXAMPLES  

| WORD | RE-EXPRESSED MEANING |  
| -- | -- |  
| bull | "male adult, of the bovine species" |  
| to dawdle | "to walk slowly, to do somethings slowly" |  
| fragrant | "giving off a pleasant smell" |  
| stink | "to give off an unpleasant smell" |  

A word is defined by stating the different parts of its meaning in this way. A dictionary definition of a word is a statement of the different parts of meaning of that word. The technical name for the parts of meaning is "components of meaning."  

Here are some sample definitions of words, taken from an English dictionary. Look up these words in your own dictionary and compare the definitions:  

| WORD | RE-EXPRESSED MEANING |  
| -- | -- |  
| cardigan | "warm woolly jersey, with sleeves, opening at the front" |  
| bungalow | "house with one storey" |  
| to burn | "to be consumed by fire" |  
| adder | "small poisonous snake" |  
| to dance | "to move with rhythmic steps, usually to music" |  

One of the ways suggested in the last chapter for translating UNKNOWN IDEAS is to use a "descriptive phrase." In fact, a descriptive phrase is a breaking down of the word, stating those components of its meaning that are most important in the context. This method can also be used to translate a word for an idea that is knon but for which there is no one-word equivalent in the Receptor Language.  

#### EXERCISE 5  

For each of the words underlined below, restate the meaning of the word by breaking it down into its components. (Use your dictionary to check the meaning of the words.)  

1. "Teacher, who sinned, this man or his **parents**, that he was born blind?"  
2. Mother of God and Ever-**Virgin** Mary  
1. Let all **mortal** flesh keep silent  
1. grant them... for **temporary** [things], eternal [things]  
1. Also we pray for... parents, **grandparents**, great grandparents, and ancestors  
1. Those who partake from here will be **multiplied**  
1. we are **sojourners** on earth  
1. worship of **pagans** 
1. For... every **city**, **town** and **village**... let us pray to the Lord.  
1. Symeon, **receive** with joy an infant in age

Citations:  
1. Vespers Stichera for the Sunday of the Blind Man  
1. Great Litany  
1. Cherubic Hymn for the Liturgy of St James  
1. Liturgy of St Basil, Priest's Prayer after "All of Creation Rejoices in You"  
1. Petition for Departed Souls  
1. Oikos for the Sunday of the Holy Fathers of the 4th Ecumenical Council  
1. Troparion for the Prophecy at Trithekti on Tuesday of the First Week of Great Lent  
1. Ode 1 of the Canon for September 1  
1. Great Litany  
1. Ode 4 of the Canon for the Presentation of Christ   

---

Adapted from *Bible Translation: An Introductory Course in Translation Principles* by Katharine Barnwell, 2017, pages 57 - 58.